/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.embeded

import org.springframework.security.crypto.codec.Hex
import java.security.MessageDigest
import java.security.SecureRandom
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.validation.constraints.Size

/**
 * Represents a set of data used to authenticate a user.
 * A private token means it must stay on the server.
 *
 * @property creationDate The token's creation date.
 * @property initializationVector The token's IV.
 * @property encryptionKey The private token's encryption key.
 * @property payload The content of the token.
 */
@Embeddable
class PrivateAuthenticationToken private constructor(
    @Column(nullable = false)
    val creationDate: ZonedDateTime,

    @Column(nullable = false)
    @Size(min = 16, max = 16)
    val initializationVector: ByteArray,

    @Column(nullable = false)
    @Size(min = 16, max = 16)
    val encryptionKey: ByteArray,

    @Column(nullable = false)
    @Size(min = 64, max = 64)
    val payload: String
) {
    companion object {
        /**
         * Generates a new random [PrivateAuthenticationToken].
         *
         * @return The new [PrivateAuthenticationToken].
         */
        fun generateNew(): PrivateAuthenticationToken {
            val random = SecureRandom()
            val iv = ByteArray(16)
            val key = ByteArray(16)
            val payload = ByteArray(128)

            random.nextBytes(iv)
            random.nextBytes(key)
            random.nextBytes(payload)

            val payloadString = String(Hex.encode(MessageDigest.getInstance("SHA-256").digest(payload)))

            return PrivateAuthenticationToken(ZonedDateTime.now(), iv, key, payloadString)
        }
    }
}
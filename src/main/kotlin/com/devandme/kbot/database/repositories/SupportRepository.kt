/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.repositories

import com.devandme.kbot.database.entities.Support
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.time.ZonedDateTime

@Repository
interface SupportRepository : PagingAndSortingRepository<Support, Long>, JpaSpecificationExecutor<Support> {
    @Query("SELECT COUNT(S) FROM Support S JOIN S.keywords K WHERE K.label = ?1")
    fun countByKeyword(label: String): Long

    @Query("FROM Support S JOIN FETCH S.warnings W WHERE S.solvedDate IS NULL AND W.date < ?1")
    fun findAllOpenWarnedBefore(before: ZonedDateTime): Set<Support>

    @Query("FROM Support S WHERE S.solvedDate IS NULL AND S.warnings IS EMPTY")
    fun findAllOpenNonWarned(): Set<Support>
}
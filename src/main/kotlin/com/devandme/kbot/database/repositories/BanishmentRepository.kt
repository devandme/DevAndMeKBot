/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.repositories

import com.devandme.kbot.database.entities.Banishment
import com.devandme.kbot.database.entities.User
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface BanishmentRepository : PagingAndSortingRepository<Banishment, Int>, JpaSpecificationExecutor<Banishment> {
    @Query(
        "SELECT EXISTS(SELECT 1 FROM sanction S WHERE S.dtype = 'Banishment' " +
                "AND S.start_date <= NOW() AND (S.end_date IS NULL OR S.end_date >= NOW()) AND S.target_id = ?)",
        nativeQuery = true
    )
    fun isBanned(userId: Long): Long

    @Query("FROM Banishment B WHERE B.startDate <= CURRENT_TIMESTAMP AND (B.endDate IS NULL OR B.endDate >= CURRENT_TIMESTAMP) AND B.target = ?1")
    fun findActiveForUser(user: User): List<Banishment>
}
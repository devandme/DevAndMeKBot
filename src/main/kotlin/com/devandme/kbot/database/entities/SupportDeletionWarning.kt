/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.entities

import java.time.ZonedDateTime
import javax.persistence.*

/**
 * Represents a support deletion warning message.
 *
 * @property messageId The ID of the channel message that has been sent.
 * @property support The support that has been warned.
 * @property date The warning creation date.
 */
@Entity
class SupportDeletionWarning(
    @Id
    @Column(nullable = false)
    val messageId: Long,

    @ManyToOne(targetEntity = Support::class, fetch = FetchType.EAGER, optional = false)
    val support: Support,

    @Column(nullable = false)
    val date: ZonedDateTime
) {
    override fun equals(other: Any?) = other is SupportDeletionWarning && other.messageId == this.messageId
    override fun hashCode() = this.messageId.hashCode()
}
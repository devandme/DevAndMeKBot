/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.entities

import java.time.ZonedDateTime
import javax.persistence.*
import javax.validation.constraints.NotEmpty

/**
 * A sanction applied to a user.
 *
 * @property id The sanction ID.
 * @property moderator The moderator who gave the sanction (null means that the user was deleted).
 * @property target The user who received.
 * @property startDate The sanction's creation date.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
abstract class Sanction(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,

    @NotEmpty
    @Column(nullable = false)
    open val reason: String,

    @ManyToOne(targetEntity = User::class, fetch = FetchType.LAZY)
    open var moderator: User?,

    @ManyToOne(targetEntity = User::class, fetch = FetchType.LAZY, optional = false)
    open val target: User,

    @Column(nullable = false)
    open val startDate: ZonedDateTime
) {
    override fun equals(other: Any?) = other is Sanction && other.id == this.id
    override fun hashCode() = this.id
}
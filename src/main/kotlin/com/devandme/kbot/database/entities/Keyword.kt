/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.entities

import javax.persistence.*

/**
 * A support keyword.
 *
 * @property label The keyword label.
 * @property supports
 */
@Entity
class Keyword(label: String) {

    @Id
    @Column(nullable = false, length = 185)
    val label: String = label.toLowerCase()

    @ManyToMany(targetEntity = Support::class, fetch = FetchType.LAZY, mappedBy = "keywords")
    val supports: MutableCollection<Support> = mutableListOf()

    override fun equals(other: Any?) = other is Keyword && other.label == this.label
    override fun hashCode() = this.label.hashCode()
}
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.entities

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.ZonedDateTime
import javax.persistence.*

/**
 * A help channel.
 *
 * @property channelId The Discord channel ID.
 * @property creationDate The support creation date.
 * @property author The support creator.
 * @property title The support title.
 * @property shortTitle The support channel name.
 * @property solvedDate When was the support closed?
 * @property keywords The tags used to identify the support.
 */
@Entity
class Support(
    @Id
    @Column(nullable = false)
    val channelId: Long,

    @Column(nullable = false)
    val creationDate: ZonedDateTime,

    @ManyToOne(targetEntity = User::class, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    val author: User,

    @Column(nullable = false)
    var title: String,

    @Column(nullable = false)
    val shortTitle: String,

    var solvedDate: ZonedDateTime?,

    @ManyToMany(targetEntity = Keyword::class, fetch = FetchType.EAGER)
    val keywords: MutableCollection<Keyword> = mutableListOf(),

    @OneToMany(targetEntity = SupportDeletionWarning::class, fetch = FetchType.LAZY, mappedBy = "support")
    val warnings: MutableCollection<SupportDeletionWarning> = mutableListOf()
) {
    override fun equals(other: Any?) = other is Support && other.channelId == this.channelId
    override fun hashCode() = this.channelId.hashCode()
}
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

/**
 * Represents the current status of a message counter.
 */
@Entity
class MessageScanStatus(
    @Id
    @Column(nullable = false)
    val channelId: Long,

    @Column(nullable = false)
    var lastScannedMessageId: Long
) {
    override fun equals(other: Any?) = other is MessageScanStatus && other.channelId == this.channelId
    override fun hashCode() = this.channelId.hashCode()
}
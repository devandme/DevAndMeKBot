/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.entities

import java.time.ZonedDateTime
import javax.persistence.Entity

/**
 * A banishment sanction.
 *
 * @property endDate The end date, or null if the ban is a life-time ban.
 */
@Entity
class Banishment(
    id: Int = 0,
    reason: String,
    moderator: User,
    target: User,
    startDate: ZonedDateTime,
    val endDate: ZonedDateTime?
) : Sanction(id, reason, moderator, target, startDate)
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.database.entities

import com.devandme.kbot.database.embeded.PrivateAuthenticationToken
import com.devandme.kbot.services.messages.LanguageLevel
import org.hibernate.validator.constraints.Range
import java.time.ZonedDateTime
import javax.persistence.*
import javax.validation.constraints.Min

/**
 * A Discord user.
 *
 * @property id The user's ID on Discord.
 * @property power The user's power (a number providing its rights).
 * @property messageCount The user's message count.
 * @property supportHelperCount The number of times the user helped someone on a support.
 * @property experience A number indicating the user's experience level.
 * @property privateToken The private authentication token.
 * @property discordLeaveDate The date when the user left the Discord server, if any.
 * @property sanctionsReceived The sanctions the user received.
 * @property sanctionsGiven The sanctions the user gave.
 * @property authoredSupports The supports the user opened.
 * @property helpedSupports The number of supports the user contributed to.
 */
@Entity
class User(
        @Id
        @Column(nullable = false)
        val id: Long,

        @Column(nullable = false)
        @Range(min = 0, max = 100)
        var power: Int,

        @Column(nullable = false)
        @Min(0)
        var messageCount: Long,

        @Column(nullable = false)
        @Min(0)
        var supportHelperCount: Int,

        @Column(nullable = false)
        @Min(0)
        var experience: Int,

        @Embedded
        var privateToken: PrivateAuthenticationToken,

        @Enumerated(EnumType.ORDINAL)
        @Column(nullable = false)
        var preferredLanguageLevel: LanguageLevel,

        @Column(nullable = false)
        var helpedSupports: Int = 0,

        @Column(nullable = true)
        var discordLeaveDate: ZonedDateTime? = null,

        @ElementCollection(targetClass = Tip::class, fetch = FetchType.EAGER)
        @CollectionTable(name = "user_received_tips", joinColumns = [JoinColumn(name = "user_id")])
        val receivedTips: MutableSet<Tip> = mutableSetOf(),

        @OneToMany(targetEntity = Sanction::class, fetch = FetchType.LAZY, mappedBy = "target")
        val sanctionsReceived: MutableCollection<Sanction> = mutableListOf(),

        @OneToMany(targetEntity = Sanction::class, fetch = FetchType.LAZY, mappedBy = "moderator")
        val sanctionsGiven: MutableCollection<Sanction> = mutableListOf(),

        @OneToMany(targetEntity = Support::class, fetch = FetchType.LAZY, mappedBy = "author")
        val authoredSupports: MutableCollection<Support> = mutableListOf()
) {
    override fun equals(other: Any?) = other is User && other.id == this.id
    override fun hashCode() = this.id.hashCode()
}
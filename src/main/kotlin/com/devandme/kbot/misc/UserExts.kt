/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.misc

import net.dv8tion.jda.core.entities.User
import net.dv8tion.jda.core.exceptions.ErrorResponseException
import net.dv8tion.jda.core.requests.ErrorResponse

/**
 * Sends a private message to a user.
 *
 * @param message The message to send.
 * @return `true` if the user allowed the bot to send the message, `false` otherwise.
 */
fun User.sendPrivateMessage(message: String): Boolean {
    val channel = this.openPrivateChannel().complete()

    for (m in splitMessage(message)) {
        try {
            channel.sendMessage(m).complete()
        } catch (e: ErrorResponseException) {
            if (e.errorResponse == ErrorResponse.CANNOT_SEND_TO_USER) {
                return false
            }

            throw e
        }
    }

    return true
}
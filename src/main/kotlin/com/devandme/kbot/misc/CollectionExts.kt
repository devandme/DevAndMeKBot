/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.misc

/**
 * Computes the cartesian product of two collections.
 *
 * @param other The other collection.
 * @param T The collection type.
 * @return A collection containing all pairs of data.
 */
operator fun <T> Collection<T>.times(other: Collection<T>): Collection<Pair<T, T>> {
    val result = mutableListOf<Pair<T, T>>()

    for (i in this) {
        for (j in other) {
            result += i to j
        }
    }

    return result
}
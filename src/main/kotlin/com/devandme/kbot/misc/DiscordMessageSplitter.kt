/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.misc

import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.Message.MAX_CONTENT_LENGTH
import java.lang.Math.max
import java.lang.Math.min

/**
 * Delimiter of the code blocks.
 */
private const val CODE_BLOCK_DELIMITER = "```"

/**
 * Inline code delimiter.
 */
private const val INLINE_CODE_DELIMITER = "`"

/**
 * Smiley delimiter.
 */
private const val SMILEY_DELIMITER = ":"

/**
 * Minimum size of a code block.
 */
private const val MIN_CODE_BLOCK_CONTENT_SIZE = 300

/**
 * Minimum size of a formatted text block.
 */
private const val MIN_TEXT_BLOCK_SIZE = 20

/**
 * Regular expressions matching all Discord's markdown 101 formatted text symbols.
 *
 * Order is important!
 */
private val FORMATTED_BLOCK_REGEX = listOf(
    INLINE_CODE_DELIMITER to "$INLINE_CODE_DELIMITER(.+?)$INLINE_CODE_DELIMITER".toRegex(RegexOption.DOT_MATCHES_ALL),
    "**" to """\*\*(.+?)\*\*""".toRegex(RegexOption.DOT_MATCHES_ALL),
    "*" to """\*(.+?)\*""".toRegex(RegexOption.DOT_MATCHES_ALL),
    "__" to "__(.+?)__".toRegex(RegexOption.DOT_MATCHES_ALL),
    "~~" to "~~(.+?)~~".toRegex(RegexOption.DOT_MATCHES_ALL)
)

/**
 * Regex matching all code blocks.
 */
private val CODE_BLOCK_REGEX =
    """$CODE_BLOCK_DELIMITER((?:[a-zA-Z]+\n)|)(.*?)$CODE_BLOCK_DELIMITER""".toRegex(RegexOption.DOT_MATCHES_ALL)

/**
 * Regex matching all smileys.
 */
private val SMILEY_REGEX = "$SMILEY_DELIMITER([a-z0-9_]+)$SMILEY_DELIMITER".toRegex()

/**
 * How does the algorithm works:
 *
 * The algorithm has been thought as a stream of blocks, than can be cut at any time.
 * A block is basically a string that can be split depending on its type.
 *
 * @property totalSize The total size of the block.
 */
private abstract class Block {
    abstract val totalSize: Int

    /**
     * Separates the block into several pieces.
     *
     * @param size The size of the first piece (may be different from the [maxBlockSize] since the first piece can be
     * concatenated to another existing piece).
     * @param maxBlockSize The size of the other pieces.
     */
    abstract fun split(size: Int, maxBlockSize: Int): List<String>
}

/**
 * A text block represents a simple string.
 *
 * @property content The text block's content.
 */
private class TextBlock(val content: String) : Block() {
    override val totalSize: Int
        get() = this.content.length

    override fun split(size: Int, maxBlockSize: Int): List<String> {
        if (this.content.length <= size) {
            return listOf(this.content)
        }

        // This algorithm tries to split the text on the first new line or space that can be found at the end of
        // this block's content.
        val result = mutableListOf<String>()
        var i = 0

        if (size == 0) {
            result += ""
        }

        while (i < this.content.length) {
            // Keep track of the first iteration which **must** return a [size]-sized String.
            val blockSize = if (i == 0 && size > 0) size else maxBlockSize

            // If the text is going to be split on the next iteration, just stop on a line feed or a space (tries to
            // take the closest from the end of the block size).
            val part = if (i + blockSize < this.content.length) {
                var endPos = max(
                    this.content.lastIndexOf(' ', i + blockSize),
                    this.content.lastIndexOf('\n', i + blockSize)
                )

                endPos = if (endPos <= i) i + blockSize else endPos

                this.content.substring(i, endPos)
            } else {
                this.content.substring(i)
            }

            // Trim here to avoid infinite loops with empty blocks.
            result += part.trimStart()

            // Important!
            i += part.length
        }

        return result
    }
}

/**
 * A code block represents a [CODE_BLOCK_DELIMITER] delimited string, along with a [language].
 *
 * @property content The code block's content.
 * @property language Never `null`, *may* be empty instead.
 */
private class CodeBlock(val content: String, val language: String) : Block() {
    override val totalSize: Int
        get() = this.content.length + this.language.length + CODE_BLOCK_DELIMITER.length * 2

    override fun split(size: Int, maxBlockSize: Int): List<String> {
        if (this.totalSize <= size) {
            return listOf(CODE_BLOCK_DELIMITER + this.language + this.content + CODE_BLOCK_DELIMITER)
        }

        // This algorithm tries to split the text on the first new line that can be found at the end of this block's
        // content.

        val minSize = this.language.length + CODE_BLOCK_DELIMITER.length * 2
        val result = mutableListOf<String>()
        var i = 0

        while (i < this.content.length) {
            // Keep track of the first iteration which **must** return a [size]-sized String.
            val blockSize = if (i == 0) {
                // If we're on the first iteration and the block size is less than the minimum code block size,
                // just insert a blank block to prevent concatenation of the first block with the previous, and
                // set the maxBlockSize as block size (minus the minimum size which includes the delimiter size).
                if (this.content.length > MIN_CODE_BLOCK_CONTENT_SIZE &&
                    size - minSize < MIN_CODE_BLOCK_CONTENT_SIZE + minSize
                ) {
                    result += ""
                    maxBlockSize - minSize
                } else {
                    size - minSize
                }
            } else {
                maxBlockSize - minSize
            }

            // If the text is going to be split on the next iteration, just stop on a line feed or a space (tries to
            // take the closest from the end of the block size).
            val part = if (i + blockSize < this.content.length) {
                var endPos = this.content.lastIndexOf('\n', i + blockSize)
                endPos = if (endPos <= i) i + blockSize else endPos

                this.content.substring(i, min(endPos, this.content.length))
            } else {
                this.content.substring(i)
            }

            // If this is not a blank part, add it.
            if (part.isNotBlank()) {
                result += CODE_BLOCK_DELIMITER + this.language + part + CODE_BLOCK_DELIMITER
            }

            // Important!
            i += part.length
        }

        return result
    }
}

/**
 * A formatted text block is a text that is delimited by one of the [FORMATTED_BLOCK_REGEX] dictionary keys.
 */
private class FormattedTextBlock(val content: Block, val delimiter: String) : Block() {
    override val totalSize: Int
        get() = this.content.totalSize + this.delimiter.length * 2

    override fun split(size: Int, maxBlockSize: Int): List<String> {
        // This algorithm tries to split the string by keeping the delimiters at the beginning and at the end of it.
        val minSize = this.delimiter.length * 2
        val split = this.content.split(size - minSize, maxBlockSize)

        return if (this.content.totalSize > MIN_TEXT_BLOCK_SIZE && split.first().length < MIN_TEXT_BLOCK_SIZE) {
            listOf("") + this.content.split(0, maxBlockSize - minSize)
                .asSequence()
                .filter(String::isNotBlank)
                .map { this.delimiter + it.trim() + this.delimiter }
                .toList()
        } else {
            split.asSequence().filter(String::isNotBlank).map { this.delimiter + it.trim() + this.delimiter }.toList()
        }
    }
}

/**
 * A smiley :D
 *
 * @property smileyName The smiley's name.
 */
private class SmileyBlock(val smileyName: String) : Block() {
    override val totalSize: Int
        get() = this.smileyName.length + SMILEY_DELIMITER.length * 2

    override fun split(size: Int, maxBlockSize: Int): List<String> =
        if (size < this.totalSize) {
            listOf("", SMILEY_DELIMITER + this.smileyName + SMILEY_DELIMITER)
        } else {
            listOf(SMILEY_DELIMITER + this.smileyName + SMILEY_DELIMITER)
        }
}

/**
 * A block container. This block is usually a root block.
 *
 * @property content The other blocks contained in this block.
 */
private class AggregateBlock(val content: List<Block>) : Block() {
    override val totalSize: Int
        get() = this.content.sumBy(Block::totalSize)

    override fun split(size: Int, maxBlockSize: Int): List<String> {
        val result = mutableListOf<String>()
        result += this.content.first().split(size, maxBlockSize)

        for (block in this.content.drop(1)) {
            if (result.isEmpty()) {
                result += block.split(maxBlockSize, maxBlockSize)
            } else {
                val lastElement = result.last()
                val split = block.split(maxBlockSize - lastElement.length, maxBlockSize)
                result[result.lastIndex] += split.first()
                result += split.drop(1)
            }
        }

        return result.filter(String::isNotBlank)
    }
}

/**
 * Markdown-safe function that splits the provided message into several [Message]s.
 *
 * @param messageString The message to split.
 * @return The created [Message]s.
 */
fun splitMessage(messageString: String): List<Message> =
    if (messageString.isEmpty()) {
        throw IllegalArgumentException("Parameter messageString cannot be empty!")
    } else if (messageString.length <= MAX_CONTENT_LENGTH) {
        listOf(MessageBuilder(messageString).build())
    } else {
        // Replace CRLF to LF.
        split(parse(messageString.replace("\r\n", "\n"))).map { MessageBuilder(it).build() }
    }


/**
 * Huge function that parses the provided markdown recursively.
 *
 * @param messageString The message to parse.
 * @return The root block.
 */
private fun parse(messageString: String): Block {
    // A list of all parsed blocks to return.
    val parsedBlocks = mutableListOf<Block>()

    // =================================================================================================================
    //                                               SOME FUNCTIONS
    // =================================================================================================================

    /**
     * Parses the substring of [messageString] before the provided regex match and adds the result to the parsed blocks.
     */
    fun parseBefore(result: MatchResult) {
        if (result.range.first > 0) {
            val parsed = parse(messageString.substring(0, result.range.first))

            if (parsed is AggregateBlock) {
                parsedBlocks += parsed.content
            } else {
                parsedBlocks += parsed
            }
        }
    }

    /**
     * Parses the substring of [messageString] after the provided regex match and adds the result to the parsed blocks.
     */
    fun parseAfter(result: MatchResult) {
        if (result.range.endInclusive < messageString.lastIndex) {
            val parsed = parse(messageString.substring(result.range.endInclusive + 1))

            if (parsed is AggregateBlock) {
                parsedBlocks += parsed.content
            } else {
                parsedBlocks += parsed
            }
        }
    }

    /**
     * Gets the first non-escaped (not preceded by an backslash) regex in the [messageString].
     *
     * @param regex The regex to find.
     * @return The match result, or `null` if nothing was found.
     */
    fun firstNonEscaped(regex: Regex): MatchResult? {
        var result: MatchResult? = null

        do {
            result = regex.find(messageString, (result?.range?.first ?: -1) + 1)
        } while (result != null && messageString.getOrNull(result.range.first - 1) == '\\')

        return result
    }

    // =================================================================================================================
    //                                          NO MORE FUNCTIONS NOW
    // =================================================================================================================

    // First, try to find the first non escaped code block.
    val codeBlockMatch = firstNonEscaped(CODE_BLOCK_REGEX)

    // If a code block was found, parse the content before it, if any, create the block and then parse the content after
    // it, if any.
    if (codeBlockMatch != null) {
        parseBefore(codeBlockMatch)
        parsedBlocks += CodeBlock(codeBlockMatch.groupValues[2], codeBlockMatch.groupValues[1])
        parseAfter(codeBlockMatch)
    } else {
        // Get the first matching (by order and by index in the string) formatted text.
        val formattedTextMatch = FORMATTED_BLOCK_REGEX
            .asSequence()
            .mapNotNull { (delimiter, regex) -> delimiter to (firstNonEscaped(regex) ?: return@mapNotNull null) }
            .minBy { (_, match) -> match.range.first }

        // If we've no formatted text,
        if (formattedTextMatch == null) {
            // Let's try to find smileys :D
            val smileyMatch = SMILEY_REGEX.find(messageString)

            // If no smiley was found, write a text block.
            if (smileyMatch == null) {
                parsedBlocks += TextBlock(messageString)
            }
            // Else, parse before, parse the smiley and then parse after it.
            else {
                parseBefore(smileyMatch)
                parsedBlocks += SmileyBlock(smileyMatch.groupValues[1])
                parseAfter(smileyMatch)
            }
        }
        // Else, if we've found a formatted text,
        else {
            val (delimiter, match) = formattedTextMatch

            parseBefore(match)

            // If we've an inline code, don't parse the content.
            parsedBlocks += if (delimiter == INLINE_CODE_DELIMITER) {
                FormattedTextBlock(TextBlock(match.groupValues[1]), INLINE_CODE_DELIMITER)
            } else {
                FormattedTextBlock(parse(match.groupValues[1]), delimiter)
            }

            parseAfter(match)
        }
    }

    // Return the created blocks.
    // Since we should return only one block, use AggregateBlock (prevents multi-root trees).
    return if (parsedBlocks.size == 1) parsedBlocks.first() else AggregateBlock(parsedBlocks)
}

/**
 * Splits the provided block into several strings.
 *
 * @param root The root block.
 * @return The result of splitting the block.
 */
private fun split(root: Block): List<String> = root.split(MAX_CONTENT_LENGTH, MAX_CONTENT_LENGTH)
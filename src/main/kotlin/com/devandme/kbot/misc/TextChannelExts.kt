/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.misc

import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel

/**
 * Retrieves the message history in chronological order while the provided [predicate] is satisfied.
 *
 * @param firstMessage The first message.
 * @param predicate The predicate.
 * @return The matching messages.
 */
fun TextChannel.getHistoryWhile(firstMessage: Message, predicate: (Message) -> Boolean) =
    this.getHistoryWhile(firstMessage.idLong, predicate)

/**
 * Retrieves the message history in chronological order while the provided [predicate] is satisfied.
 *
 * @param firstMessageId The first message ID.
 * @param predicate The predicate.
 * @return The matching messages.
 */
fun TextChannel.getHistoryWhile(firstMessageId: Long, predicate: (Message) -> Boolean): List<Message> {

    fun getReversedHistoryAfter(firstMessageId: Long, predicate: (Message) -> Boolean) =
        this.getHistoryAfter(firstMessageId, 100).complete().retrievedHistory.reversed().takeWhile(predicate)

    val result = mutableListOf<Message>()

    var messages = getReversedHistoryAfter(firstMessageId, predicate)

    while (messages.isNotEmpty()) {
        result += messages
        messages = getReversedHistoryAfter(messages.last().idLong, predicate)
    }

    return result
}

/**
 * The last [Message] of the channel.
 */
val TextChannel.lastMessage: Message?
    get() = this.iterableHistory.firstOrNull()

/**
 * @return The last [Message] sent by the provided user ID.
 */
fun TextChannel.getLastMessageSentBy(userId: Long) =
    this.iterableHistory.asSequence().take(1000).firstOrNull { it.author.idLong == userId }
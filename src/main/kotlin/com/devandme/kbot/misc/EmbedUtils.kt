/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.misc

import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.entities.MessageEmbed
import net.dv8tion.jda.core.entities.User
import java.awt.Color

/**
 * Creates a message embed with a user.
 */
fun createEmbed(
    title: String? = null,
    url: String? = null,
    description: String? = null,
    author: User? = null,
    color: Color? = null,
    fields: Array<MessageEmbed.Field>
) = createEmbed(title, url, description, author?.name, null, author?.avatarUrl, color, fields)

/**
 * Creates a message embed.
 */
fun createEmbed(
    title: String? = null,
    url: String? = null,
    description: String? = null,
    authorName: String? = null,
    authorUrl: String? = null,
    authorAvatar: String? = null,
    color: Color? = null,
    fields: Array<MessageEmbed.Field>
): MessageEmbed =
    EmbedBuilder()
        .apply {
            setTitle(title, url)
            setDescription(description)
            this.fields.addAll(fields)
            setColor(color)
            setAuthor(authorName, authorUrl, authorAvatar)
        }
        .build()
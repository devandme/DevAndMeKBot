/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.misc

/**
 * Power required to update someone else's preferences or experience.
 */
const val POWER_UPDATE_OTHER_USER = 50

/**
 * Power required to open someone else's support.
 */
const val POWER_OPEN_OTHER_SUPPORT = 50

/**
 * Power required to close someone else's support.
 */
const val POWER_CLOSE_OTHER_SUPPORT = 50

/**
 * Power required to update someone else's support.
 */
const val POWER_UPDATE_OTHER_SUPPORT = 50

/**
 * Power required to delete a support.
 */
const val POWER_DELETE_SUPPORT = 50

/**
 * Power required to delete a keyword.
 */
const val POWER_KEYWORD_MANAGE = 50

/**
 * Power required to view information about sanctions.
 */
const val POWER_SANCTION_LIST = 50

/**
 * Power required to mute people (must have lower power than the moderator).
 */
const val POWER_SANCTION_MUTE = 50

/**
 * Power required to kick people (must have lower power than the moderator).
 */
const val POWER_SANCTION_KICK = 60

/**
 * Power required to ban people (must have lower power than the moderator).
 */
const val POWER_SANCTION_BAN = 70

/**
 * Power required to execute //clear command.
 */
const val POWER_CHANNEL_CLEAR = 60

/**
 * Power required to execute //slowmode command.
 */
const val POWER_CHANNEL_SLOW = 40
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.misc

import java.text.ParseException
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoField
import java.time.temporal.Temporal
import java.util.*
import java.util.concurrent.TimeUnit

val FRENCH_SHORT_DATETIME = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss", Locale.FRANCE)

val FRENCH_LONG_DATETIME = DateTimeFormatter.ofPattern("EE d LLLL yyyy à HH:mm:ss", Locale.FRANCE)

val FRENCH_SHORT_DATE = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.FRANCE)

val FRENCH_LONG_DATE = DateTimeFormatter.ofPattern("EE d LL yyyy", Locale.FRANCE)

val FRENCH_TIME = DateTimeFormatter.ofPattern("HH:mm:ss", Locale.FRANCE)

val EUROPE = ZoneId.of("Europe/Paris")

val INFINITE_DURATION = Duration.ofMillis(Long.MAX_VALUE)

private val FRENCH_DURATION_GLOBAL_REGEX =
    "^([0-9]+ *(j|jours?|h|heures?|m|minutes?|s|secondes?)( *, *| *et *)? *)+$".toRegex()

private val FRENCH_DURATION_UNIT_REGEX = "([0-9]+) *(j|jours?|h|heures?|m|minutes?|s|secondes?)".toRegex()

// TODO Document
fun formatDifference(start: Temporal, end: Temporal): String {
    val result = StringBuilder()
    val period = Period.between(LocalDate.from(start), LocalDate.from(end)).normalized()

    format(period, result)

    if (start.isSupported(ChronoField.SECOND_OF_MINUTE) && end.isSupported(ChronoField.SECOND_OF_MINUTE) && period.days <= 2 && period.years == 0 && period.months == 0) {
        format(Duration.between(LocalTime.from(start), LocalTime.from(end)), result)
    }

    return result.toString()
}

// TODO Document
fun format(period: Period, result: StringBuilder = java.lang.StringBuilder()): String {
    // Difference in years.
    val years = period.years.toLong()
    val showYears = years > 0

    // Difference in months.
    val months = period.months.toLong()
    val showMonths = months > 0 && years <= 1

    // Difference in days.
    val days = period.days.toLong()
    val showDays = days > 0 && !showYears


    if (showYears) {
        result.append(years).append(if (years == 1L) " an" else " ans")
    }

    if (showMonths) {
        if (showYears) {
            result.append(", ")
        }

        result.append(months).append(" mois")
    }

    if (showDays) {
        if (showMonths) {
            result.append(", ")
        }

        result.append(days).append(if (days == 1L) " jour" else " jours")
    }

    return result.toString()
}

// TODO Document
fun format(duration: Duration, result: StringBuilder = java.lang.StringBuilder()): String {
    if (duration.toMillis() < 1000) {
        result.append("moins d'une seconde")
        return result.toString()
    }

    var seconds = duration.seconds

    // Difference in hours.
    val hours = Math.floorDiv(seconds, 3600)
    val showHours = hours > 0

    if (hours > 0) {
        seconds %= 3600
    }

    // Difference in minutes.
    val minutes = Math.floorDiv(seconds, 60)
    val showMinutes = minutes > 0

    if (minutes > 0) {
        seconds %= 60
    }

    val showSeconds = seconds > 0 && !showHours

    if (showHours) {
        result.append(hours).append(if (hours == 1L) " heure" else " heures")
    }

    if (showMinutes) {
        if (showHours) {
            result.append(", ")
        }

        result.append(minutes).append(if (minutes == 1L) " minute" else " minutes")
    }

    if (showSeconds) {
        if (showMinutes) {
            result.append(", ")
        }

        result.append(seconds).append(if (seconds == 1L) " seconde" else " secondes")
    }

    return result.toString()
}

// TODO Document
fun parseDuration(string: String): Duration {
    if (string == "infinite") {
        return INFINITE_DURATION
    }

    if (!string.matches(FRENCH_DURATION_GLOBAL_REGEX)) {
        throw ParseException("Unrecognized sequence!", 0)
    }

    val seconds = FRENCH_DURATION_UNIT_REGEX.findAll(string)
        .asSequence()
        .map {
            val (amount, unit) = it.destructured

            when (unit) {
                "j", "jour", "jours" -> TimeUnit.DAYS.toSeconds(amount.toLong())
                "h", "heure", "heures" -> TimeUnit.HOURS.toSeconds(amount.toLong())
                "m", "minute", "minutes" -> TimeUnit.MINUTES.toSeconds(amount.toLong())
                "s", "seconde", "secondes" -> amount.toLong()
                else -> throw RuntimeException("Unhandled case!")
            }
        }
        .sum()

    return Duration.ofSeconds(seconds)
}
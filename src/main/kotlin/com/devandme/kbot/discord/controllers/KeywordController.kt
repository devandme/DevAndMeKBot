/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandDefinition
import com.devandme.kbot.misc.POWER_KEYWORD_MANAGE
import com.devandme.kbot.services.KeywordService
import com.devandme.kbot.services.SupportService
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import org.springframework.transaction.annotation.Transactional

@Transactional
@DiscordController
class KeywordController(
        messageService: MessageService,
        private val keywordService: KeywordService,
        private val supportService: SupportService
) {
    private val messages = messageService / MessageStorage.KEYWORD_CONTROLLER

    @CommandDefinition(
            "keyword search <term>",
            "Recherche tous les mots-clefs contenant le terme spécifié",
            "Permet de chercher tous les mots-clefs correspondants"
    )
    fun keywordSearch(context: CommandContext, term: String) {
        // Limit to 10 keywords so we don't spam the user.
        val keyword = this.keywordService.search(term).take(10)

        if (keyword.isEmpty()) {
            context.sendMessage(this.messages / "search-without-result")
        } else {
            context.sendMessageFormat(this.messages / "search-all", keyword.joinToString { it.label })
        }
    }

    @CommandDefinition(
            "keyword info <term>",
            "Fournit des statistiques sur un mot-clef",
            "Permet d'obtenir des informations sur un mot-clef"
    )
    fun keywordInfo(context: CommandContext, term: String) {
        val keyword = this.keywordService.findOneByLabel(term)

        if (keyword == null) {
            context.sendMessage(messages / "search-without-result")
        } else {
            context.sendMessageFormat(this.messages / "search-one", keyword.label, supportService.countByKeyword(keyword))
        }
    }

    @CommandDefinition(
            "keyword delete <keyword>",
            "Supprime un mot-clef particulier",
            "Permet de supprimer un mot-clef particulier",
            POWER_KEYWORD_MANAGE
    )
    fun keywordDelete(context: CommandContext, keyword: String) {
        val keywordFound = this.keywordService.findOneByLabel(keyword)

        if (keywordFound == null) {
            context.sendMessage(messages / "search-without-result")
        } else {
            this.keywordService.delete(keywordFound)
            context.sendMessageFormat(this.messages / "delete", keywordFound.label)
        }
    }
}
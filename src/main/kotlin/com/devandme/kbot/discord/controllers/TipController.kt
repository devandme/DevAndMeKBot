/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.database.entities.Tip
import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.discord.services.listeners.DiscordEventListener
import com.devandme.kbot.misc.sendPrivateMessage
import com.devandme.kbot.services.UserService
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.core.events.user.UserTypingEvent
import java.util.*
import javax.transaction.Transactional

/**
 * Controller that handles welcome messages and help while writing code/presentation.
 */
@DiscordController
class TipController(private val discord: DiscordService, private val users: UserService, messageService: MessageService) {

    private val messages = messageService / MessageStorage.TIP_CONTROLLER
    private val presentationTipsRecipients = Collections.synchronizedSet(mutableSetOf<Long>())
    private val codeTipsRecipients = Collections.synchronizedSet(mutableSetOf<Long>())

    @DiscordEventListener
    fun onGuildJoin(evt: GuildMemberJoinEvent) {
        val user = evt.user
        val databaseUser = this.users.findOneById(user.idLong) ?: this.users.createDefault(user.idLong)

        if (!user.sendPrivateMessage((this.messages / "welcome").fmt(databaseUser, user.asMention))) {
            this.discord.spamChannel
                    .sendMessage((this.messages / "welcome-no-private-messages").fmt(databaseUser, user.asMention))
                    .complete()
        }
    }

    @Transactional
    @DiscordEventListener
    fun onChannelTyping(evt: UserTypingEvent) {
        val user = evt.user
        val channel = evt.textChannel ?: return

        if (this.discord.presentationChannel == channel && user.idLong !in this.presentationTipsRecipients) {
            val databaseUser = this.users.findOneById(user.idLong) ?: this.users.createDefault(user.idLong)

            if (Tip.PRESENTATION !in databaseUser.receivedTips) {
                if (user.sendPrivateMessage((this.messages / "presentation-channel-rules").fmt(databaseUser, user.asMention))) {
                    databaseUser.receivedTips += Tip.PRESENTATION
                    this.users.save(databaseUser)
                } else {
                    this.discord.spamChannel.sendMessage(this.messages / "no-private-messages" / databaseUser).complete()
                }
            }

            this.presentationTipsRecipients += user.idLong
            return
        }

        if (channel in this.discord.channelsWithCode && user.idLong !in this.codeTipsRecipients) {
            val databaseUser = this.users.findOneById(user.idLong) ?: this.users.createDefault(user.idLong)

            if (Tip.CODE !in databaseUser.receivedTips) {
                if (user.sendPrivateMessage((this.messages / "code-channel-syntax").fmt(databaseUser, user.asMention))) {
                    databaseUser.receivedTips += Tip.CODE
                    this.users.save(databaseUser)
                } else {
                    this.discord.spamChannel
                            .sendMessage((this.messages / "no-private-messages").fmt(databaseUser, user.asMention))
                            .complete()
                }
            }

            this.codeTipsRecipients += user.idLong
        }
    }
}
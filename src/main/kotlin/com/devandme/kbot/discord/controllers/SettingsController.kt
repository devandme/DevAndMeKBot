/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandDefinition
import com.devandme.kbot.services.UserService
import com.devandme.kbot.services.messages.LanguageLevel
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage

private val LANGUAGE_LEVELS_BY_LABELS = LanguageLevel.values().associateBy { it.label.toLowerCase() }

@DiscordController
class SettingsController(messageService: MessageService, private val users: UserService) {

    private val messages = messageService / MessageStorage.SETTINGS_CONTROLLER

    @CommandDefinition(
            "settings get language-level",
            "Affiche le paramètre de niveau de langage",
            "Affiche le niveau de langage que le bot utilisera pour communiquer avec vous"
    )
    fun getLanguage(commandContext: CommandContext) {
        commandContext.sendMessage(this.messages / "language-get")
    }

    @CommandDefinition(
            "settings set language-level <level>",
            "Définit le paramètre de niveau de langage",
            "Définit le niveau de langage que le bot utilisera pour communiquer avec vous"
    )
    fun setLanguage(commandContext: CommandContext, level: String) {
        val languageLevel = LANGUAGE_LEVELS_BY_LABELS[level.toLowerCase()]

        if (languageLevel == null) {
            commandContext.sendMessage(this.messages / "language-set-error")
        } else {
            commandContext.databaseUser.preferredLanguageLevel = languageLevel
            this.users.save(commandContext.databaseUser)

            commandContext.sendMessage(this.messages / "language-set")
        }
    }
}
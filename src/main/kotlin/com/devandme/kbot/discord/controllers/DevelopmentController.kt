/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandDefinition
import com.devandme.kbot.web.security.PublicAuthenticationToken
import org.springframework.context.annotation.Profile
import java.time.ZonedDateTime

@Profile("dev")
@DiscordController
class DevelopmentController {
    @CommandDefinition(
        "auth [duration-minutes]",
        "Procure des informations d'authentification",
        "Envoie un message avec l'ID de l'utilisateur ainsi qu'un token d'une validité de 24h par défaut"
    )
    fun auth(context: CommandContext, durationMinutes: Long = 3600 * 24) {
        val token = PublicAuthenticationToken.createFromPrivate(
            context.databaseUser.privateToken,
            ZonedDateTime.now().plusMinutes(durationMinutes)
        )

        context.sendMessage(
            "User ID: `${context.databaseUser.id}`\nAuth token: ```$token``` (validity: $durationMinutes minutes)"
        )
    }
}
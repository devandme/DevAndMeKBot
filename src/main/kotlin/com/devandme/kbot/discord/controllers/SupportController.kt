/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.database.entities.User
import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandDefinition
import com.devandme.kbot.discord.services.commands.DiscordCommandContext
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.discord.services.discordWizard.DiscordWizardService
import com.devandme.kbot.discord.services.discordWizard.StepExecutionResult
import com.devandme.kbot.misc.POWER_CLOSE_OTHER_SUPPORT
import com.devandme.kbot.misc.POWER_DELETE_SUPPORT
import com.devandme.kbot.misc.POWER_OPEN_OTHER_SUPPORT
import com.devandme.kbot.misc.POWER_UPDATE_OTHER_SUPPORT
import com.devandme.kbot.services.*
import com.devandme.kbot.services.SupportService.ExistingAttachment
import com.devandme.kbot.services.SupportService.SupportContents
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import com.vdurmont.emoji.EmojiManager
import net.dv8tion.jda.core.entities.Channel
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageReaction
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent
import javax.persistence.EntityManager
import javax.transaction.Transactional

private val SHORT_TITLE_REGEX = "^[a-zA-Z0-9_-]{3,30}$".toRegex()

@Transactional
@DiscordController
class SupportController(
        messageService: MessageService,
        private val users: UserService,
        private val supports: SupportService,
        private val discord: DiscordService,
        private val wizards: DiscordWizardService,
        private val keywords: KeywordService,
        private val entityManager: EntityManager
) {

    private val messages = messageService / MessageStorage.SUPPORT_CONTROLLER

    @CommandDefinition("support create", "Crée un support", "Débute la création d'un nouveau support")
    fun supportCreate(context: CommandContext) {
        if (context !is DiscordCommandContext) {
            context.sendMessage(this.messages / "command-not-executable-in-cli")
            return
        }

        val databaseUser = this.entityManager.merge(context.databaseUser)

        if (databaseUser.authoredSupports.size >= MAX_SUPPORTS_PER_USER) {
            context.sendMessage(this.messages / "too-many-supports")
            return
        }

        class SupportData(
                var title: String,
                var shortTitle: String? = null,
                var content: String? = null,
                var attachments: List<SupportService.SupportAttachment>? = null,
                var keywords: Set<String>? = null
        )

        val supportCreation = this.messages / "support-creation" / context.databaseUser

        val result = this.wizards.wizardBuilder(supportCreation, context.discordMember.user, context.channel)
                .first { awaiter ->
                    context.sendMessage(this.messages / "support-title-question")

                    val title = awaiter.waitForUserMessage()?.contentRaw
                            ?: return@first StepExecutionResult.cancel<SupportData>()

                    if (title.length in 6..255) {
                        context.sendMessage(this.messages / "support-ok")
                        StepExecutionResult.next(SupportData(title))
                    } else {
                        context.sendMessage(this.messages / "support-title-invalid-length")
                        StepExecutionResult.replay()
                    }
                }
                .then { data, awaiter ->
                    context.sendMessage(this.messages / "support-short-title-question")

                    val shortTitle = awaiter.waitForUserMessage()?.contentRaw
                            ?: return@then StepExecutionResult.cancel<SupportData>()

                    if (shortTitle.matches(SHORT_TITLE_REGEX)) {
                        if (this.discord.guild.getTextChannelsByName(shortTitle, true).isNotEmpty()) {
                            context.sendMessage(this.messages / "support-short-title-in-use")

                            StepExecutionResult.replay()
                        } else {
                            data.shortTitle = shortTitle.toLowerCase()
                            context.sendMessage(this.messages / "support-ok")
                            StepExecutionResult.next(data)
                        }
                    } else {
                        context.sendMessage(this.messages / "support-short-title-invalid")
                        StepExecutionResult.replay()
                    }
                }
                .then { data, awaiter ->
                    context.sendMessage(this.messages / "support-keywords-question")

                    val message = awaiter.waitForUserMessage()?.contentRaw
                            ?: return@then StepExecutionResult.cancel<SupportData>()

                    val keywords = message.split(',', ';', ' ', '\t', '\n').asSequence().filter(String::isNotBlank).toSet()

                    when {
                        keywords.isEmpty() -> {
                            context.sendMessage(this.messages / "support-keywords-empty")
                            StepExecutionResult.replay()
                        }
                        keywords.size > MAX_KEYWORD_COUNT -> {
                            context.sendMessage(this.messages / "support-keywords-too-many")
                            StepExecutionResult.replay()
                        }
                        keywords.any { k -> Message.MentionType.values().any { it.pattern.matcher(k).matches() } } -> {
                            context.sendMessage(this.messages / "support-keywords-no-mention")
                            StepExecutionResult.replay()
                        }
                        else -> {
                            data.keywords = keywords
                            context.sendMessage(this.messages / "support-ok")
                            StepExecutionResult.next(data)
                        }
                    }
                }
                .then { data, awaiter ->
                    context.sendMessage(this.messages / "support-content-question")

                    val content = StringBuilder()
                    val attachments = mutableListOf<SupportService.SupportAttachment>()
                    var message = awaiter.waitForUserMessage()
                            ?: return@then StepExecutionResult.cancel<SupportData>()

                    while (message.contentRaw != "!finish") {
                        for (attachment in message.attachments) {
                            if (attachments.size == MAX_ATTACHMENT_COUNT) {
                                context.sendMessage(this.messages / "support-content-too-much-files")
                                break
                            }

                            attachments += ExistingAttachment(attachment)
                        }

                        if (Message.MentionType.values().any { it.pattern.matcher(message.contentRaw).find() }) {
                            context.sendMessage(this.messages / "support-content-no-mention")
                        } else {
                            content.appendln(message.contentRaw)

                            if (content.length >= MAX_CONTENT_LENGTH) {
                                content.setLength(MAX_CONTENT_LENGTH)
                                context.sendMessage(this.messages / "support-content-too-long")
                                break
                            }
                        }

                        message = awaiter.waitForUserMessage()
                                ?: return@then StepExecutionResult.cancel<SupportData>()
                    }

                    if (content.isEmpty()) {
                        context.sendMessage(this.messages / "support-content-empty")
                        return@then StepExecutionResult.replay<SupportData>()
                    }

                    data.content = content.toString()
                    data.attachments = attachments
                    return@then StepExecutionResult.next(data)
                }
                .buildAndExecuteOrNull() ?: return

        context.sendMessage(this.messages / "support-publishing")

        try {
            val support = this.supports.create(
                    databaseUser,
                    result.title,
                    result.shortTitle!!,
                    result.content!!,
                    result.attachments!!,
                    this.keywords.findOrCreate(result.keywords!!)
            )

            context.sendMessageFormat(this.messages / "support-published", support.channelId)
        } catch (e: SupportService.TooManyOpenedSupportsException) {
            context.sendMessage(this.messages / "too-many-supports")
        } catch (e: SupportService.SupportChannelAlreadyExistsException) {
            context.sendMessage(this.messages / "support-short-title-in-use")
        }
    }

    @CommandDefinition(
            "support open <support-channel>",
            "Ré-ouvre un support archivé",
            "Ré-ouvre le support spécifié"
    )
    fun supportOpen(context: CommandContext, supportChannel: Channel) {
        val support = this.supports.findOneById(supportChannel.idLong)

        if (support == null) {
            context.sendMessage(this.messages / "channel-not-support")
            return
        }

        if (support.solvedDate == null) {
            context.sendMessage(this.messages / "support-opening-already-open")
            return
        }

        if (support.author != context.databaseUser && context.userPower < POWER_OPEN_OTHER_SUPPORT) {
            context.sendMessage(this.messages / "support-opening-insufficient-power")
            return
        }

        this.supports.open(support)

        context.sendMessage(this.messages / "support-opened")
    }

    @CommandDefinition(
            "support modify <support-channel>",
            "Modifie un support ouvert",
            "Permet de changer certains éléments d'un support ouvert"
    )
    fun supportModify(context: CommandContext, supportChannel: Channel) {
        if (context !is DiscordCommandContext) {
            context.sendMessage(this.messages / "command-not-executable-in-cli")
            return
        }

        val support = this.supports.findOneById(supportChannel.idLong)

        if (support == null) {
            context.sendMessage(this.messages / "channel-not-support")
            return
        }

        if (support.solvedDate != null) {
            context.sendMessage(this.messages / "support-edition-closed")
            return
        }

        if (support.author != context.databaseUser && context.userPower < POWER_UPDATE_OTHER_SUPPORT) {
            context.sendMessage(this.messages / "support-edition-insufficient-power")
            return
        }

        val result = this.wizards.wizardBuilder(
                this.messages / "support-edition-title" / context.databaseUser,
                context.discordMember.user,
                context.channel
        )
                .first { awaiter ->
                    val msg = context.channel
                            .sendMessage(this.messages / "support-edition-question" / context.databaseUser)
                            .complete()

                    val emotes = EditChoice.values().associateBy(EditChoice::string)

                    for (emote in emotes.keys) {
                        msg.addReaction(emote).complete()
                    }

                    var reaction: MessageReaction.ReactionEmote

                    do {
                        reaction = awaiter.waitForUserReaction<MessageReactionAddEvent>(msg, context.discordMember.user)
                                ?: return@first StepExecutionResult.cancel<EditChoice>()
                    } while (reaction.name !in emotes)

                    StepExecutionResult.next(emotes[reaction.name]!!)
                }
                .then { choice, awaiter ->
                    when (choice) {
                        EditChoice.TITLE -> {
                            context.sendMessage(this.messages / "support-title-question")

                            val title = awaiter.waitForUserMessage()?.contentRaw
                                    ?: return@then StepExecutionResult.cancel<Unit>()

                            if (title.length in 6..255) {
                                support.title = title
                                this.supports.update(support)
                                StepExecutionResult.next()
                            } else {
                                context.sendMessage(this.messages / "support-title-invalid-length")
                                StepExecutionResult.replay()
                            }
                        }

                        EditChoice.KEYWORDS -> {
                            context.sendMessage(this.messages / "support-keywords-question")

                            val message = awaiter.waitForUserMessage()?.contentRaw
                                    ?: return@then StepExecutionResult.cancel<Unit>()

                            val keywords = message.split(',', ';', ' ', '\t', '\n').asSequence().filter(String::isNotBlank).toSet()

                            when {
                                keywords.isEmpty() -> {
                                    context.sendMessage(this.messages / "support-keywords-empty")
                                    StepExecutionResult.replay()
                                }
                                keywords.size > MAX_KEYWORD_COUNT -> {
                                    context.sendMessage(this.messages / "support-keywords-too-many")
                                    StepExecutionResult.replay()
                                }
                                keywords.any { k -> Message.MentionType.values().any { it.pattern.matcher(k).matches() } } -> {
                                    context.sendMessage(this.messages / "support-keywords-no-mention")
                                    StepExecutionResult.replay()
                                }
                                else -> {
                                    support.keywords.clear()
                                    support.keywords += this.keywords.findOrCreate(keywords)
                                    this.supports.update(support)
                                    StepExecutionResult.next()
                                }
                            }
                        }

                        EditChoice.CONTENT -> {
                            context.sendMessage(this.messages / "support-content-question")

                            val content = StringBuilder()
                            val attachments = mutableListOf<SupportService.SupportAttachment>()
                            var message = awaiter.waitForUserMessage()
                                    ?: return@then StepExecutionResult.cancel<Unit>()

                            while (message.contentRaw != "!finish") {
                                for (attachment in message.attachments) {
                                    if (attachments.size == MAX_ATTACHMENT_COUNT) {
                                        context.sendMessage(this.messages / "support-content-too-much-files")
                                        break
                                    }

                                    attachments += ExistingAttachment(attachment)
                                }

                                if (Message.MentionType.values().any { it.pattern.matcher(message.contentRaw).find() }) {
                                    context.sendMessage(this.messages / "support-content-no-mention")
                                } else {
                                    content.appendln(message.contentRaw)

                                    if (content.length >= MAX_CONTENT_LENGTH) {
                                        content.setLength(MAX_CONTENT_LENGTH)
                                        context.sendMessage(this.messages / "support-content-too-long")
                                        break
                                    }
                                }

                                message = awaiter.waitForUserMessage()
                                        ?: return@then StepExecutionResult.cancel<Unit>()
                            }

                            if (content.isEmpty()) {
                                context.sendMessage(this.messages / "support-content-empty")
                                return@then StepExecutionResult.replay<Unit>()
                            }

                            this.supports.setContents(support, SupportContents(content.toString(), attachments))
                            StepExecutionResult.next()
                        }
                    }
                }
                .buildAndExecuteOrNull()

        if (result != null) {
            context.sendMessage(this.messages / "support-edition-done")
        }
    }

    @CommandDefinition(
            "support close <support-channel>",
            "Ferme un support",
            "Ferme le support spécifié"
    )
    fun supportClose(context: CommandContext, supportChannel: Channel) {
        if (context !is DiscordCommandContext) {
            context.sendMessage(this.messages / "command-not-executable-in-cli")
            return
        }

        val support = this.supports.findOneById(supportChannel.idLong)

        if (support == null) {
            context.sendMessage(this.messages / "channel-not-support")
            return
        }

        if (support.solvedDate != null) {
            context.sendMessage(this.messages / "support-closure-already-closed")
            return
        }

        val helpers: List<User> = if (support.author == context.databaseUser) {
            val title = this.messages / "support-closure-title" / context.databaseUser

            this.wizards.wizardBuilder(title, context.discordMember.user, context.channel)
                    .first {
                        context.sendMessage(this.messages / "support-closure-helpers-question")

                        val message = it.waitForUserMessage() ?: return@first StepExecutionResult.cancel<List<User>>()

                        if (message.contentRaw.equals("personne", true)) {
                            return@first StepExecutionResult.next<List<User>>(emptyList())
                        }

                        val usersNames = message.contentRaw.split("\\s+".toRegex())
                        val users = mutableListOf<User>()

                        for (userName in usersNames) {
                            val matcher = Message.MentionType.USER.pattern.matcher(userName)
                            val found = if (matcher.matches()) {
                                this.discord.guild.getMemberById(matcher.group(1).toLong())
                            } else {
                                this.discord.guild.getMembersByEffectiveName(userName, true).singleOrNull()
                            }

                            if (found == null) {
                                context.sendMessageFormat(this.messages / "support-closure-helpers-error", userName)
                                return@first StepExecutionResult.replay<List<User>>()
                            }

                            users += this.users.findOneById(found.user.idLong) ?: this.users.createDefault(found.user.idLong)
                        }

                        StepExecutionResult.next<List<User>>(users)
                    }
                    .buildAndExecuteOrNull() ?: return
        } else {
            if (context.userPower < POWER_CLOSE_OTHER_SUPPORT) {
                context.sendMessage(this.messages / "support-closure-insufficient-power")
                return
            }

            emptyList()
        }

        this.supports.close(support, helpers)

        context.sendMessage(this.messages / "support-closed")
    }

    @CommandDefinition(
            "support delete <support-channel>",
            "Supprime un support",
            "Supprime le support spécifié",
            POWER_DELETE_SUPPORT
    )
    fun supportDelete(context: CommandContext, supportChannel: Channel) {
        val support = this.supports.findOneById(supportChannel.idLong)

        if (support == null) {
            context.sendMessage(this.messages / "channel-not-support")
            return
        }

        this.supports.delete(support)
        context.sendMessage(this.messages / "support-deleted")
    }

    private enum class EditChoice(val string: String) {
        TITLE(EmojiManager.getForAlias("one").unicode),
        KEYWORDS(EmojiManager.getForAlias("two").unicode),
        CONTENT(EmojiManager.getForAlias("three").unicode)
    }
}
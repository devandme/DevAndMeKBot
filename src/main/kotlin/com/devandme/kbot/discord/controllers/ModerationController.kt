/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandDefinition
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.misc.POWER_CHANNEL_CLEAR
import com.devandme.kbot.misc.POWER_CHANNEL_SLOW
import com.devandme.kbot.misc.lastMessage
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import net.dv8tion.jda.core.entities.Channel
import net.dv8tion.jda.core.entities.TextChannel
import org.springframework.stereotype.Component

@DiscordController
@Component
class ModerationController(private val discord: DiscordService, messageService: MessageService) {

    private val messages = messageService / MessageStorage.MODERATION_CONTROLLER

    @CommandDefinition(
            "clear <channel> [amount]",
            "Faire du ménage dans les salons",
            "Supprime `amount` messages du canal",
            requiredPower = POWER_CHANNEL_CLEAR
    )
    fun clearCommand(context: CommandContext, channel: Channel, amount: Int = -1) {
        if (channel !is TextChannel) {
            context.sendMessage(this.messages / "no-text-channel")
            return
        }

        if (channel in this.discord.channelsIgnoringClearCommand) {
            context.sendMessage(this.messages / "immunize-from-clear-channel")
            return
        }

        val lastMessageId = channel.lastMessage?.idLong

        if (lastMessageId == null) {
            context.sendMessage(this.messages / "empty-channel")
            return
        }

        val history = channel.getHistoryBefore(lastMessageId, amount).complete()

        for (message in history.retrievedHistory + channel.lastMessage!!) {
            message.delete()
                    .reason("Cleanup requested by ${context.discordMember.effectiveName} ($amount)")
                    .complete()
        }

        channel.sendMessage((this.messages / "channel-clear").fmt(context.databaseUser, context.discordMember.asMention))
                .complete()
    }

    @CommandDefinition(
            "slowmode <channel> <time>",
            "Mettre un salon en mode ralenti",
            "Permet de forcer un intervalle de temps entre chaque message",
            requiredPower = POWER_CHANNEL_SLOW
    )
    fun slowCommand(context: CommandContext, channel: Channel, time: Int) {
        if (channel !is TextChannel) {
            context.sendMessage(this.messages / "no-text-channel")
            return
        }

        if (time !in 0..120) {
            context.sendMessage(this.messages / "slowmode-bad-time")
            return
        }

        channel.manager.setSlowmode(time).complete()

        if (time == 0) {
            context.sendMessage((this.messages / "slowmode-disabled").fmt(context.databaseUser, time))
        } else {
            context.sendMessage((this.messages / "slowmode-defined").fmt(context.databaseUser, time))
        }
    }
}
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.configuration.DiscordConfiguration
import com.devandme.kbot.database.entities.Banishment
import com.devandme.kbot.database.entities.Exclusion
import com.devandme.kbot.database.entities.Mute
import com.devandme.kbot.database.entities.Sanction
import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandDefinition
import com.devandme.kbot.discord.services.listeners.DiscordEventListener
import com.devandme.kbot.misc.*
import com.devandme.kbot.services.SanctionService
import com.devandme.kbot.services.UserService
import com.devandme.kbot.services.messages.LanguageLevel
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageEmbed
import net.dv8tion.jda.core.entities.User
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import org.springframework.transaction.annotation.Transactional
import java.awt.Color
import java.time.Duration
import java.time.ZonedDateTime

@Transactional
@DiscordController
class SanctionController(
        discordConfiguration: DiscordConfiguration,
        messageService: MessageService,
        val sanctions: SanctionService,
        val userService: UserService
) {

    private val commandPrefix = discordConfiguration.commandPrefix

    private val messages = messageService / MessageStorage.SANCTION_CONTROLLER

    @CommandDefinition(
            "sanction list <user>",
            "Affiche les sanctions reçues et prononcées par l'utilisateur",
            "Donne l'historique de toutes les sanctions liées à l'utilisateur",
            requiredPower = POWER_SANCTION_LIST
    )
    fun sanctionList(context: CommandContext, user: User) {
        val now = ZonedDateTime.now()
        val received = this.sanctions.findAll(SanctionService.SanctionFilter(target = user.idLong))
        val given = this.sanctions.findAll(SanctionService.SanctionFilter(moderator = user.idLong))

        fun sendSanctions(sanctions: Iterable<Sanction>, forceColor: Color? = null) {
            for (sanction in sanctions.take(10)) {
                // Is the sanction active?
                var active = false

                // Embed fields.
                val fields = mutableListOf<MessageEmbed.Field>()

                when (sanction) {
                    is Banishment, is Mute -> {
                        val endDate = if (sanction is Banishment) sanction.endDate else (sanction as Mute).endDate

                        // Ban duration.
                        val duration = if (endDate == null) {
                            this.messages / "infinite-duration" / LanguageLevel.NORMAL_FRENCH
                        } else {
                            formatDifference(endDate, sanction.startDate)
                        }

                        fields += MessageEmbed.Field(
                                this.messages / "start" / LanguageLevel.NORMAL_FRENCH,
                                sanction.startDate.format(FRENCH_SHORT_DATETIME),
                                false
                        )

                        fields += MessageEmbed.Field(
                                this.messages / "duration" / LanguageLevel.NORMAL_FRENCH,
                                duration,
                                false
                        )

                        active = sanction.startDate <= now && (endDate == null || endDate >= now)
                    }
                    is Exclusion -> {
                        fields += MessageEmbed.Field(
                                this.messages / "date" / LanguageLevel.NORMAL_FRENCH,
                                sanction.startDate.format(FRENCH_SHORT_DATETIME),
                                false
                        )
                    }
                    else -> throw RuntimeException("Unhandled case!")
                }

                context.sendMessage(
                        createEmbed(
                                title = this.messages / sanction::class.simpleName!! / LanguageLevel.NORMAL_FRENCH,
                                description = sanction.reason,
                                fields = fields.toTypedArray(),
                                color = forceColor ?: if (active) Color.RED else Color.GREEN
                        )
                )
            }
        }

        if (received.isEmpty()) {
            context.sendMessage(this.messages / "no-sanctions-received")
        } else {
            context.sendMessageFormat(
                    this.messages / "sanctions-received",
                    if (received.size == 100) "100+" else received
            )

            sendSanctions(received)
        }

        if (given.isEmpty()) {
            context.sendMessage(this.messages / "no-sanctions-given")
        } else {
            context.sendMessageFormat(
                    this.messages / "sanctions-given",
                    if (received.size == 100) "100+" else received
            )

            sendSanctions(given, Color.CYAN)
        }
    }

    @CommandDefinition(
            "mute <user> <duration> <reason...>",
            "Réduire un utilisateur au silence",
            "Permet de rendre un utilisateur silencieux",
            requiredPower = POWER_SANCTION_MUTE
    )
    fun mute(context: CommandContext, user: Member, duration: Duration, reason: List<String>) {
        val target = this.userService.findOneById(user.user.idLong)
                ?: this.userService.createDefault(user.user.idLong)

        if (context.databaseUser.id == target.id) {
            context.sendMessage(this.messages / "bad-target")
            return
        }

        if (context.userPower <= this.userService.getTotalPower(target, user)) {
            context.sendMessage(this.messages / "not-enough-power")
            return
        }

        val reasonText = reason.joinToString(" ")

        if (reasonText.isBlank()) {
            context.sendMessage(this.messages / "empty-reason")
            return
        }

        val now = ZonedDateTime.now()

        this.sanctions.save(
                Mute(
                        reason = reasonText,
                        moderator = context.databaseUser,
                        target = target,
                        startDate = now,
                        endDate = now + duration
                )
        )

        context.sendMessageFormat(messages / "muted", user.effectiveName)
    }

    @CommandDefinition(
            "unmute <user>",
            "Redonner la parole à un utilisateur",
            "Permet de rendre la parole à un utilisateur",
            requiredPower = POWER_SANCTION_MUTE
    )
    fun unmute(context: CommandContext, user: Member) {
        // If the target was not found, just return
        val target = this.userService.findOneById(user.user.idLong)

        if (target == null) {
            context.sendMessage(this.messages / "not-muted")
            return
        }

        val activeMutes = this.sanctions.getActiveMutes(target)

        if (activeMutes.isEmpty()) {
            context.sendMessage(this.messages / "not-muted")
            return
        }

        for (mute in activeMutes) {
            this.sanctions.delete(mute)
        }

        context.sendMessageFormat(this.messages / "unmuted", user.effectiveName)
    }

    @CommandDefinition(
            "kick <user> <reason...>",
            "Exclure un utilisateur",
            "Permet d'exclure un utilisateur",
            requiredPower = POWER_SANCTION_KICK
    )
    fun kick(context: CommandContext, user: Member, reason: List<String>) {
        val target = userService.findOneById(user.user.idLong) ?: this.userService.createDefault(user.user.idLong)

        if (context.databaseUser.id == target.id) {
            context.sendMessage(this.messages / "bad-target")
            return
        }

        if (context.userPower <= this.userService.getTotalPower(target, user)) {
            context.sendMessage(this.messages / "not-enough-power")
            return
        }

        val reasonText = reason.joinToString(" ")

        if (reasonText.isBlank()) {
            context.sendMessage(this.messages / "empty-reason")
            return
        }

        this.sanctions.save(
                Exclusion(
                        reason = reasonText,
                        moderator = context.databaseUser,
                        target = target,
                        startDate = ZonedDateTime.now()
                )
        )

        context.sendMessageFormat(this.messages / "kicked", user.effectiveName)
    }

    @CommandDefinition(
            "ban <user> <duration> <reason...>",
            "Bannir un utilisateur",
            "Permet de bannir un utilisateur",
            requiredPower = POWER_SANCTION_BAN
    )
    fun ban(context: CommandContext, user: Member, duration: Duration, reason: List<String>) {
        val target = this.userService.findOneById(user.user.idLong) ?: this.userService.createDefault(user.user.idLong)

        if (context.databaseUser.id == target.id) {
            context.sendMessage(this.messages / "bad-target")
            return
        }

        if (context.userPower <= this.userService.getTotalPower(target, user)) {
            context.sendMessage(this.messages / "not-enough-power")
            return
        }

        val reasonText = reason.joinToString(" ")

        if (reasonText.isBlank()) {
            context.sendMessage(this.messages / "empty-reason")
            return
        }

        val now = ZonedDateTime.now()

        this.sanctions.save(
                Banishment(
                        reason = reasonText,
                        moderator = context.databaseUser,
                        target = target,
                        startDate = now,
                        endDate = if (duration == INFINITE_DURATION) null else now + duration
                )
        )

        context.sendMessageFormat(
                this.messages / "banned",
                user.effectiveName,
                if (duration == INFINITE_DURATION) this.messages / "infinite-duration" / target else format(duration)
        )
    }

    @CommandDefinition(
            "unban <user>",
            "Révoquer le bannissement d'un utilisateur",
            "Permet de révoquer le bannissement à un utilisateur",
            requiredPower = POWER_SANCTION_MUTE
    )
    fun unban(context: CommandContext, user: String) {
        val matcher = Message.MentionType.USER.pattern.matcher(user)
        val id = if (matcher.matches()) matcher.group(1).toLong() else user.toLongOrNull()

        if (id == null) {
            context.sendMessage(this.messages / "invalid-id")
            return
        }

        val target = this.userService.findOneById(id)

        if (target == null) {
            context.sendMessage(this.messages / "not-banned")
            return
        }

        val activeBans = this.sanctions.getActiveBans(target)

        if (activeBans.isEmpty()) {
            context.sendMessage(this.messages / "not-banned")
            return
        }

        for (ban in activeBans) {
            this.sanctions.delete(ban)
        }

        context.sendMessageFormat(this.messages / "unbanned")
    }

    @DiscordEventListener
    fun messageSent(event: MessageReceivedEvent) {
        // Ignore private messages.
        event.guild ?: return

        // If not a command and user is muted,
        if (!event.message.contentRaw.startsWith(this.commandPrefix) && this.sanctions.isMuted(event.member)) {
            event.message.delete().complete()

            event.author.openPrivateChannel().queue {
                val target = this.userService.findOneById(event.author.idLong)
                        ?: this.userService.createDefault(event.author.idLong)

                it.sendMessage(this.messages / "message-deleted-user-muted" / target).complete()
            }
        }
    }
}
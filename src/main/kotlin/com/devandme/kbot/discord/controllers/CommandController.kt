/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.configuration.DiscordConfiguration
import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.DiscordCommandContext
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.discord.services.listeners.DiscordEventListener
import com.devandme.kbot.services.CommandExecutorService
import com.devandme.kbot.services.UserService
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import net.dv8tion.jda.core.entities.PrivateChannel
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import kotlin.math.min

@DiscordController
class CommandController(
    discordConfiguration: DiscordConfiguration,
    messageService: MessageService,
    private val commandExecutor: CommandExecutorService,
    private val discord: DiscordService,
    private val users: UserService
) {

    private val messages = messageService / MessageStorage.COMMAND_CONTROLLER

    private val commandPrefix = discordConfiguration.commandPrefix

    private val spamChannel = this.discord.spamChannel

    @DiscordEventListener(0)
    fun onMessageSent(event: MessageReceivedEvent): Boolean {
        var messageContent = event.message.contentRaw.trim()

        // Ignore messages that does not start with the command prefix.
        if (!messageContent.startsWith(this.commandPrefix)) {
            return false
        }

        messageContent = messageContent.substring(this.commandPrefix.length)

        // If the user is null, then we've a non-member message sender. We don't want this.
        val discordUser = event.member ?: this.discord.guild.getMember(event.author) ?: return true
        val databaseUser = this.users.findOneById(event.author.idLong) ?: this.users.createDefault(event.author.idLong)
        val power = min(this.discord.getRolesPower(discordUser) + databaseUser.power, 100)
        val context = DiscordCommandContext(databaseUser, discordUser, power, event.channel)

        if (event.channel !is PrivateChannel && event.channel.name != this.spamChannel.name) {
            event.author
                .openPrivateChannel()
                .queue {
                    it.sendMessage(
                        (this.messages / "command-not-in-spam-or-private")
                            .fmt(context.databaseUser, this.spamChannel.id)
                    ).queue()
                }
            return true
        }

        this.commandExecutor.execute(context, messageContent)

        return true
    }
}
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.configuration.DiscordConfiguration
import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.Command
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandDefinition
import com.devandme.kbot.discord.services.commands.CommandRegistryService
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.services.messages.LanguageLevel
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage

@DiscordController
class HelpController(
        messageService: MessageService,
        discordConfiguration: DiscordConfiguration,
        private val discord: DiscordService,
        private val commands: CommandRegistryService
) {

    private val messages = messageService / MessageStorage.HELP_CONTROLLER
    private val commandPrefix = discordConfiguration.commandPrefix

    @CommandDefinition(
            "help",
            "Obtient de l'aide",
            "Permet d'obtenir des informations générales sur l'utilisation du bot"
    )
    fun generalHelp(context: CommandContext) {
        context.sendMessageFormat(this.messages / "help-general", this.commandPrefix)
    }

    @CommandDefinition(
            "help topic [topic]",
            "Obtient de l'aide sur un sujet en particulier",
            "Permet de récupérer des informations sur un sujet"
    )
    fun topicHelp(context: CommandContext, topic: String?) {
        if (topic == null) {
            context.sendMessage(this.messages / "help-topic-list")
            return
        }

        when (topic.toLowerCase()) {
            "commands" -> context.sendMessageFormat(
                    this.messages / "help-commands" / LanguageLevel.NORMAL_FRENCH,
                    this.commandPrefix
            )

            "presentation" -> context.sendMessageFormat(
                    this.messages / "help-presentation" / LanguageLevel.NORMAL_FRENCH,
                    this.discord.presentationChannel.asMention
            )

            "supports" -> context.sendMessageFormat(
                    this.messages / "help-support" / LanguageLevel.NORMAL_FRENCH,
                    this.commandPrefix
            )

            else -> context.sendMessage(this.messages / "help-topic-not-found")
        }
    }

    @CommandDefinition(
            "help command <command-name>",
            "Obtient de l'aide sur une commande en particulier",
            "Permet de récupérer des informations sur une commande"
    )
    fun commandHelp(context: CommandContext, commandName: String) {
        val commands = this.commands.findCommandsByName(commandName, context.userPower)

        if (commands.isEmpty()) {
            context.sendMessage(this.messages / "no-commands-found")
            return
        }

        val names = commands.first().names
        val result = StringBuilder()

        fun appendCommandSyntax(command: Command) {
            result.append("\t`").append(this.commandPrefix).append(command.syntax).append("` ").appendln(command.title)
            result.append('\t')
            result.appendln((this.messages / "commands-search-description").fmt(LanguageLevel.NORMAL_FRENCH, command.description))
            result.appendln()
        }

        result.appendln((this.messages / "commands-search-title").fmt(LanguageLevel.NORMAL_FRENCH, names.first()))

        if (names.size > 1) {
            result.appendln((this.messages / "commands-search-aliases").fmt(LanguageLevel.NORMAL_FRENCH, names.drop(1).joinToString()))
        }

        if (commands.size == 1) {
            appendCommandSyntax(commands.single())
        } else {
            result.appendln((this.messages / "commands-search-count").fmt(LanguageLevel.NORMAL_FRENCH, commands.size))
            commands.forEach(::appendCommandSyntax)
        }

        context.sendMessage(result.toString())
    }

    @CommandDefinition(
            "help search-command <keyword>",
            "Recherche des commandes",
            "Permet de rechercher une ou plusieurs commande(s) à l'aide d'un mot-clef"
    )
    fun commandSearchHelp(context: CommandContext, keyword: String) {
        val limit = 20
        val commands = this.commands.findCommandsContaining(keyword, context.databaseUser.power)

        if (commands.isEmpty()) {
            context.sendMessage(this.messages / "no-commands-found")
            return
        }

        if (commands.size > limit) {
            context.sendMessageFormat(this.messages / "too-much-commands-found", limit)
        }

        val result = StringBuilder()

        result.appendln(this.messages / "commands-found" / context.databaseUser)

        for (command in commands.take(limit)) {
            result.append("\t- `").append(command.syntax).append("` : ").appendln(command.title)
        }

        context.sendMessage(result.toString())
    }
}
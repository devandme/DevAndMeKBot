/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandDefinition
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import org.springframework.boot.SpringApplication
import org.springframework.context.ApplicationContext

@DiscordController
class GeneralCommandsController(messageService: MessageService, private val applicationContext: ApplicationContext) {

    private val messages = messageService / MessageStorage.GENERAL_COMMANDS_CONTROLLER

    @CommandDefinition("stop", "Arrête le bot", "Procède à l'arrêt du bot Discord", requiredPower = 100)
    fun stop(context: CommandContext) {
        context.sendMessage(this.messages / "stopping")
        SpringApplication.exit(this.applicationContext)
    }
}
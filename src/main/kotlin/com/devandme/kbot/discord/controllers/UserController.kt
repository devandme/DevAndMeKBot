/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.controllers

import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandDefinition
import com.devandme.kbot.misc.createEmbed
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.MessageEmbed
import javax.transaction.Transactional

@Transactional
@DiscordController
class UserController(messageService: MessageService) {

    private val messages = messageService / MessageStorage.USER_CONTROLLER

    @CommandDefinition(
        "me <member>",
        "Affiche des informations sur le profil",
        "Permet d'obtenir des informations comme le power, les rôles et l'expérience"
    )
    fun me(ctx: CommandContext, member: Member) {
        ctx.sendMessage(
            createEmbed(
                author = member.user,
                fields = arrayOf(
                    MessageEmbed.Field(this.messages / "alias" / ctx.databaseUser, member.effectiveName, false),
                    MessageEmbed.Field(this.messages / "power" / ctx.databaseUser, ctx.userPower.toString(), false)
                )
            )
        )
    }
}
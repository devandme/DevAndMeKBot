/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.commands

import com.devandme.kbot.discord.services.commands.parser.*
import com.devandme.kbot.misc.nameWithClass
import java.lang.reflect.InvocationTargetException
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.instanceParameter
import kotlin.reflect.jvm.jvmErasure

/**
 * A command that can be executed.
 *
 * @property syntax The command's syntax.
 * @property name The command's name.
 * @property title The command's title.
 * @property description The command's description.
 * @property function The command's function.
 * @property location The command's location (class + method name).
 * @property parameters The command's parameters.
 */
class Command(val function: KFunction<*>, private val controller: Any) {

    val syntax: String

    val requiredPower: Int

    val names: List<String>

    val title: String

    val description: String

    val location: String
        get() = this.function.nameWithClass

    private val parameters: Array<CommandParameter>

    init {
        val annotation = this.function.findAnnotation<CommandDefinition>()
            ?: throw IllegalArgumentException("The function must have the CommandDefinition annotation!")

        this.syntax = normalizeSyntax(annotation.syntax)
        this.requiredPower = annotation.requiredPower
        this.title = annotation.title
        this.description = annotation.description
        this.parameters = matchSyntaxToFunction(parseSyntax(this.syntax), this.function)
        this.names = listOf((this.parameters.first() as ConstantCommandParameter).value) +
                annotation.aliases.map { it.trim() }
    }

    /**
     * Tests whether this command can be called with the supplied parameters.
     *
     * @param arguments The arguments.
     * @return `true` if the command can be called with the arguments, `false` otherwise.
     */
    fun canBeCalledWith(arguments: List<String>): Boolean {
        if (this.parameters.none { it is VarargsCommandParameter } &&
            arguments.size > this.parameters.count { it !is CommandContextParameter }) {
            return false
        }

        for ((i, commandParameter) in this.parameters.withIndex()) {
            val suppliedParameter = arguments.getOrNull(i)

            if (commandParameter is ConstantCommandParameter) {
                if (i == 0 && suppliedParameter !in this.names) {
                    return false
                } else if (i > 0 && suppliedParameter != commandParameter.value) {
                    return false
                }
            } else if (commandParameter is RequiredCommandParameter && suppliedParameter == null) {
                return false
            } else if (commandParameter is VarargsCommandParameter) {
                return true
            }
        }

        return true
    }

    /**
     * Calls the command.
     *
     * @param parameters The list of the command parameters.
     * @param context The command context.
     * @param converter A service that c
     * @throws CommandExecutionException If an error occurs during the command execution.
     * @throws Exception Indicates an internal error.
     */
    @Throws(CommandExecutionException::class)
    fun call(
        parameters: List<String>,
        context: CommandContext,
        converter: ParameterConverter
    ) {
        if (!this.canBeCalledWith(parameters)) {
            throw IncompatibleCommandParametersException()
        }

        if (context.userPower < this.requiredPower) {
            throw InsufficientPowerException(context.userPower, this.requiredPower)
        }

        val functionParameters = mutableMapOf<KParameter, Any?>()

        functionParameters[this.function.instanceParameter!!] = this.controller

        val commandContextParameter =
            this.parameters.firstOrNull { it is CommandContextParameter } as CommandContextParameter?
        val paramsWithoutCommandContext = this.parameters.filter { it !is CommandContextParameter }

        if (commandContextParameter != null) {
            functionParameters[commandContextParameter.parameter] = context
        }

        for ((i, cmdParam) in paramsWithoutCommandContext.withIndex()) {
            var providedParam = parameters.getOrNull(i)

            if (cmdParam is OptionalCommandParameter) {
                if (providedParam == null) {
                    if (!cmdParam.parameter.isOptional && cmdParam.parameter.type.isMarkedNullable) {
                        functionParameters[cmdParam.parameter] = null
                    }
                } else {
                    try {
                        functionParameters[cmdParam.parameter] =
                                converter.convert(providedParam, cmdParam.type.jvmErasure)
                    } catch (conversionException: ParameterConversionException) {
                        throw ParameterConversionException(conversionException.targetType, i, cmdParam.name)
                    }
                }
            } else if (cmdParam is RequiredCommandParameter) {
                try {
                    functionParameters[cmdParam.parameter] =
                            converter.convert(providedParam!!, cmdParam.type.jvmErasure)
                } catch (conversionException: ParameterConversionException) {
                    throw ParameterConversionException(conversionException.targetType, i, cmdParam.name)
                }
            } else if (cmdParam is VarargsCommandParameter) {
                val listType = cmdParam.type.arguments.single().type!!.jvmErasure
                val result = mutableListOf<Any>()

                for (j in i until parameters.size) {
                    providedParam = parameters[j]

                    try {
                        result += converter.convert(providedParam, listType)
                    } catch (conversionException: ParameterConversionException) {
                        throw ParameterConversionException(conversionException.targetType, j, cmdParam.name)
                    }
                }

                functionParameters[cmdParam.parameter] = result
            }
        }

        try {
            this.function.callBy(functionParameters)
        } catch (exception: InvocationTargetException) {
            throw exception.cause ?: exception
        }
    }

    override fun equals(other: Any?) = other is Command && other.syntax == this.syntax
    override fun hashCode() = this.syntax.hashCode()

    /**
     * Interface required to convert parameters from the user's string to the requested type.
     */
    interface ParameterConverter {

        /**
         * Converts the [String] value to the provided type.
         *
         * @param value The [String] value.
         * @param toType The target [KClass].
         * @throws ParameterConversionException If an error occurs during a parameter conversion.
         */
        @Throws(ParameterConversionException::class)
        fun convert(value: String, toType: KClass<*>): Any
    }
}
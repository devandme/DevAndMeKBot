/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.discordWizard

/**
 * Represents a [Wizard] step.
 *
 * @property action The action to execute.
 * @param T The return type of the previous step.
 * @param U The return type of this step.
 */
class Step<T, U>(private val action: (previousStepResult: T, awaiter: StepAwaiter) -> StepExecutionResult<U>) {

    /**
     * Executes the step.
     */
    fun execute(previousStepResult: T, awaiter: StepAwaiter) = this.action(previousStepResult, awaiter)
}
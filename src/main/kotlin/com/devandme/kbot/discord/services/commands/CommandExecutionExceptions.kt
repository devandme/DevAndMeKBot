/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.commands

import kotlin.reflect.KClass

/**
 * Exception thrown when an error occurs during a command execution.
 */
abstract class CommandExecutionException(message: String? = null, cause: Throwable? = null) : Exception(message, cause)

/**
 * The provided parameters does not match the command parameters.
 */
class IncompatibleCommandParametersException : CommandExecutionException()

/**
 * An argument couldn't be converted from a user parameter string.
 *
 * @property targetType The type targeted for the conversion.
 * @property index The parameter index.
 * @property parameterName The name of the parameter.
 */
class ParameterConversionException(val targetType: KClass<*>, val index: Int = -1, val parameterName: String = "") :
    CommandExecutionException("Couldn't convert String to ${targetType.simpleName}!")

/**
 * Thrown when a user hasn't enough power to execute a command.
 */
class InsufficientPowerException(val userPower: Int, val requiredPower: Int) : CommandExecutionException()
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.discordWizard

/**
 * Represents the result of a step execution.
 */
abstract class StepExecutionResult<T> {
    companion object {
        /**
         * Creates a new [StepExecutionResult] that replays the current step.
         */
        fun <T> replay(): StepExecutionResult<T> = ReplayExecutionResult()

        /**
         * Creates a new [StepExecutionResult] that cancels the wizard.
         */
        fun <T> cancel(): StepExecutionResult<T> = CancellationExecutionResult()

        /**
         * Creates a new [StepExecutionResult] that goes to the next step.
         *
         * @param result The step result.
         */
        fun <T> next(result: T): StepExecutionResult<T> = NextExecutionResult(result)

        /**
         * Creates a new [StepExecutionResult] that goes to the next step.
         */
        fun next(): StepExecutionResult<Unit> = NextExecutionResult(Unit)
    }
}

class NextExecutionResult<T>(val result: T) : StepExecutionResult<T>()
class CancellationExecutionResult<T> : StepExecutionResult<T>()
class ReplayExecutionResult<T> : StepExecutionResult<T>()
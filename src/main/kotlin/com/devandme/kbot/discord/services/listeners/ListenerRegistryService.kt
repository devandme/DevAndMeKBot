/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.listeners

import com.devandme.kbot.discord.DiscordController
import com.devandme.kbot.discord.services.commands.Command
import com.devandme.kbot.discord.services.discord.DiscordService
import net.dv8tion.jda.core.events.Event
import org.slf4j.Logger
import org.springframework.aop.support.AopUtils
import org.springframework.beans.factory.getBeansWithAnnotation
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Service
import kotlin.reflect.jvm.kotlinFunction

/**
 * @property listeners The registered [Command]s.
 */
@Service
final class ListenerRegistryService(private val logger: Logger, discord: DiscordService) :
    ApplicationListener<ContextRefreshedEvent> {

    private val listeners = mutableListOf<Listener>()

    init {
        discord.listeners += listener@{ event: Event ->
            for (listener in this.listeners) {
                if (listener.callIfPossible(event)) {
                    return@listener true
                }
            }

            return@listener false
        }
    }

    /**
     * Application context refresh listener. **For internal use only!**
     */
    override fun onApplicationEvent(event: ContextRefreshedEvent): Unit =
        synchronized(this.listeners) {
            this.logger.info("Context refreshed, searching for listeners...")
            this.listeners.clear()

            // Create a list of the registered commands.
            val listeners = event.applicationContext.getBeansWithAnnotation<DiscordController>().values.asSequence()
                // Get bean class along with its instance. The pair is used to keep them both.
                // Don't try to optimize using HashMaps since we don't know if the bean implements the equals/hashCode
                // method properly.
                .map { AopUtils.getTargetClass(it) to it }
                // Map each couple (class, bean instance) to another couple (method, instance).
                // We try to get KFunctions from the Java methods.
                // flatMap is used here to avoid nested collections, since each class has several methods.
                .flatMap { (clazz, inst) -> clazz.methods.asSequence().map { it.kotlinFunction to inst } }
                // Filter methods that don't have any Kotlin equivalent (KFunction), since the property
                // Class<*>.kotlinFunction returns null when not found. Additionally, remove all functions that doesn't
                // have the @CommandDefinition annotation.
                .filter { (kFunction) -> kFunction != null && kFunction.annotations.any { it is DiscordEventListener } }
                // Now we're sure that we've non-null functions and the bean instances.
                // So let's create Listeners :D
                .map { (kFunction, controller) -> Listener(kFunction!!, controller) }
                // Sort by order.
                .sortedBy { it.order }
                .toList()

            this.listeners += listeners
            this.logger.info("Finished listener search, found {} listeners.", this.listeners.size)
        }
}
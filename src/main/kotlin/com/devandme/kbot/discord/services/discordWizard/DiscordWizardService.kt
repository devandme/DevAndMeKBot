/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.discordWizard

import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.services.UserService
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.User
import org.slf4j.Logger
import org.springframework.stereotype.Service

/**
 * Manages Discord wizards.
 *
 * @property executingWizards List of the executing wizards, by user ID.
 */
@Service
class DiscordWizardService(
    messageService: MessageService,
    private val logger: Logger,
    private val users: UserService,
    private val discordService: DiscordService
) {
    private val messages = messageService / MessageStorage.DISCORD_WIZARD_SERVICE
    private val executingWizards = mutableMapOf<Long, Wizard<*>>()

    /**
     * Runs the provided wizard.
     *
     * @param title The wizard's title.
     * @param targetUser The wizard's targeted user.
     * @param targetChannel The wizard's targeted channel.
     */
    fun wizardBuilder(title: String, targetUser: User, targetChannel: MessageChannel) =
        FirstStepWizardBuilder(this, title, targetUser, targetChannel)

    /**
     * Executes the provided [Wizard].
     *
     * @throws IllegalStateException If a wizard is already running for the targeted user.
     */
    fun <T> execute(wizard: Wizard<out T>): T {
        val userId = wizard.targetUser.idLong
        val databaseUser = this.users.findOneById(userId) ?: this.users.createDefault(userId)

        synchronized(this.executingWizards) {
            if (this.executingWizards.containsKey(wizard.targetUser.idLong)) {
                val title = this.executingWizards[userId]!!.title
                throw IllegalStateException("User $userId already has a running wizard titled '$title'!")
            }

            this.executingWizards[userId] = wizard
        }

        try {
            wizard.targetChannel.sendMessage((this.messages / "started").fmt(databaseUser, wizard.title)).complete()

            return wizard.execute(this.discordService)
        } catch (e: WizardCancelledException) {
            wizard.targetChannel.sendMessage(this.messages / "cancelled" / databaseUser).complete()
            throw e
        } catch (e: InterruptedException) {
            throw e
        } catch (e: Exception) {
            wizard.targetChannel.sendMessage(this.messages / "internal-error" / databaseUser).complete()
            this.logger.error("Couldn't execute wizard '{}' due to an unexpected error:", wizard.title, e)
            throw e
        } finally {
            synchronized(this.executingWizards) {
                this.executingWizards -= userId
            }
        }
    }
}
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.commands

import com.devandme.kbot.discord.DiscordController
import org.springframework.aop.support.AopUtils
import org.springframework.beans.factory.getBeansWithAnnotation
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.*
import kotlin.reflect.jvm.kotlinFunction

/**
 * @property commands The registered [Command]s.
 */
@Service
class CommandRegistryService : ApplicationListener<ContextRefreshedEvent> {

    private val commands = mutableSetOf<Command>()

    /**
     * Finds all available [Command]s.
     *
     * @param power The maximum power.
     * @param page The page, if any.
     * @return The matching [Command]s.
     */
    fun findAll(power: Int, page: Pageable = Pageable.unpaged()): List<Command> = synchronized(this.commands) {
        val commands = this.commands.filter { it.requiredPower <= power }

        if (page.isUnpaged) {
            return commands
        }

        return commands.drop(page.offset.toInt()).take(page.pageSize)
    }

    /**
     * Finds [Command]s by its name.
     *
     * @param name The [Command] name.
     * @param minimumPower A minimum power for the command execution.
     * @return The [Command] or `null` if none match.
     */
    fun findCommandsByName(name: String, minimumPower: Int, page: Pageable = Pageable.unpaged()): List<Command> =
            synchronized(this.commands) {
                val commands = this.commands.filter { name in it.names && it.requiredPower <= minimumPower }

                if (page.isUnpaged) {
                    commands
                } else {
                    commands.asSequence().drop(page.offset.toInt()).take(page.pageSize).toList()
                }
            }

    /**
     * Finds [Command]s containing the provided search keyword.
     *
     * @param keyword The keyword.
     * @param minimumPower A minimum power for the command execution.
     * @param page The page.
     * @return The matching [Command]s.
     */
    fun findCommandsContaining(keyword: String, minimumPower: Int, page: Pageable = Pageable.unpaged()) =
            synchronized(this.commands) {
                val k = keyword.toLowerCase()
                val result = (this.commands.filter { it.requiredPower <= minimumPower && k in it.names } +
                        this.commands.filter { it.requiredPower <= minimumPower && k in it.title.toLowerCase() } +
                        this.commands.filter { it.requiredPower <= minimumPower && k in it.description.toLowerCase() }).distinct()

                if (page.isUnpaged) {
                    result
                } else {
                    result.asSequence().drop(page.offset.toInt()).take(page.pageSize).toList()
                }
            }

    /**
     * Finds a single [Command] thanks to the provided parameters.
     */
    fun findCommandByParameters(parameters: List<String>): Command? =
            synchronized(this.commands) {
                try {
                    this.commands.single { it.canBeCalledWith(parameters) }
                } catch (noSuchElementException: NoSuchElementException) {
                    null
                } catch (illegalArgumentException: IllegalArgumentException) {
                    throw RuntimeException("Two commands for the same syntax were found, this is a bug!")
                }
            }

    /**
     * Application context refresh listener. **For internal use only!**
     */
    override fun onApplicationEvent(event: ContextRefreshedEvent): Unit =
            synchronized(this.commands) {
                this.commands.clear()

                // Create a list of the registered commands.
                val commands = event.applicationContext.getBeansWithAnnotation<DiscordController>().values.asSequence()
                        // Get bean class along with its instance. The pair is used to keep them both.
                        // Don't try to optimize using HashMaps since we don't know if the bean implements the equals/hashCode
                        // method properly.
                        .map { AopUtils.getTargetClass(it) to it }
                        // Map each couple (class, bean instance) to another couple (method, instance).
                        // We try to get KFunctions from the Java methods.
                        // flatMap is used here to avoid nested collections, since each class has several methods.
                        .flatMap { (clazz, inst) -> clazz.methods.asSequence().map { it.kotlinFunction to inst } }
                        // Filter methods that don't have any Kotlin equivalent (KFunction), since the property
                        // Class<*>.kotlinFunction returns null when not found. Additionally, remove all functions that doesn't
                        // have the @CommandDefinition annotation.
                        .filter { (kFunction) -> kFunction != null && kFunction.annotations.any { it is CommandDefinition } }
                        // Now we're sure that we've non-null functions and the bean instances.
                        // So let's create Commands :D
                        .map { (kFunction, controller) -> Command(kFunction!!, controller) }
                        .toList()

                // Let's find probable bugs!

                // Find duplicate commands ( = 2 (or more) commands with the same syntax).
                val duplicates = commands.groupBy { it.syntax }.filterValues { it.size > 1 }.values

                if (duplicates.isNotEmpty()) {
                    val message = duplicates.joinToString("; ") { declarations ->
                        "Command '${declarations.first().names.first()}' has been declared ${declarations.size} times in: " +
                                declarations.joinToString(", ", transform = Command::location)
                    }

                    throw DuplicateCommandException(message)
                }

                this.commands.addAll(commands)
            }

    class DuplicateCommandException(message: String) : Exception(message)
}
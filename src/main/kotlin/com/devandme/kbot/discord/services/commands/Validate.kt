/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.commands

/**
 * Allows to validate a [String] parameter.
 *
 * @property errorMessage An error message to show to the user.
 * @property regex A regex to use for the validation.
 * @property minSize A minimum size for the [String].
 * @property maxSize A maximum size for the [String].
 * @see CommandDefinition
 */
@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
annotation class Validate(
    val errorMessage: String,
    val regex: String = "",
    val minSize: Int = -1,
    val maxSize: Int = -1
)
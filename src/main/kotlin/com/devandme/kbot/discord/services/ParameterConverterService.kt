/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services

import com.devandme.kbot.discord.services.commands.Command
import com.devandme.kbot.discord.services.commands.ParameterConversionException
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.misc.*
import net.dv8tion.jda.core.entities.*
import org.springframework.stereotype.Service
import java.text.ParseException
import java.time.*
import java.time.format.DateTimeParseException
import java.time.zone.ZoneRulesException
import kotlin.reflect.KClass

@Service
class ParameterConverterService(private val discord: DiscordService) : Command.ParameterConverter {

    private val userMentionRegex = Message.MentionType.USER.pattern.toRegex()
    private val roleMentionRegex = Message.MentionType.ROLE.pattern.toRegex()
    private val channelMentionRegex = Message.MentionType.CHANNEL.pattern.toRegex()

    private val booleanValues = hashMapOf(
        // === true/false - t/f ===
        "true" to true,
        "false" to false,
        "t" to true,
        "f" to false,
        // === yes/no - y/n ===
        "yes" to true,
        "no" to false,
        "y" to true,
        "n" to false,
        // === on/off ===
        "on" to true,
        "off" to false,
        // === oui/non - o/n ===
        "oui" to true,
        "non" to false,
        "o" to true,
        "n" to false,
        // === binary (wtf) ===
        "1" to true,
        "0" to false
    )

    override fun convert(value: String, toType: KClass<*>): Any = when (toType) {
        Int::class -> value.toIntOrNull() ?: throw ParameterConversionException(toType)
        Long::class -> value.toIntOrNull() ?: throw ParameterConversionException(toType)
        Float::class -> value.toFloatOrNull() ?: throw ParameterConversionException(toType)
        Double::class -> value.toDoubleOrNull() ?: throw ParameterConversionException(toType)
        Boolean::class -> this.booleanValues[value.toLowerCase()] ?: throw ParameterConversionException(toType)
        String::class -> value
        Member::class -> {
            val match = this.userMentionRegex.matchEntire(value)
            val userId = if (match == null) value.toLongOrNull() else match.groupValues[1].toLong()

            if (userId == null) {
                this.discord.guild.getMembersByName(value, true).singleOrNull()
                    ?: this.discord.guild.getMembersByNickname(value, true).singleOrNull()
                    ?: throw ParameterConversionException(toType)
            } else {
                this.discord.guild.getMemberById(userId)
                    ?: this.discord.guild.getMembersByName(value, true).singleOrNull()
                    ?: this.discord.guild.getMembersByNickname(value, true).singleOrNull()
                    ?: throw ParameterConversionException(toType)
            }
        }
        User::class -> {
            val match = this.userMentionRegex.matchEntire(value)
            val userId = if (match == null) value.toLongOrNull() else match.groupValues[1].toLong()

            if (userId == null) {
                this.discord.jda.getUsersByName(value, true).singleOrNull()
                    ?: throw ParameterConversionException(toType)
            } else {
                this.discord.jda.getUserById(userId)
                    ?: this.discord.jda.getUsersByName(value, true).singleOrNull()
                    ?: throw ParameterConversionException(toType)
            }
        }
        Role::class -> {
            val match = this.roleMentionRegex.matchEntire(value)
            val roleId = if (match == null) value.toLongOrNull() else match.groupValues[1].toLong()

            if (roleId == null) {
                this.discord.guild.getRolesByName(value, true).singleOrNull()
                    ?: throw ParameterConversionException(toType)
            } else {
                this.discord.guild.getRoleById(roleId)
                    ?: this.discord.guild.getRolesByName(value, true).singleOrNull()
                    ?: throw ParameterConversionException(toType)
            }
        }
        Channel::class -> {
            val match = this.channelMentionRegex.matchEntire(value)
            val channelId = if (match == null) value.toLongOrNull() else match.groupValues[1].toLong()

            if (channelId == null) {
                this.discord.guild.getTextChannelsByName(value, true).singleOrNull()
                    ?: throw ParameterConversionException(toType)
            } else {
                this.discord.guild.getTextChannelById(channelId)
                    ?: this.discord.guild.getTextChannelsByName(value, true).singleOrNull()
                    ?: throw ParameterConversionException(toType)
            }
        }
        LocalDate::class -> {
            if (value == "now") {
                LocalDate.now()
            } else {
                try {
                    LocalDate.parse(value, FRENCH_SHORT_DATE)
                } catch (e: DateTimeParseException) {
                    throw ParameterConversionException(toType)
                }
            }
        }
        LocalTime::class ->
            if (value == "now") {
                LocalTime.now()
            } else {
                try {
                    LocalTime.parse(value, FRENCH_TIME)
                } catch (e: DateTimeParseException) {
                    throw ParameterConversionException(toType)
                }
            }
        LocalDateTime::class ->
            if (value == "now") {
                LocalDateTime.now()
            } else {
                try {
                    LocalDateTime.parse(value, FRENCH_SHORT_DATETIME)
                } catch (e: DateTimeParseException) {
                    throw ParameterConversionException(toType)
                }
            }
        ZonedDateTime::class ->
            if (value == "now") {
                ZonedDateTime.now()
            } else {
                try {
                    LocalDateTime.parse(value, FRENCH_SHORT_DATETIME).atZone(EUROPE)
                } catch (e: DateTimeParseException) {
                    throw ParameterConversionException(toType)
                }
            }
        ZoneId::class ->
            try {
                ZoneId.of(value)
            } catch (e: DateTimeParseException) {
                throw ParameterConversionException(toType)
            } catch (e: ZoneRulesException) {
                throw ParameterConversionException(toType)
            }
        Duration::class ->
            try {
                parseDuration(value)
            } catch (e: ParseException) {
                throw ParameterConversionException(toType)
            }
        else -> throw RuntimeException("Unhandled case!")
    }
}
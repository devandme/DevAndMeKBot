/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.discordWizard

import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.User

/**
 * Represents the first step of wizard building.
 */
class FirstStepWizardBuilder(
    private val service: DiscordWizardService,
    private val title: String,
    private val targetUser: User,
    private val targetChannel: MessageChannel
) {
    /**
     * Sets the first step of the wizard.
     */
    fun <U> first(action: (awaiter: StepAwaiter) -> StepExecutionResult<U>) =
        WizardBuilder<U>(
            Step { _, awaiter -> action(awaiter) },
            this.service,
            this.title,
            this.targetUser,
            this.targetChannel
        )
}

/**
 * Represents the 2..N steps of wizard building.
 */
class WizardBuilder<T>(
    firstStep: Step<Unit, T>,
    private val service: DiscordWizardService,
    private val title: String,
    private val targetUser: User,
    private val targetChannel: MessageChannel
) {
    private val steps = mutableListOf<Step<*, *>>(firstStep)

    /**
     * Sets the next wizard step.
     */
    fun <U> then(action: (previousStepResult: T, awaiter: StepAwaiter) -> StepExecutionResult<U>): WizardBuilder<U> {
        this.steps += Step(action)

        @Suppress("UNCHECKED_CAST")
        return this as WizardBuilder<U>
    }

    /**
     * Builds the wizard.
     */
    @Suppress("UNCHECKED_CAST")
    fun build() = Wizard<T>(this.title, this.targetUser, this.targetChannel, this.steps as List<Step<Any?, Any?>>)

    /**
     * Builds and executes the wizard.
     *
     * @throws IllegalStateException If a wizard is already running for the targeted user.
     * @throws WizardCancelledException If the wizard has been cancelled.
     */
    fun buildAndExecute() = this.service.execute(this.build())

    /**
     * Builds and executes the wizard, returning `null` if the wizard is cancelled or one is already running for the
     * user.
     */
    fun buildAndExecuteOrNull() = try {
        this.service.execute(this.build())
    } catch (cancellation: IllegalStateException) {
        null
    } catch (cancellation: WizardCancelledException) {
        null
    }
}
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.discord

import com.devandme.kbot.configuration.DiscordConfiguration
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.OnlineStatus
import net.dv8tion.jda.core.entities.*
import org.slf4j.Logger
import org.springframework.beans.factory.DisposableBean
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

/**
 * Provides access to the Discord.
 *
 * @property jda The JDA instance.
 * @property guild The Discord guild.
 * @property prefixedRoles The roles that have the configured role prefix.
 * @property spamChannel The channel where the commands are posted.
 * @property presentationChannel The channel where the presentation messages are posted.
 * @property channelsWithCode The channel where the bot is allowed to send tips.
 * @property supportCategories The categories where the bot manages the open supports.
 * @property channelsIgnoringClearCommand The channels that should not be affected by the clear command.
 */
@Service
class DiscordService(logger: Logger, private val discordConfiguration: DiscordConfiguration) : DisposableBean {

    final val jda: JDA = JDABuilder(AccountType.BOT)
            .setAutoReconnect(true)
            .setAudioEnabled(false)
            .setToken(this.discordConfiguration.authenticationToken)
            .setCorePoolSize(1)
            .build()
            .awaitReady()

    val listeners = ListenerManager(logger, this.jda)

    val guild: Guild
        get() = this.jda.guilds.single()

    val prefixedRoles: List<Role>
        get() = this.guild.roles.filter { it.name.startsWith(this.discordConfiguration.rolePrefix) }

    val spamChannel: TextChannel
        get() = this.guild.getTextChannelById(
                this.discordConfiguration.spamChannelId
                        ?: throw UnsupportedOperationException("spamChannelId is not initialised !")
        )

    val presentationChannel: TextChannel
        get() = this.guild.getTextChannelById(
                this.discordConfiguration.presentationChannelId
                        ?: throw UnsupportedOperationException("presentationChannelId is not initialised !")
        )

    val channelsIgnoringClearCommand: List<TextChannel>
        get() = this.discordConfiguration.ignoreClearChannelIds.asSequence()
                .map(this.guild::getTextChannelById)
                .filterNotNull()
                .toList()

    val channelsWithCode: List<TextChannel>
        get() = this.guild.textChannels.filter {
            it.name !in this.discordConfiguration.channelsWithoutCode &&
                    it.idLong != this.discordConfiguration.spamChannelId
        }

    val supportCategories: List<Category>
        get() = this.guild.categories.filter { it.name.startsWith(this.discordConfiguration.supportCategoryPrefix) }

    /**
     * Retrieves the power provided by the member's roles.
     */
    fun getRolesPower(member: Member): Int =
            member.roles
                    .asSequence()
                    .map { this.discordConfiguration.rolesPower[it.idLong] }
                    .filterNotNull()
                    .max() ?: 0

    override fun destroy(): Unit = this.jda.shutdown()

    @Volatile
    private var index = 0

    @Scheduled(fixedRate = 120 * 1000, initialDelay = 1 * 1000)
    fun updatePlayingAt() {
        if (this.discordConfiguration.gameListForPresence.isEmpty()) {
            return
        }

        if (this.index >= this.discordConfiguration.gameListForPresence.size) {
            this.index = 0
        }

        this.jda.presence.setPresence(
                OnlineStatus.ONLINE,
                Game.playing(this.discordConfiguration.gameListForPresence[this.index])
        )
        this.index++
    }
}
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.commands

/**
 * Represents a command that can be executed by a user.
 *
 * **I. Command Syntax**
 *
 * How to use the [syntax]:
 * Each element of the syntax is called a term and is separated by one (or more) space.
 * Each term must be a lowercase alphabetic (and optionally dashed) string.
 *
 * The general syntax is:
 * - Command name (constant term);
 * - Parameters.
 *
 * A constant term requires a command parameter to have a constant value. Constant terms can only be preceded by
 * constant terms.
 *
 * **Example**: `help on-syntax` is a command named "help" that has one constant parameter that must always be
 * "on-syntax".
 *
 * A required term makes a command parameter required.
 * A special required term is `<param-name...>` which allows an unlimited number of arguments (including zero).
 *
 * **Example**: `help <topic> <search-keywords...>` is a command named "help" that accepts at least one parameter (the
 * topic) without limits.
 *
 * An optional term makes a command parameter optional.
 *
 * **Example**: `help [some-topic]` is a command named "help" that accepts zero or one parameter.
 *
 * **II. Parameters**
 *
 * Each optional or required term is bound to a function parameter.
 * The algorithm tries to tie each syntax parameter name with an optional `@Param` annotation.
 *
 * If the annotation is not present on the parameter, then the term is tied with the parameter name.
 *
 * If both of the attempts fails, an exception is thrown.
 *
 * **III. Typing parameters**
 *
 * The following types are allowed:
 * - [A JDA Member][net.dv8tion.jda.core.entities.Member];
 * - [A JDA User][net.dv8tion.jda.core.entities.User];
 * - [A JDA Role][net.dv8tion.jda.core.entities.Role];
 * - [An int][Int];
 * - [A long][Long];
 * - [A string][String];
 * - [A float][Float];
 * - [A double][Double];
 * - [A boolean][Boolean];
 * - [An array of one of the types above][Array] (valid for varargs parameters only).
 *
 * Additionally, [String] parameters can be validated with a [@Validate][Validate] annotation.
 *
 * @property syntax The syntax syntax.
 * @property title A short title for the command.
 * @property description The command's full description.
 * @property requiredPower The minimum amount of power that is required to execute the syntax.
 * @property aliases The command aliases.
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class CommandDefinition(
    val syntax: String,
    val title: String,
    val description: String,
    val requiredPower: Int = 0,
    val aliases: Array<String> = []
)
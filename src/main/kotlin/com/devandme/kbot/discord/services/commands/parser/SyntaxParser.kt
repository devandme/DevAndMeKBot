/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.commands.parser

/**
 * The [String] used to indicate a parameter is varargs.
 */
private const val VARARGS_STRING = "..."

/**
 * Regex that is used in the [String.replace] function to convert kebab-case word into a camel-case one.
 */
private val CAMEL_REGEX = "-([a-z])".toRegex()

/**
 * Regex that is used in the [String.replace] function to remove useless spaces.
 */
private val NORMALIZE_REGEX = "\\s{2,}".toRegex()

/**
 * Regex that validates a term name.
 */
private val TERM_NAME = "^[a-z-]+$".toRegex()

/**
 * A normalized syntax removes all trailing and leading whitespaces and useless whitespaces between the terms.
 *
 * @param syntax The syntax to normalize.
 * @return The normalized syntax.
 */
fun normalizeSyntax(syntax: String) = syntax.trim().replace(NORMALIZE_REGEX, " ")

/**
 * This function parses the syntax in order to get a list of [Term]s that are then matched against the
 * [@CommandDefinition][com.devandme.kbot.discord.services.commands.CommandDefinition] function parameters.
 *
 * @param syntax The syntax to parse.
 * @return The parsed [Term]s.
 * @throws CommandSyntaxParseException If the syntax could not be parsed.
 */
fun parseSyntax(syntax: String): Array<Term> {
    val result = mutableListOf<Term>()
    val terms = normalizeSyntax(syntax).split(' ')

    for (i in 0 until terms.size) {
        val term = terms[i]

        if (result.lastOrNull() is VarargsTerm) {
            throw CommandSyntaxGrammarException("Term #$i was found after a varargs term!")
        }

        result += when {
            term.startsWith('[') -> {
                if (!term.endsWith(']')) {
                    throw CommandSyntaxSyntaxException("Missing closing square bracket for term #$i, near '$term'!")
                }

                OptionalTerm(term.substring(1, term.length - 1))
            }
            term.startsWith('<') -> {
                if (!term.endsWith('>')) {
                    throw CommandSyntaxSyntaxException("Missing closing angle bracket for term #$i, near '$term'!")
                }

                if (result.any { it is OptionalTerm }) {
                    throw CommandSyntaxGrammarException("Required term #$i was found after an optional term!")
                }

                val trimmedTerm = term.substring(1, term.length - 1)

                // If the term is a varargs term...
                if (trimmedTerm.endsWith(VARARGS_STRING)) {
                    VarargsTerm(trimmedTerm.substring(0, trimmedTerm.length - VARARGS_STRING.length))
                } else {
                    RequiredTerm(trimmedTerm)
                }
            }
            else -> {
                if (result.any { it !is ConstantTerm }) {
                    throw CommandSyntaxGrammarException("Constant term #$i was found after a non-constant term!")
                }

                ConstantTerm(term)
            }
        }
    }

    if (result.firstOrNull() !is ConstantTerm) {
        throw CommandSyntaxGrammarException("A command syntax must start with at least one constant term!")
    }

    return result.toTypedArray()
}

abstract class Term(val name: String) {

    val camelCaseName = this.name.replace(CAMEL_REGEX) { it.groups[1]!!.value.toUpperCase() }

    init {
        if (!TERM_NAME.matches(name)) {
            throw CommandSyntaxGrammarException(
                "Invalid parameter name: a name must be a lowercase alphabetic or a dashed string!"
            )
        }
    }
}

class ConstantTerm(name: String) : Term(name)
class OptionalTerm(name: String) : Term(name)
class RequiredTerm(name: String) : Term(name)
class VarargsTerm(name: String) : Term(name)

abstract class CommandSyntaxParseException(message: String) : Exception(message)
class CommandSyntaxSyntaxException(message: String) : CommandSyntaxParseException(message)
class CommandSyntaxGrammarException(message: String) : CommandSyntaxParseException(message)
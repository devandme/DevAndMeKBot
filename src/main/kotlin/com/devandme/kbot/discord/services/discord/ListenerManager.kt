/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.discord

import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.events.Event
import net.dv8tion.jda.core.hooks.EventListener
import org.slf4j.Logger
import java.util.concurrent.CompletableFuture
import kotlin.reflect.KClass

/**
 * Manages listeners: sets calling order, allows events to be cancelled.
 */
class ListenerManager(private val logger: Logger, jda: JDA) {

    private val listeners = mutableListOf<DiscordEventHandler<*>>()

    init {
        jda.addEventListener(EventListener(this::onEvent))
    }

    private fun onEvent(e: Event) {
        val listeners = synchronized(this.listeners) { this.listeners.toTypedArray() }

        CompletableFuture.runAsync {
            for (listener in listeners) {
                if (listener.eventClass.isInstance(e)) {

                    val cancelled = try {
                        @Suppress("UNCHECKED_CAST")
                        (listener as DiscordEventHandler<Event>).callable(e)
                    } catch (e: Throwable) {
                        this.logger.error("An error occurred while calling a JDA listener:", e)
                        throw e
                    }

                    if (cancelled) {
                        break
                    }
                }
            }
        }
    }

    @Deprecated("Use plusAssign operator instead!")
    fun <T : Event> addListener(eventClass: KClass<T>, listener: (T) -> Boolean) {
        synchronized(this.listeners) {
            this.listeners += DiscordEventHandler(eventClass, listener)
        }
    }

    inline operator fun <reified T : Event> plusAssign(noinline listener: (T) -> Boolean) {
        // Normal use of addListener.
        @Suppress("DEPRECATION")
        this.addListener(T::class, listener)
    }

    operator fun <T : Event> minusAssign(listener: (T) -> Boolean) {
        synchronized(this.listeners) {
            this.listeners.removeIf { it.callable == listener }
        }
    }

    private class DiscordEventHandler<T : Event>(val eventClass: KClass<T>, val callable: (T) -> Boolean)
}
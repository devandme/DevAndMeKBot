/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.commands.parser

/**
 * The escape sequence character.
 */
const val ESCAPE_SEQUENCE_CHAR = '\\'

/**
 * The argument (string) delimiter.
 */
const val ARGUMENT_DELIMITER = '"'

/**
 * Parses the command in order to search for parameters.
 *
 * @param command The command to parse.
 * @return The command arguments.
 * @throws CommandParseException If the command couldn't be parsed.
 */
fun parseCommand(command: String): List<String> {

    // All arguments.
    val arguments = ArrayList<String>()

    // The current argument builder.
    val argument = StringBuilder()

    // If we're inside a delimited argument, the index of the current delimiter.
    var currentDelimiterIndex: Int? = null

    // If the current character is escaped.
    var isEscaped = false

    // Iteration on the command string.
    for (i in 0 until command.length) {

        // The current character.
        val char = command[i]

        // If this character is the beginning of an escape sequence,
        if (char == ESCAPE_SEQUENCE_CHAR) {

            // If we already are in an escape sequence,
            if (isEscaped) {

                // The user is escaping the escape sequence so we add the escape sequence char to the argument.
                argument.append(char)

                // We are no longer escaping the next character now.
                isEscaped = false
            }
            // Else, if we aren't in an escape sequence,
            else {
                // We tell the next loop iteration that we are escaping the character.
                isEscaped = true
            }

            // We skip the rest of the loop because we want to ignore the trailing instruction "isEscaped = false".
            continue
        }

        // If the character is a "blank" char,
        if (char.isWhitespace()) {

            // If we aren't inside a parameter delimiter, we finish the current argument.
            if (currentDelimiterIndex == null && argument.isNotEmpty()) {
                arguments += argument.toString()
                argument.setLength(0)
            }
            // Else, we add the character to the output.
            else if (currentDelimiterIndex != null) {
                argument.append(char)
            }
        }
        // Else, if the char is an argument delimiter,
        else if (char == ARGUMENT_DELIMITER) {

            // If it is escaped, we add it to the current argument.
            if (isEscaped) {
                argument.append(char)
            }
            // Else, if the char is not escaped and we aren't inside an argument delimiter, we set the delimiter index.
            else if (currentDelimiterIndex == null) {
                currentDelimiterIndex = i
            }
            // Last case : if the char is not escaped and we're inside an argument delimiter, we finish the current
            // argument along with the delimiter.
            else {
                currentDelimiterIndex = null
                arguments += argument.toString()
                argument.setLength(0)
            }
        }
        // Finally, if the char is neither an argument delimiter nor a whitespace char, we add it to the output !
        else {
            argument.append(char)
        }

        // Since we parsed a character, we are no longer inside an escape sequence.
        isEscaped = false
    }

    // When the loop is finished, we check that no argument remains in the string builder.
    if (argument.isNotEmpty()) {
        arguments += argument.toString()
        argument.setLength(0)
    }

    // We check that all escape sequences are finished.
    if (isEscaped) {
        throw UnfinishedEscapeSequenceException()
    }

    // We check that all delimiters are terminated.
    if (currentDelimiterIndex != null) {
        throw UnclosedArgumentDelimiterException(currentDelimiterIndex)
    }

    // We retrieve the command name from the parameters.
    // If we get an empty string or the list is empty, we throw an exception.
    if (arguments.firstOrNull().isNullOrBlank()) {
        throw EmptyCommandNameException()
    }

    // We finally return the command name and the arguments.
    return arguments
}

abstract class CommandParseException(message: String) : Exception(message)
class EmptyCommandNameException : CommandParseException("Command does not have any name!")
class UnfinishedEscapeSequenceException : CommandParseException("Last escape sequence was not finished!")
class UnclosedArgumentDelimiterException(val index: Int) :
    CommandParseException("Argument delimiter opened at index $index is not closed!")

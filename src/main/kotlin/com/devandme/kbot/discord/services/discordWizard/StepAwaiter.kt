/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.discordWizard

import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.User
import net.dv8tion.jda.core.events.Event
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.events.message.react.GenericMessageReactionEvent
import net.dv8tion.jda.core.hooks.EventListener
import java.util.concurrent.TimeUnit
import kotlin.reflect.KClass

/**
 * Provides several methods that helps waiting for specific Discord events.
 */
class StepAwaiter(
    private val listeners: Wizard.AwaiterListenerRegistry,
    private val targetUser: User,
    private val targetChannel: MessageChannel
) {
    /**
     * Blocks until the provided event occurs.
     *
     * @param eventType The event type.
     * @param timeout The maximum amount of time to wait.
     * @param unit The time unit for the [timeout].
     * @param validator A predicate telling if the event should be retained or not.
     * @return The awaited event, or `null` if the timeout is reached before.
     */
    fun <T : Event> waitFor(
        eventType: KClass<out T>,
        timeout: Long = 10,
        unit: TimeUnit = TimeUnit.MINUTES,
        validator: (T) -> Boolean = { true }
    ): T? {
        val lock = Object()
        var running = true
        val startTime = System.currentTimeMillis()
        val maxTime = startTime + unit.toMillis(timeout)
        var event: T? = null

        val eventListener = EventListener { e: Event ->
            if (eventType.isInstance(e)) {
                @Suppress("UNCHECKED_CAST")
                val tEvent = e as T

                if (validator(tEvent)) {
                    synchronized(lock) {
                        event = tEvent
                        lock.notify()
                    }
                }
            }
        }

        val stopListener = {
            synchronized(lock) {
                running = false
                lock.notify()
            }
        }

        this.listeners += eventListener
        this.listeners += stopListener

        try {
            synchronized(lock) {
                while (running && event == null && System.currentTimeMillis() < maxTime) {
                    lock.wait(maxTime - System.currentTimeMillis())
                }

                if (!running) {
                    throw WizardCancelledException()
                }
            }
        } finally {
            this.listeners -= stopListener
            this.listeners -= eventListener
        }

        return event
    }

    /**
     * Blocks until the provided event occurs.
     *
     * @param T The event type.
     * @param timeout The maximum amount of time to wait.
     * @param unit The time unit for the [timeout].
     * @param validator A predicate telling if the event should be retained or not.
     * @return The awaited event, or `null` if the timeout is reached before.
     */
    inline fun <reified T : Event> waitFor(
        timeout: Long = 10,
        unit: TimeUnit = TimeUnit.MINUTES,
        noinline validator: (T) -> Boolean = { true }
    ) = this.waitFor(T::class, timeout, unit, validator)

    /**
     * Waits for a message authored by a user in the channel.
     *
     * @param timeout The maximum amount of time to wait.
     * @param unit The time unit for the [timeout].
     * @return The message, or `null` if the timeout is reached before.
     */
    fun waitForUserMessage(
        timeout: Long = 10,
        unit: TimeUnit = TimeUnit.MINUTES
    ) =
        this.waitFor<MessageReceivedEvent>(timeout, unit)
        { it.author.idLong == this.targetUser.idLong && it.channel.idLong == this.targetChannel.idLong }?.message

    /**
     * Waits for a reaction being sent by a user.
     *
     * @param message The source channel.
     * @param user The reaction's author.
     * @param timeout The maximum amount of time to wait.
     * @param unit The time unit for the [timeout].
     * @param T The reaction event subtype (either MessageReactionAddEvent or MessageReactionRemoveEvent).
     * @return The reaction, or `null` if the timeout is reached before.
     */
    inline fun <reified T : GenericMessageReactionEvent> waitForUserReaction(
        message: Message,
        user: User,
        timeout: Long = 10,
        unit: TimeUnit = TimeUnit.MINUTES
    ) =
        this.waitFor<T>(timeout, unit)
        { it.user.idLong == user.idLong && it.messageIdLong == message.idLong }?.reactionEmote
}
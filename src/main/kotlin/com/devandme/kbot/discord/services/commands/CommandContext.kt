/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.commands

import com.devandme.kbot.misc.splitMessage
import net.dv8tion.jda.core.MessageBuilder
import net.dv8tion.jda.core.entities.Message.MAX_CONTENT_LENGTH
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.MessageEmbed
import com.devandme.kbot.database.entities.User as DatabaseUser
import com.devandme.kbot.services.messages.Message as ExternalMessage
import net.dv8tion.jda.core.entities.Member as DiscordUser
import net.dv8tion.jda.core.entities.Message as DiscordMessage

/**
 * The command execution context.
 *
 * @property databaseUser The user who executed the command (database version).
 * @property discordMember The user who executed the command (discord version).
 * @property userPower The total power granted to the user.
 */
abstract class CommandContext(val databaseUser: DatabaseUser, val discordMember: DiscordUser, val userPower: Int) {

    /**
     * Sends a message to the command sender.
     */
    fun sendMessage(message: ExternalMessage) {
        this.sendMessage(message / this.databaseUser)
    }

    /**
     * Sends a formatted message to the command sender.
     */
    fun sendMessageFormat(message: ExternalMessage, vararg format: Any?) {
        this.sendMessage(message.fmt(this.databaseUser, *format))
    }

    /**
     * Sends a string message to the command sender.
     */
    fun sendMessage(message: String) {
        if (message.length <= MAX_CONTENT_LENGTH) {
            this.sendMessage(MessageBuilder(message).build())
        } else {
            for (i in splitMessage(message)) {
                this.sendMessage(i)
            }
        }
    }

    /**
     * Sends a formatted string message to the command sender.
     */
    fun sendMessageFormat(message: String, vararg format: Any?) {
        this.sendMessage(message.format(*format))
    }

    /**
     * Sends a [DiscordMessage] to the command sender.
     */
    abstract fun sendMessage(message: DiscordMessage)

    /**
     * Sends a [MessageEmbed] to the command sender.
     */
    abstract fun sendMessage(message: MessageEmbed)
}

/**
 * A [CommandContext] for API command issuers.
 *
 * @property messagesSent A list of the messages that the user sent.
 */
class ApiCommandContext(databaseUser: DatabaseUser, discordUser: DiscordUser, userPower: Int) :
    CommandContext(databaseUser, discordUser, userPower) {

    val messagesSent: List<DiscordMessage>
        get() = this._sentMessages

    private val _sentMessages = mutableListOf<DiscordMessage>()

    override fun sendMessage(message: DiscordMessage) {
        this._sentMessages += message
    }

    override fun sendMessage(message: MessageEmbed) {
        this._sentMessages += MessageBuilder(message).build()
    }
}

/**
 * A [CommandContext] for Discord command issuers.
 *
 * @property channel The channel from with the command has been sent.
 */
class DiscordCommandContext(
    databaseUser: DatabaseUser,
    discordUser: DiscordUser,
    userPower: Int,
    val channel: MessageChannel
) : CommandContext(databaseUser, discordUser, userPower) {

    override fun sendMessage(message: DiscordMessage) {
        this.channel.sendMessage(message).complete()
    }

    override fun sendMessage(message: MessageEmbed) {
        this.channel.sendMessage(message).complete()
    }
}
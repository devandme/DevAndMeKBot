/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.listeners

import com.devandme.kbot.misc.nameWithClass
import net.dv8tion.jda.core.events.Event
import java.lang.reflect.InvocationTargetException
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.jvm.jvmErasure

/**
 * A listener that can be called.
 *
 * @property method The method to call.
 * @property controller The controller instance.
 * @property canCancel If the method supports the event cancellation or not.
 * @property eventType The type of the event that has been subscribed.
 */
class Listener(private val method: KFunction<*>, private val controller: Any) {

    val order: Int

    private val eventType: KClass<*>

    init {
        val parameters = this.method.parameters.filter { it.kind == KParameter.Kind.VALUE }

        if (parameters.size != 1) {
            throw ListenerRegistrationException("The listener ${this.method.nameWithClass} must have a single parameter!")
        }

        this.eventType = parameters.single().type.jvmErasure

        if (!this.eventType.isSubclassOf(Event::class)) {
            throw ListenerRegistrationException("The listener's single parameter ${this.method.nameWithClass} must be a subtype of the JDA Event class!")
        }

        this.order = this.method.findAnnotation<DiscordEventListener>()!!.order
    }

    /**
     * Calls this listener if the subscribed event type matches the provided event.
     *
     * @param event The event to trigger.
     * @return `true` if the event was cancelled, `false` otherwise.
     */
    fun callIfPossible(event: Event): Boolean {
        if (this.eventType.isInstance(event)) {
            try {
                val result = this.method.call(this.controller, event)

                if (result == true) {
                    return true
                }
            } catch (e: InvocationTargetException) {
                throw e.cause ?: e
            }
        }

        return false
    }
}
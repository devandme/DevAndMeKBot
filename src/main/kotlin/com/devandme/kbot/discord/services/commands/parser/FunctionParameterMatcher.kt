/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.commands.parser

import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.Param
import net.dv8tion.jda.core.entities.Channel
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.entities.User
import java.time.*
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSuperclassOf
import kotlin.reflect.jvm.jvmErasure

/**
 * List of the allowed types in the [@CommandDefinition][com.devandme.kbot.discord.services.commands.CommandDefinition]s.
 */
private val ALLOWED_TYPES = arrayOf(
    Member::class,
    User::class,
    Role::class,
    Channel::class,
    Int::class,
    Long::class,
    String::class,
    Float::class,
    Double::class,
    Boolean::class,
    LocalDate::class,
    LocalTime::class,
    LocalDateTime::class,
    ZonedDateTime::class,
    ZoneId::class,
    Duration::class
)

/**
 * This function ties syntax terms to the
 * [@CommandDefinition][com.devandme.kbot.discord.services.commands.CommandDefinition]s' parameters.
 *
 * @param parsedSyntax The parsed command syntax.
 * @param function The function to bind.
 * @return The command parameters.
 * @throws SyntaxToParameterBindingException If an error occurs while the binding is performed.
 */
fun matchSyntaxToFunction(parsedSyntax: Array<Term>, function: KFunction<*>): Array<CommandParameter> {
    val result = mutableListOf<CommandParameter>()
    val boundParameters = mutableListOf<KParameter>()
    val parameters = function.parameters.filter { it.kind == KParameter.Kind.VALUE }.associateBy {
        it.findAnnotation<Param>()?.name
            ?: it.name
            ?: throw SyntaxToParameterBindingException("Parameter #${it.index} has no valid name!")
    }

    for (term in parsedSyntax) {
        //  Warning
        //     V
        result += if (term is ConstantTerm) {
            ConstantCommandParameter(term.name)
        } else {
            val matchingParameter = parameters[term.name]
                ?: parameters[term.camelCaseName]
                ?: throw SyntaxToParameterBindingException("Term '${term.name}' has no matching parameter!")

            boundParameters += matchingParameter

            when (term) {
                is OptionalTerm -> OptionalCommandParameter(term.name, matchingParameter)
                is RequiredTerm -> RequiredCommandParameter(term.name, matchingParameter)
                is VarargsTerm -> VarargsCommandParameter(term.name, matchingParameter)
                else -> throw RuntimeException("Unsupported case!")
            }
        }
    }

    // Search for command context params.
    val commandContextParameters = parameters.values.filter { it.type.jvmErasure == CommandContext::class }

    if (commandContextParameters.size > 1) {
        throw SyntaxToParameterBindingException("Only one parameter should be present for the CommandContext type!")
    } else if (commandContextParameters.size == 1) {
        val commandContextParameter = commandContextParameters.first()
        result += CommandContextParameter(commandContextParameter)
        boundParameters += commandContextParameter
    }

    if (parameters.size != boundParameters.size) {
        val unboundParameters = parameters.values - boundParameters
        val message = unboundParameters.joinToString(", ") { "parameter #${it.index}" }

        throw SyntaxToParameterBindingException("Parameters are not bound: ${message}")
    }

    return result.toTypedArray()
}

/**
 * Represents a command parameter.
 */
abstract class CommandParameter

/**
 * A parameter for the [CommandContext] element.
 *
 * @property parameter The parameter.
 */
class CommandContextParameter(val parameter: KParameter) : CommandParameter()

/**
 * A constant command parameter.
 *
 * @property value The parameter value.
 */
class ConstantCommandParameter(val value: String) : CommandParameter() {
    override fun equals(other: Any?) = other is ConstantCommandParameter && other.value == this.value
    override fun hashCode() = this.value.hashCode()
}

/**
 * A required command parameter.
 *
 * @property name The parameter's name.
 * @property parameter The matching function parameter.
 */
class RequiredCommandParameter(val name: String, val parameter: KParameter) : CommandParameter() {

    val type: KType
        get() = this.parameter.type

    init {
        if (this.parameter.type.jvmErasure !in ALLOWED_TYPES) {
            throw SyntaxToParameterBindingException("Parameter #${this.parameter.index} type is not allowed!")
        }
    }
}

/**
 * An optional command parameter.
 *
 * @property name The parameter's name.
 * @property parameter The matching function parameter.
 */
class OptionalCommandParameter(val name: String, val parameter: KParameter) : CommandParameter() {

    val type: KType
        get() = this.parameter.type

    init {
        if (this.parameter.type.jvmErasure !in ALLOWED_TYPES) {
            throw SyntaxToParameterBindingException("Parameter #${this.parameter.index} type is not allowed!")
        }

        if (!this.parameter.isOptional && !this.parameter.type.isMarkedNullable) {
            throw SyntaxToParameterBindingException("Parameter #${this.parameter.index} binds to an optional syntax parameter, but isn't optional nor nullable!")
        }
    }
}

/**
 * A varargs command parameter.
 *
 * @property name The parameter's name.
 * @property parameter The matching function parameter.
 */
class VarargsCommandParameter(val name: String, val parameter: KParameter) : CommandParameter() {

    val type: KType
        get() = this.parameter.type

    init {
        if (!this.parameter.type.jvmErasure.isSuperclassOf(List::class)) {
            throw SyntaxToParameterBindingException("Parameter #${this.parameter.index} type must be a List or a superclass!")
        }

        if (this.parameter.type.arguments.single().type!!.jvmErasure !in ALLOWED_TYPES) {
            throw SyntaxToParameterBindingException("Parameter #${this.parameter.index} is not a List of an allowed type!")
        }
    }
}

class SyntaxToParameterBindingException(message: String) : Exception(message)
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.discord.services.discordWizard

import com.devandme.kbot.discord.services.discord.DiscordService
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.User
import net.dv8tion.jda.core.events.Event
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.EventListener

/**
 * Messages that allows leaving the wizard.
 */
private val CANCEL_MESSAGES = listOf("!quit", "!q")

/**
 * Represents a wizard.
 *
 * @property title The wizard's title.
 */
class Wizard<T>(
    val title: String,
    val targetUser: User,
    val targetChannel: MessageChannel,
    private val steps: List<Step<Any?, Any?>>
) {

    init {
        if (this.steps.isEmpty()) {
            throw IllegalArgumentException("steps cannot be empty!")
        }
    }

    /**
     * Executes the wizard and returns the result.
     */
    @Throws(WizardCancelledException::class)
    fun execute(discordService: DiscordService): T {
        val listeners = AwaiterListenerRegistry()
        val cancellationListener = { e: Event ->
            if (e is MessageReceivedEvent && e.author.idLong == this.targetUser.idLong && e.message.contentRaw in CANCEL_MESSAGES) {
                listeners.fireStopEvent()
            } else {
                listeners.fire(e)
            }

            false
        }

        discordService.listeners += cancellationListener

        try {
            return this.executeInternal(StepAwaiter(listeners, this.targetUser, this.targetChannel))
        } finally {
            discordService.listeners -= cancellationListener
        }
    }

    private fun executeInternal(awaiter: StepAwaiter): T {
        var previousResult: NextExecutionResult<*>? = null

        for (step in this.steps) {
            var currentResult: StepExecutionResult<*>

            do {
                currentResult = step.execute(previousResult?.result ?: Unit, awaiter)
            } while (currentResult is ReplayExecutionResult)

            if (currentResult is CancellationExecutionResult) {
                throw WizardCancelledException()
            }

            if (currentResult !is NextExecutionResult) {
                throw RuntimeException("Unhandled case!")
            }

            previousResult = currentResult
        }

        @Suppress("UNCHECKED_CAST")
        return previousResult!!.result as T
    }

    class AwaiterListenerRegistry {
        private val listeners = mutableListOf<EventListener>()
        private val stopListeners = mutableListOf<() -> Unit>()

        operator fun plusAssign(listener: EventListener) {
            synchronized(this.listeners) {
                this.listeners += listener
            }
        }

        operator fun minusAssign(listener: EventListener) {
            synchronized(this.listeners) {
                this.listeners -= listener
            }
        }

        operator fun plusAssign(listener: () -> Unit) {
            synchronized(this.stopListeners) {
                this.stopListeners += listener
            }
        }

        operator fun minusAssign(listener: () -> Unit) {
            synchronized(this.stopListeners) {
                this.stopListeners -= listener
            }
        }

        fun fireStopEvent() {
            synchronized(this.stopListeners) {
                for (listener in this.stopListeners) {
                    listener()
                }
            }
        }

        fun fire(e: Event) {
            synchronized(this.listeners) {
                for (listener in this.listeners) {
                    listener.onEvent(e)
                }
            }
        }
    }
}
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.configuration

import com.devandme.kbot.web.security.DatabaseAndDiscordAuthentication
import io.swagger.annotations.Api
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.BasicAuth
import springfox.documentation.service.SecurityReference
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import java.util.*

@Configuration
class SwaggerConfiguration {

    private val projectName: String

    private val projectVersion: String

    private val projectDescription: String

    init {
        val projectProperties = Properties()

        SwaggerConfiguration::class.java.getResourceAsStream("/project.properties").use { projectProperties.load(it) }

        this.projectName = projectProperties.getProperty("project.name")
        this.projectVersion = projectProperties.getProperty("project.version")
        this.projectDescription = projectProperties.getProperty("project.description")
    }

    @Bean
    fun getApi(): Docket =
        Docket(DocumentationType.SWAGGER_2)
            .apiInfo(
                ApiInfo(
                    this.projectName,
                    this.projectDescription,
                    this.projectVersion,
                    null,
                    null,
                    "Apache",
                    null,
                    mutableListOf()
                )
            )
            .ignoredParameterTypes(DatabaseAndDiscordAuthentication::class.java)
            .select()
            .apis(RequestHandlerSelectors.withClassAnnotation(Api::class.java))
            .paths(PathSelectors.any())
            .build()
            .securitySchemes(this.securitySchemes())
            .securityContexts(this.securityContexts())

    private fun securitySchemes() = listOf(BasicAuth("userAuthentication"))

    private fun securityContexts() = listOf(
        SecurityContext.builder().securityReferences(listOf(SecurityReference("userAuthentication", arrayOf()))).build()
    )
}
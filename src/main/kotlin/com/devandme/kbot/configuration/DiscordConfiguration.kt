/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import javax.validation.constraints.Min

/**
 * The Discord server configuration.
 *
 * @property authenticationToken The Discord authentication token.
 * @property commandPrefix The message prefix used to recognize commands.
 * @property rolePrefix The prefix used to recognize roles.
 * @property spamChannelId The channel id long.
 * @property spamChannelPinPrefix A prefix used to differentiate pinned messages from spam messages. The spam channel is
 * automatically cleared.
 * @property presentationChannelId The presentation channel id long.
 * @property channelsWithoutCode The list of the channels where the tip messages are not sent automatically by the bot.
 * @property supportCategoryPrefix The category prefix used to identify support categories.
 * @property gameListForPresence The list of messages to display as presence ("Playing at").
 * @property daysBeforeInactiveSupportWarning The number of days (since the last message) the bot must wait before
 * sending a warning message about the support closure.
 * @property daysBeforeInactiveSupportClosure The number of days (since the last message) the bot must wait before
 * closing the support.
 * @property daysBeforeAwayUserDeletion Number of days before the user.
 * @property ignoreClearChannelIds List of channel IDs ignored by the clear command.
 */
@Configuration
@ConfigurationProperties(prefix = "discord", ignoreUnknownFields = false)
class DiscordConfiguration {

    lateinit var authenticationToken: String

    lateinit var commandPrefix: String

    lateinit var rolePrefix: String

    var spamChannelId: Long? = null

    lateinit var spamChannelPinPrefix: String

    var presentationChannelId: Long? = null

    lateinit var channelsWithoutCode: List<String>

    lateinit var supportCategoryPrefix: String

    lateinit var gameListForPresence: List<String>

    @Min(1)
    var daysBeforeInactiveSupportWarning: Long = 7

    @Min(1)
    var daysBeforeInactiveSupportClosure: Long = 14

    @Min(1)
    var daysBeforeAwayUserDeletion: Long = 60

    lateinit var ignoreClearChannelIds: List<Long>

    lateinit var rolesPower: HashMap<Long, Int>
}
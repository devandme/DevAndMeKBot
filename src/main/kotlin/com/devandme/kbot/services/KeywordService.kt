/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.services

import com.devandme.kbot.database.entities.Keyword
import com.devandme.kbot.database.repositories.KeywordRepository
import com.devandme.kbot.misc.orNull
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Transactional
@Service
class KeywordService(private val keywordRepository: KeywordRepository) {

    /**
     * Tries to find a single [Keyword] thanks to its label.
     *
     * @param label The [Keyword] label.
     * @return The [Keyword], or `null` if none match.
     */
    fun findOneByLabel(label: String): Keyword? = this.keywordRepository.findById(label.toLowerCase()).orNull()

    /**
     * Tries to find the [Keyword]s thanks to the provided labels, or create the missing if necessary.
     *
     * @return The matching and the created [Keyword]s.
     */
    fun findOrCreate(labels: Iterable<String>): Iterable<Keyword> =
        labels.map { this.findOneByLabel(it) ?: this.keywordRepository.save(Keyword(it)) }

    /**
     * Searches for [Keyword]s containing the provided search string.
     *
     * @param search The string to search.
     */
    fun search(search: String): Set<Keyword> = this.keywordRepository.findAllByLabelContaining(search.toLowerCase())

    /**
     * Saves the [Keyword].
     *
     * @param keyword The [Keyword] to save.
     */
    fun save(keyword: Keyword) {
        this.keywordRepository.save(keyword)
    }

    /**
     * Deletes the [Keyword].
     *
     * @param keyword The [Keyword] to delete.
     */
    fun delete(keyword: Keyword) {
        this.keywordRepository.delete(keyword)
    }
}
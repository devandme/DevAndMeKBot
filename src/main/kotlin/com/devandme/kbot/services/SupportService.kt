/*
 * Copyright 2019 Mathéo CIMBARO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.services

import com.devandme.kbot.configuration.DiscordConfiguration
import com.devandme.kbot.database.entities.Keyword
import com.devandme.kbot.database.entities.Support
import com.devandme.kbot.database.entities.SupportDeletionWarning
import com.devandme.kbot.database.entities.User
import com.devandme.kbot.database.repositories.SupportDeletionWarningRepository
import com.devandme.kbot.database.repositories.SupportRepository
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.misc.*
import com.devandme.kbot.services.messages.LanguageLevel.NORMAL_FRENCH
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.*
import net.dv8tion.jda.core.entities.MessageEmbed.Field
import org.springframework.context.annotation.Lazy
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.awt.Color
import java.io.IOException
import java.net.URL
import java.time.OffsetDateTime
import java.time.ZonedDateTime
import javax.persistence.criteria.Predicate
import javax.transaction.Transactional
import javax.validation.constraints.Min

/**
 * The maximum size of an attachment in bytes.
 */
const val MAX_ATTACHMENT_SIZE = 8_000_000

/**
 * The maximum number of attachments per support.
 */
const val MAX_ATTACHMENT_COUNT = 15

/**
 * The maximum number of keywords for a support.
 */
const val MAX_KEYWORD_COUNT = 10

/**
 * The maximum length of a support content size.
 */
const val MAX_CONTENT_LENGTH = 100_000

/**
 * The maximum number of channels allowed in one Discord category.
 */
const val MAX_CHANNELS_PER_CATEGORY = 50

/**
 * The maximum number of days an archived support is moved.
 */
const val ARCHIVED_SUPPORT_MAX_AGE = 7L

/**
 * The maximum supports a user can have.
 */
const val MAX_SUPPORTS_PER_USER = 30

@Transactional
@Service
class SupportService(
        private val discord: DiscordService,
        private val config: DiscordConfiguration,
        private val supports: SupportRepository,
        private val supportWarnings: SupportDeletionWarningRepository,
        @Lazy private val users: UserService,
        messageService: MessageService
) {
    private val messages = messageService / MessageStorage.SUPPORT_SERVICE

    /**
     * Finds a single [Support] thanks to its ID.
     *
     * @param id The [Support] ID.
     * @return The [Support], or `null` if none match.
     */
    fun findOneById(id: Long): Support? = this.supports.findById(id).orNull()

    /**
     * Finds all [Support]s.
     */
    fun findAll(pageable: Pageable = Pageable.unpaged()): Iterable<Support> = this.supports.findAll(pageable)

    /**
     * Finds all [Support] with a filter.
     *
     * @param filter The filter.
     */
    fun findAll(filter: SupportFilter): Collection<Support> {
        val page = PageRequest.of(filter.page ?: 0, 100, Sort.Direction.DESC, Support::creationDate.name)

        return this.supports.findAll(
                { support, q, c ->
                    val conditions = mutableListOf<Predicate>()

                    if (filter.author != null) {
                        conditions += c.equal(support.get<Long>(Support::author.name), filter.author)
                    }

                    if (filter.title != null) {
                        conditions += c.like(support.get<String>(Support::title.name), "%${filter.title}%")
                    }

                    if (filter.keywords != null && filter.keywords.isNotEmpty()) {
                        // JOIN with Keyword.
                        val distinctKeywords = filter.keywords.distinct()
                        val join = support.join<Keyword, Support>(Support::keywords.name)

                        conditions += join.get<String>(Keyword::label.name).`in`(distinctKeywords)

                        // SELECT and GROUP BY all support attributes (since we want a full Support entity).
                        val supportAttributes = support.model.declaredSingularAttributes.map { support.get<Any>(it.name) }
                        q.multiselect(supportAttributes)
                        q.groupBy(supportAttributes)
                        q.having(c.equal(c.countDistinct(join.get<String>(Keyword::label.name)), distinctKeywords.size))
                    }

                    if (filter.start != null) {
                        conditions += c.lessThanOrEqualTo(
                                support.get<ZonedDateTime>(Support::creationDate.name),
                                filter.start
                        )
                    }

                    if (filter.end != null) {
                        conditions += c.greaterThanOrEqualTo(
                                support.get<ZonedDateTime>(Support::creationDate.name),
                                filter.end
                        )
                    }

                    c.and(*conditions.toTypedArray())
                },
                page
        ).toList()
    }

    /**
     * Creates a new [Support].
     *
     * @param author The support's author.
     * @param title The support's title.
     * @param shortTitle The support's channel name.
     * @param content The support's content.
     * @param attachments The support's attachments.
     * @param keywords The support's keywords.
     * @throws SupportChannelAlreadyExistsException If the channel already exists.
     * @throws TooManyOpenedSupportsException If the user has opened too many support channels at the same time.
     */
    @Throws(SupportCreationException::class)
    fun create(
            author: User,
            title: String,
            shortTitle: String,
            content: String,
            attachments: List<SupportAttachment>,
            keywords: Iterable<Keyword>
    ): Support {
        if (author.authoredSupports.size >= MAX_SUPPORTS_PER_USER) {
            throw TooManyOpenedSupportsException()
        }

        if (this.discord.guild.getTextChannelsByName(shortTitle, false).firstOrNull() != null) {
            throw SupportChannelAlreadyExistsException()
        }

        // Get the first non-full category.
        val creationDate = ZonedDateTime.now()
        val channel = this.getNonFullSupportCategory().createTextChannel(shortTitle).complete() as TextChannel

        this.publishSupport(title, author, channel, content, keywords, attachments)

        return this.supports.save(
                Support(
                        channel.idLong,
                        creationDate,
                        author,
                        title,
                        shortTitle,
                        null,
                        keywords.toMutableList()
                )
        )
    }

    /**
     * Retrieves the attachments for the provided [Support].
     */
    fun getAttachmentsFor(support: Support): List<ExistingAttachment> = this.getAttachmentsFor(support.channelId)

    /**
     * Retrieves the attachments for the provided [Support] ID.
     *
     * @throws SupportChannelDeletedException If the channel has been deleted.
     * @throws SupportChannelCorruptedException If the channel has no pinned messages.
     */
    fun getAttachmentsFor(supportId: Long): List<ExistingAttachment> {
        val channel = this.discord.guild.getTextChannelById(supportId) ?: throw SupportChannelDeletedException()

        val firstContentMessage = channel.pinnedMessages.complete()
                .firstOrNull { it.author.idLong == this.discord.jda.selfUser.idLong }
                ?: throw SupportChannelCorruptedException()

        return channel.getHistoryAfter(firstContentMessage, 100).complete()
                .retrievedHistory
                .asSequence()
                .filter { it.author.idLong == this.discord.jda.selfUser.idLong }
                .flatMap { it.attachments.asSequence() }
                .map(SupportService::ExistingAttachment)
                .toList()
    }

    /**
     * Retrieves the contents of a [Support].
     *
     * @throws SupportChannelDeletedException If the channel has been deleted.
     */
    fun getContents(support: Support): SupportContents {
        val selfId = this.discord.jda.selfUser.idLong
        val channel = this.discord.guild.getTextChannelById(support.channelId) ?: throw SupportChannelDeletedException()

        val firstContentMessage = channel.pinnedMessages.complete()
                .firstOrNull { it.author.idLong == selfId } ?: throw SupportChannelCorruptedException()

        val contentMessages = channel.getHistoryWhile(firstContentMessage) {
            it.author.idLong == selfId && it.type != MessageType.CHANNEL_PINNED_ADD
        }

        return SupportContents(
                contentMessages.joinToString("\n") { it.contentRaw },
                contentMessages.asSequence().flatMap { it.attachments.asSequence() }.map(SupportService::ExistingAttachment).toMutableList()
        )
    }

    /**
     * Changes the contents of a [Support].
     *
     * @throws SupportChannelDeletedException If the channel has been deleted.
     */
    fun setContents(support: Support, contents: SupportContents) {
        val selfId = this.discord.jda.selfUser.idLong
        val channel = this.discord.guild.getTextChannelById(support.channelId) ?: throw SupportChannelDeletedException()

        val firstContentMessage = channel.pinnedMessages.complete()
                .firstOrNull { it.author.idLong == selfId } ?: throw SupportChannelCorruptedException()

        val contentMessages = channel.getHistoryWhile(firstContentMessage) {
            it.author.idLong == selfId && it.type != MessageType.CHANNEL_PINNED_ADD
        }

        val messagesToDelete = contentMessages + firstContentMessage

        // See https://github.com/discordapp/discord-api-docs/issues/208.
        if (messagesToDelete.any { it.creationTime.isBefore(OffsetDateTime.now().minusWeeks(2)) }) {
            for (messageToDelete in contentMessages + firstContentMessage) {
                messageToDelete.delete().complete()
            }
        } else {
            channel.deleteMessages(messagesToDelete).complete()
        }

        this.publishSupport(
                support.title,
                support.author,
                channel,
                contents.content,
                support.keywords,
                contents.attachments
        )
    }

    /**
     * Opens a *closed* [Support].
     *
     * @param support The [Support] to open.
     * @throws SupportChannelDeletedException If the channel has been deleted.
     */
    fun open(support: Support) {
        if (support.solvedDate == null) {
            throw IllegalStateException("Support is already open!")
        }

        val channel = this.discord.guild.getTextChannelById(support.channelId) ?: throw SupportChannelDeletedException()

        channel.manager.setName(support.shortTitle).complete()
        channel.manager.putPermissionOverride(this.discord.guild.publicRole, Permission.MESSAGE_WRITE.rawValue, 0)
                .complete()

        channel.sendMessage(this.messages / "support-re-opened" / NORMAL_FRENCH).complete()

        support.solvedDate = null
        this.supports.save(support)
    }

    /**
     * Closes the provided [Support].
     *
     * @param support The [Support] to close.
     * @throws SupportChannelDeletedException If the channel has been deleted.
     */
    fun close(support: Support, helpers: Iterable<User>) {
        if (support.solvedDate != null) {
            throw IllegalStateException("Support is already closed!")
        }

        val channel = this.discord.guild.getTextChannelById(support.channelId) ?: throw SupportChannelDeletedException()

        channel.manager.setName("archive-${support.shortTitle}").complete()
        channel.manager.putPermissionOverride(this.discord.guild.publicRole, 0, Permission.MESSAGE_WRITE.rawValue)
                .complete()

        channel.sendMessage(
                (this.messages / "support-closed").fmt(
                        NORMAL_FRENCH,
                        ARCHIVED_SUPPORT_MAX_AGE
                )
        ).complete()

        support.solvedDate = ZonedDateTime.now()
        this.supports.save(support)
        this.supportWarnings.deleteAll(support.warnings)

        for (helper in helpers) {
            helper.helpedSupports++
        }

        this.users.save(helpers)
    }

    /**
     * Updates an *open* [Support].
     *
     * @param support The [Support] to update.
     * @throws SupportChannelDeletedException If the channel has been deleted.
     * @throws SupportChannelCorruptedException If the channel has been corrupted.
     */
    fun update(support: Support) {
        if (support.solvedDate != null) {
            throw IllegalStateException("Unable to update a closed support, use SupportService::close() instead!")
        }

        val selfId = this.discord.jda.selfUser.idLong
        val channel = this.discord.guild.getTextChannelById(support.channelId) ?: throw SupportChannelDeletedException()

        val firstContentMessage = channel.pinnedMessages.complete()
                .firstOrNull { it.author.idLong == selfId } ?: throw SupportChannelCorruptedException()

        firstContentMessage
                .editMessage(this.createSupportEmbed(support.title, support.author, support.keywords))
                .complete()

        this.supports.save(support)
    }

    /**
     * Deletes the provided [Support].
     *
     * @param support The [Support] to delete.
     */
    fun delete(support: Support) {
        this.discord.guild.getTextChannelById(support.channelId)?.delete()?.complete()
        this.supportWarnings.deleteAll(support.warnings)
        this.supportWarnings.flush()
        this.supports.delete(support)
    }

    /**
     * Counts the [Support]s having the provided [Keyword].
     *
     * @param keyword The [Keyword].
     * @return The number of matching supports.
     */
    fun countByKeyword(keyword: Keyword) = this.supports.countByKeyword(keyword.label)

    @Scheduled(cron = "0 0 * * * *")
    fun askAndCloseOldSupports() {
        val now = OffsetDateTime.now()
        val limitClosureDate = now.minusDays(this.config.daysBeforeInactiveSupportClosure)
        val limitWarningDate = now.minusDays(this.config.daysBeforeInactiveSupportWarning)
        val warnedSupports = this.supports.findAllOpenWarnedBefore(limitClosureDate.toZonedDateTime())
        val nonWarnedSupports = this.supports.findAllOpenNonWarned()
        val allChannels = this.discord.supportCategories.asSequence()
                .flatMap { it.textChannels.asSequence() }
                .associateBy { it.idLong }

        for (support in warnedSupports) {
            val channel = allChannels[support.channelId] ?: continue
            val lastMessage = channel.getLastMessageSentBy(support.author.id)

            // If we've no reply from the user or the last reply was before the
            if (lastMessage == null || lastMessage.creationTime < support.warnings.map { it.date.toOffsetDateTime() }.max()) {
                this.close(support, emptyList())
            } else {
                channel.sendMessage(this.messages / "warning-cancelled" / NORMAL_FRENCH).complete()
                this.supportWarnings.deleteAll(support.warnings)
            }
        }

        for (support in nonWarnedSupports) {
            val channel = allChannels[support.channelId] ?: continue
            val lastMessage = channel.lastMessage ?: continue

            if (lastMessage.creationTime < limitWarningDate) {
                val message = channel.sendMessage(
                        (this.messages / "warning").fmt(
                                NORMAL_FRENCH,
                                lastMessage.creationTime.format(FRENCH_SHORT_DATE),
                                lastMessage.creationTime
                                        .plusDays(this.config.daysBeforeInactiveSupportClosure)
                                        .format(FRENCH_SHORT_DATE)
                        )
                ).complete()

                this.supportWarnings.save(
                        SupportDeletionWarning(
                                message.idLong,
                                support,
                                message.creationTime.toZonedDateTime()
                        )
                )
            }
        }
    }

    @Scheduled(cron = "0 10 * * * *")
    fun organizeCategoriesAndSupports() {
        val categories = this.discord.supportCategories.sortedBy(Category::getName)

        // Move channels to non-empty categories to remove unused categories.
        for (i in 1 until categories.size) {
            val previousCategoryAvailableSize = MAX_CHANNELS_PER_CATEGORY - categories[i - 1].channels.size

            if (previousCategoryAvailableSize == 0) {
                continue
            }

            val previousCategory = categories[i - 1]
            val remainingChannels = categories
                    .asSequence()
                    .drop(i)
                    .flatMap { it.channels.asSequence() }
                    .take(previousCategoryAvailableSize)
                    .toList()

            for (remainingChannel in remainingChannels) {
                remainingChannel.manager.setParent(previousCategory).complete()
            }
        }

        // Remove empty categories.
        for (emptyCategory in categories.filter { it.channels.isEmpty() }) {
            emptyCategory.delete().complete()
        }

        val supports = this.findAll().associateBy(Support::channelId).toMutableMap()
        val supportChannels = categories.flatMap(Category::getChannels).associateBy(Channel::getIdLong).toMutableMap()

        // Remove supports that does not exists in database but exists on the Discord server.
        for ((id, nonSupportChannel) in supportChannels.filter { (k, _) -> k !in supports }) {
            nonSupportChannel.delete().complete()
            supportChannels.remove(id)
        }

        // Remove supports that exists in the database but not on the Discord server.
        for ((id, supportToRemove) in supports.filterKeys { it !in supportChannels }) {
            this.delete(supportToRemove)
            supports.remove(id)
        }

        val limitDate = ZonedDateTime.now().minusDays(ARCHIVED_SUPPORT_MAX_AGE)

        // Remove old closed supports.
        for (supportToDelete in supports.values.filter { it.solvedDate != null && it.solvedDate!! < limitDate }) {
            this.delete(supportToDelete)
        }
    }

    /**
     * Convenience method used to publish a support.
     */
    private fun publishSupport(
            title: String,
            author: User,
            channel: TextChannel,
            content: String,
            keywords: Iterable<Keyword>,
            attachments: List<SupportAttachment>
    ) {
        // Create the support header.
        val header = this.createSupportEmbed(title, author, keywords)
        val headerMessage = channel.sendMessage(header).complete()

        for (message in splitMessage(content)) {
            channel.sendMessage(message).complete()
        }

        for (attachment in attachments) {
            channel.sendFile(attachment.read(), attachment.name).complete()
        }

        headerMessage.pin().complete()
    }

    /**
     * Creates the embed for a support.
     */
    private fun createSupportEmbed(title: String, author: User, keywords: Iterable<Keyword>) =
            createEmbed(
                    title = title,
                    description = (this.messages / "support-description").fmt(
                            NORMAL_FRENCH,
                            ZonedDateTime.now(EUROPE).format(FRENCH_LONG_DATETIME)
                    ),
                    author = this.discord.jda.getUserById(author.id),
                    color = Color.CYAN,
                    fields = arrayOf(
                            Field(this.messages / "keywords" / NORMAL_FRENCH, keywords.joinToString { it.label }, false)
                    )
            )

    /**
     * @return The first non-full support category or a new one.
     */
    private fun getNonFullSupportCategory(): Category {
        val categories = this.discord.supportCategories

        // Get the first non-empty category. If all categories are full, create a new one.
        return categories.firstOrNull { it.channels.size < MAX_CHANNELS_PER_CATEGORY }
                ?: this.discord.guild.controller
                        .createCategory(this.config.supportCategoryPrefix + (categories.size + 1))
                        .complete() as Category
    }

    /**
     * An attachment.
     *
     * @property name The attachment file's name.
     */
    abstract class SupportAttachment {

        abstract val name: String

        /**
         * Reads the attachment.
         */
        @Throws(IOException::class)
        abstract fun read(): ByteArray
    }

    /**
     * An attachment created from a [URL].
     */
    class ExistingAttachment(val attachment: Message.Attachment) : SupportAttachment() {
        override val name: String
            get() = this.attachment.fileName

        override fun read(): ByteArray = this.attachment.inputStream.use { it.readBytes() }
    }

    /**
     * An attachment created from a [MultipartFile].
     */
    class NewAttachment(private val file: MultipartFile) : SupportAttachment() {
        override val name: String
            get() = this.file.originalFilename ?: "Unknown"

        override fun read() = this.file.bytes
    }

    /**
     * The contents of a support.
     */
    class SupportContents(var content: String, val attachments: MutableList<SupportAttachment>)

    /**
     * A filter to query [Support]s.
     */
    class SupportFilter(
            @get:Min(0)
            val page: Int?,
            val keywords: Array<String>?,
            val title: String?,
            val author: Long?,
            val start: ZonedDateTime?,
            val end: ZonedDateTime?
    )

    abstract class SupportCreationException : Exception()

    class SupportChannelAlreadyExistsException : SupportCreationException()

    class TooManyOpenedSupportsException : SupportCreationException()

    class SupportChannelDeletedException : Exception()

    class SupportChannelCorruptedException : Exception()
}
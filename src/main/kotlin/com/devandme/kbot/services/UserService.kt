/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.services

import com.devandme.kbot.configuration.DiscordConfiguration
import com.devandme.kbot.database.embeded.PrivateAuthenticationToken
import com.devandme.kbot.database.entities.Banishment
import com.devandme.kbot.database.entities.User
import com.devandme.kbot.database.repositories.UserRepository
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.misc.orNull
import com.devandme.kbot.services.messages.LanguageLevel
import net.dv8tion.jda.core.entities.Member
import org.springframework.data.domain.Pageable
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.lang.Math.min
import java.time.ZonedDateTime
import javax.transaction.Transactional

@Transactional
@Service
class UserService(
    conf: DiscordConfiguration,
    private val userRepository: UserRepository,
    private val supportService: SupportService,
    private val sanctionService: SanctionService,
    private val discord: DiscordService
) {
    private val daysBeforeAwayUserDeletion = conf.daysBeforeAwayUserDeletion

    /**
     * Finds a single [User] thanks to its ID.
     *
     * @param id The [User] ID.
     * @return The [User], or `null` if none match.
     */
    fun findOneById(id: Long): User? = this.userRepository.findById(id).orNull()

    /**
     * Finds all [User]s.
     */
    fun findAll(pageable: Pageable = Pageable.unpaged()): Iterable<User> = this.userRepository.findAll(pageable)

    /**
     * Finds all [User]s with the provided IDs.
     */
    fun findAllByIds(ids: Iterable<Long>): Iterable<User> = this.userRepository.findAllById(ids)

    /**
     * Creates a [User] from the ID with the default values.
     */
    fun createDefault(id: Long) = this.userRepository.save(
        User(
            id = id,
            power = 0,
            messageCount = 0,
            supportHelperCount = 0,
            experience = 0,
            privateToken = PrivateAuthenticationToken.generateNew(),
            preferredLanguageLevel = LanguageLevel.NORMAL_FRENCH
        )
    )

    /**
     * Saves the provided [User].
     *
     * @param user The [User] to save.
     */
    fun save(user: User) {
        this.userRepository.save(user)
    }

    /**
     * Saves the provided [User]s.
     *
     * @param users The [User]s to save.
     */
    fun save(users: Iterable<User>) {
        this.userRepository.saveAll(users)
    }

    /**
     * Deletes the provided [User].
     *
     * @param user The [User] to delete.
     */
    fun delete(user: User) {
        for (sanction in user.sanctionsReceived) {
            this.sanctionService.delete(sanction)
        }

        for (sanction in user.sanctionsGiven) {
            sanction.moderator = null
            this.sanctionService.save(sanction)
        }

        for (support in user.authoredSupports) {
            this.supportService.delete(support)
        }

        this.userRepository.delete(user)
    }

    /**
     * Computes the total power of a user.
     *
     * @param user The database user.
     * @param member The Discord member.
     * @return The total power.
     */
    fun getTotalPower(user: User, member: Member) = min(user.power + this.discord.getRolesPower(member), 100)

    /**
     * Creates users who were not found in the Discord, deletes users who left the Discord.
     */
    @Scheduled(cron = "0 0 * * * *")
    fun updateAvailableUsers() {
        val discordUsersIds = this.discord.guild.members.asSequence().map { it.user.idLong }.toSet()
        val databaseUsers = this.userRepository.findAll()
        val now = ZonedDateTime.now()
        val limitDate = now.minusDays(this.daysBeforeAwayUserDeletion)

        for (user in databaseUsers) {
            if (user.id in discordUsersIds) {
                if (user.discordLeaveDate != null && user.discordLeaveDate!! <= limitDate) {
                    this.delete(user)
                }
                // Don't remove banned users.
            } else if (user.sanctionsReceived.none { it is Banishment && it.startDate <= now && (it.endDate == null || it.endDate >= now) }) {
                user.discordLeaveDate = ZonedDateTime.now()
                this.save(user)
            }
        }
    }
}
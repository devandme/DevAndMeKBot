/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.services

import com.devandme.kbot.configuration.DiscordConfiguration
import com.devandme.kbot.discord.services.ParameterConverterService
import com.devandme.kbot.discord.services.commands.CommandContext
import com.devandme.kbot.discord.services.commands.CommandRegistryService
import com.devandme.kbot.discord.services.commands.InsufficientPowerException
import com.devandme.kbot.discord.services.commands.ParameterConversionException
import com.devandme.kbot.discord.services.commands.parser.EmptyCommandNameException
import com.devandme.kbot.discord.services.commands.parser.UnclosedArgumentDelimiterException
import com.devandme.kbot.discord.services.commands.parser.UnfinishedEscapeSequenceException
import com.devandme.kbot.discord.services.commands.parser.parseCommand
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import net.dv8tion.jda.core.entities.Channel
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.entities.User
import org.slf4j.Logger
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import java.io.PrintWriter
import java.io.StringWriter
import java.time.*
import java.util.*

@Service
class CommandExecutorService(
    discordConfiguration: DiscordConfiguration,
    messageService: MessageService,
    private val environment: Environment,
    private val commands: CommandRegistryService,
    private val converterService: ParameterConverterService,
    private val logger: Logger
) {

    private val messages = messageService / MessageStorage.COMMAND_CONTROLLER

    private val commandPrefix = discordConfiguration.commandPrefix

    /**
     * Executes the provided command.
     *
     * @param context The [CommandContext].
     * @param commandString The command to execute.
     */
    fun execute(context: CommandContext, commandString: String) {
        try {
            val parameters = parseCommand(commandString)

            val matchingCommand = this.commands.findCommandByParameters(parameters)

            if (matchingCommand == null) {
                context.sendMessage(this.messages / "command-not-found")

                val commands = this.commands.findCommandsByName(parameters.first(), context.databaseUser.power).take(10)

                if (commands.isNotEmpty()) {
                    val helpMessage = StringBuilder(this.messages / "found-commands-to-help" / context.databaseUser)

                    for (command in commands) {
                        helpMessage.appendln().append("\t- ").append(command.syntax).append(" : ").append(command.title)
                    }

                    context.sendMessage(helpMessage.toString())
                }

                return
            }

            matchingCommand.call(parameters, context, this.converterService)

        } catch (e: EmptyCommandNameException) {
            context.sendMessage(this.messages / "command-empty")

        } catch (e: UnfinishedEscapeSequenceException) {
            context.sendMessage(this.messages / "unfinished-escape-sequence")

        } catch (e: UnclosedArgumentDelimiterException) {
            context.sendMessageFormat(this.messages / "unclosed-argument-delimiter", e.index)

        } catch (e: ParameterConversionException) {
            val message = when (e.targetType) {
                Int::class, Long::class ->
                    (this.messages / "param-error-int").fmt(context.databaseUser, e.index + 1, e.parameterName)

                Float::class, Double::class ->
                    (this.messages / "param-error-float").fmt(context.databaseUser, e.index + 1, e.parameterName)

                Boolean::class ->
                    (this.messages / "param-error-boolean").fmt(context.databaseUser, e.index + 1, e.parameterName)

                Member::class, User::class ->
                    (this.messages / "param-error-user").fmt(context.databaseUser, e.index + 1, e.parameterName)

                Role::class ->
                    (this.messages / "param-error-role").fmt(context.databaseUser, e.index + 1, e.parameterName)

                Channel::class ->
                    (this.messages / "param-error-channel").fmt(context.databaseUser, e.index + 1, e.parameterName)

                LocalTime::class ->
                    (this.messages / "param-error-time").fmt(context.databaseUser, e.index + 1, e.parameterName)

                LocalDate::class, LocalDateTime::class, ZonedDateTime::class ->
                    (this.messages / "param-error-date").fmt(context.databaseUser, e.index + 1, e.parameterName)

                ZoneId::class ->
                    (this.messages / "param-error-zone-id").fmt(context.databaseUser, e.index + 1, e.parameterName)

                Duration::class ->
                    (this.messages / "param-error-duration").fmt(context.databaseUser, e.index + 1, e.parameterName)

                String::class -> throw RuntimeException("Shouldn't happen!")

                else -> throw RuntimeException("Unhandled case!")
            }

            context.sendMessage(message)

        } catch (e: InsufficientPowerException) {
            context.sendMessageFormat(this.messages / "insufficient-power", e.requiredPower, e.userPower)

        } catch (other: Throwable) {
            val randomInt = Random().nextInt(4) + 1

            if (this.environment.acceptsProfiles("dev")) {
                val error = StringWriter().use { sw ->
                    PrintWriter(sw, true).use(other::printStackTrace)
                    sw.toString()
                }

                context.sendMessage("An internal error occurred while executing your command: ```$error```")
            } else {
                context.sendMessage(this.messages / "internal-error-message$randomInt")
            }

            this.logger.error(
                "Couldn't execute command. User issued '{}{}', error is:",
                this.commandPrefix,
                commandString,
                other
            )

            throw other
        }
    }
}
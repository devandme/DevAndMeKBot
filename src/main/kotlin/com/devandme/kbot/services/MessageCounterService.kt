/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.services

import com.devandme.kbot.database.entities.MessageScanStatus
import com.devandme.kbot.database.repositories.MessageScanStatusRepository
import com.devandme.kbot.discord.services.discord.DiscordService
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.concurrent.CompletableFuture

/**
 * Service that counts messages.
 */
@Transactional
@Service
class MessageCounterService(
    private val discord: DiscordService,
    private val statuses: MessageScanStatusRepository,
    private val users: UserService
) {
    @Scheduled(cron = "0 0 1 * * *")
    fun countMessages() {
        val channels = this.getCountableChannels()

        val statuses = mutableMapOf<Long, MessageScanStatus>()
        this.getCountStatuses(channels).associateByTo(statuses, MessageScanStatus::channelId)

        val tasks = this.createTasks(channels, statuses)
        val messageCountByUser = mutableMapOf<Long, Long>()

        // Asynchronously handle each result.
        repeat(tasks.size) {
            val result = CompletableFuture.anyOf(*tasks.values.toTypedArray()).get() as ScanResult

            tasks -= result.channelId

            if (result is EmptyScanResult) {
                return@repeat
            }

            if (result !is MessageCountScanResult) {
                throw RuntimeException("Unexpected value for result!")
            }

            val status = statuses[result.channelId]

            // Add message count.
            for ((userId, messageCount) in result.countByUserId) {
                messageCountByUser.compute(userId) { _, v -> (v ?: 0) + messageCount }
            }

            if (status == null) {
                // If the status is absent, create a new one.
                statuses[result.channelId] = MessageScanStatus(result.channelId, result.lastMessageId)
            } else {
                // Else, update its last scanned message.
                status.lastScannedMessageId = result.lastMessageId
            }
        }

        this.updateUsers(messageCountByUser)

        // Save all changes.
        this.statuses.saveAll(statuses.values)
    }

    /**
     * Updates the users thanks to the provided message count.
     *
     * @param messageCountByUser The message count by user ID.
     */
    private fun updateUsers(messageCountByUser: Map<Long, Long>) {
        val users = this.users.findAllByIds(messageCountByUser.keys)

        for (user in users) {
            user.messageCount += messageCountByUser[user.id]!!
        }

        this.users.save(users)
    }

    /**
     * @return All channels except the spam channel.
     */
    private fun getCountableChannels() =
        (this.discord.guild.textChannels - this.discord.spamChannel).associateBy(TextChannel::getIdLong)

    /**
     * Retrieves a list of the count status by channel and deletes the channels that does not exists.
     *
     * @param existingChannels A list of the channels that already exists.
     * @return The existing channels statuses.
     */
    private fun getCountStatuses(existingChannels: Map<Long, TextChannel>): Set<MessageScanStatus> {
        val statuses = this.statuses.findAll().toMutableSet()

        val oldStatuses = statuses.filter { !existingChannels.containsKey(it.channelId) }

        statuses -= oldStatuses

        this.statuses.deleteAll(oldStatuses)

        return statuses
    }

    /**
     * Creates the scanning tasks for the provided [TextChannel] thanks to the given [MessageScanStatus]es.
     *
     * @param channels The channels.
     * @param statuses The statuses.
     */
    private fun createTasks(channels: Map<Long, TextChannel>, statuses: Map<Long, MessageScanStatus>):
            MutableMap<Long, CompletableFuture<ScanResult>> {
        val tasks = mutableMapOf<Long, CompletableFuture<ScanResult>>()

        for ((channelId, channel) in channels) {
            val status = statuses[channelId]

            tasks[channelId] = CompletableFuture.supplyAsync {
                if (status == null) {
                    this.countMessagesFromChannelCreation(channel)
                } else {
                    this.countMessagesFrom(status.lastScannedMessageId, channel)
                }
            }
        }

        return tasks
    }

    /**
     * Counts all messages from the channel creation.
     *
     * @param channel The channel to count the messages from.
     */
    fun countMessagesFromChannelCreation(channel: TextChannel): ScanResult {
        val countByUserId = mutableMapOf<Long, Long>()
        var lastMessageId: Long? = null

        for ((i, message) in channel.iterableHistory.cache(false).withIndex()) {
            if (i == 0) {
                lastMessageId = message.idLong
            }

            countByUserId.compute(message.author.idLong) { _, v -> (v ?: 0) + 1 }
        }

        return if (lastMessageId == null) {
            EmptyScanResult(channel.idLong)
        } else {
            MessageCountScanResult(channel.idLong, countByUserId, lastMessageId)
        }
    }

    /**
     * Counts all messages that were sent after the provided message ID in the provided channel.
     *
     * @param messageId The message to start counting from.
     * @param channel The channel to count messages from.
     */
    fun countMessagesFrom(messageId: Long, channel: TextChannel): ScanResult {
        val countByUserId = mutableMapOf<Long, Long>()
        var history: List<Message>
        var lastMessageId = messageId

        do {
            history = channel.getHistoryAfter(lastMessageId, 100).complete().retrievedHistory

            for (message in history) {
                countByUserId.compute(message.author.idLong) { _, v -> (v ?: 0) + 1 }
            }

            if (history.isNotEmpty()) {
                lastMessageId = history.first().idLong
            }
        } while (history.size == 100)

        return MessageCountScanResult(channel.idLong, countByUserId, lastMessageId)
    }

    /**
     * Represents a scan result.
     */
    abstract class ScanResult(val channelId: Long)

    /**
     * An empty scan result (no message was found in the returned channel).
     */
    open class EmptyScanResult(channelId: Long) : ScanResult(channelId)

    /**
     * A full scan result (number of messages that were found).
     */
    class MessageCountScanResult(channelId: Long, val countByUserId: Map<Long, Long>, val lastMessageId: Long) :
        ScanResult(channelId)
}
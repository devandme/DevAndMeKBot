/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.services

import com.devandme.kbot.database.entities.*
import com.devandme.kbot.database.repositories.BanishmentRepository
import com.devandme.kbot.database.repositories.ExclusionRepository
import com.devandme.kbot.database.repositories.MuteRepository
import com.devandme.kbot.database.repositories.SanctionRepository
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.misc.toUnsignedString
import net.dv8tion.jda.core.entities.Member
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import javax.persistence.criteria.Predicate
import javax.transaction.Transactional
import javax.validation.constraints.Min
import kotlin.reflect.KClass

@Transactional
@Service
class SanctionService(
    private val discordService: DiscordService,
    private val sanctionRepository: SanctionRepository,
    private val banishmentRepository: BanishmentRepository,
    private val exclusionRepository: ExclusionRepository,
    private val muteRepository: MuteRepository
) {
    /**
     * Finds a single [Sanction] thanks to its ID.
     *
     * @param id The [Sanction] ID.
     * @return The [Sanction], or `null` if none match.
     */
    fun findById(id: Int): Sanction? = this.sanctionRepository.findOneById(id)

    /**
     * Finds all [Sanction]s.
     */
    fun findAll(page: Pageable = Pageable.unpaged()): Collection<Sanction> = this.sanctionRepository.findAll(page)

    /**
     * Finds all [Sanction]s with a filter.
     *
     * @param filter The filter to apply.
     * @return The matching [Sanction]s.
     */
    fun findAll(filter: SanctionFilter): Collection<Sanction> {
        val page = PageRequest.of(filter.page ?: 0, 100, Sort.Direction.DESC, Sanction::startDate.name)
        val spec = Specification.where<Sanction> { sanction, _, c ->
            val conditions = mutableListOf<Predicate>()

            if (filter.moderator != null) {
                conditions += c.equal(sanction.get<Long>(Sanction::moderator.name), filter.moderator)
            }

            if (filter.target != null) {
                conditions += c.equal(sanction.get<Long>(Sanction::target.name), filter.target)
            }

            if (filter.start != null) {
                conditions += c.greaterThanOrEqualTo(
                    sanction.get<ZonedDateTime>(Sanction::startDate.name),
                    filter.start
                )
            }

            if (filter.end != null) {
                conditions += c.lessThanOrEqualTo(
                    sanction.get<ZonedDateTime>(Sanction::startDate.name),
                    filter.end
                )
            }

            c.and(*conditions.toTypedArray())
        }

        @Suppress("UNCHECKED_CAST")
        return when (filter.type) {
            SanctionType.EXCLUSION -> this.exclusionRepository.findAll(spec as Specification<Exclusion>, page)
            SanctionType.BANISHMENT -> this.banishmentRepository.findAll(spec as Specification<Banishment>, page)
            SanctionType.MUTE -> this.muteRepository.findAll(spec as Specification<Mute>, page)
            null -> this.sanctionRepository.findAll(spec, page)
        }.toList()
    }

    /**
     * Retrieves all mutes that are active for the provided user.
     */
    fun getActiveMutes(user: User) = this.muteRepository.findActiveForUser(user)

    /**
     * Determines if the user is muted or not.
     */
    fun isMuted(user: User) = this.isMuted(user.id)

    /**
     * Determines if the user is muted or not.
     */
    fun isMuted(user: net.dv8tion.jda.core.entities.User) = this.isMuted(user.idLong)

    /**
     * Determines if the user is muted or not.
     */
    fun isMuted(member: Member) = this.isMuted(member.user.idLong)

    /**
     * Determines if the user is muted or not.
     *
     * @param userId The user's ID.
     * @return `true` if the user is muted, `false` otherwise.
     */
    fun isMuted(userId: Long) = this.muteRepository.isMuted(userId) > 0

    /**
     * Retrieves all bans that are active for the provided user.
     */
    fun getActiveBans(user: User) = this.banishmentRepository.findActiveForUser(user)

    /**
     * Determines if the user is banned or not.
     */
    fun isBanned(user: User) = this.isBanned(user.id)

    /**
     * Determines if the user is banned or not.
     */
    fun isBanned(user: net.dv8tion.jda.core.entities.User) = this.isBanned(user.idLong)

    /**
     * Determines if the user is banned or not.
     */
    fun isBanned(member: Member) = this.isBanned(member.user.idLong)

    /**
     * Determines if the user is banned or not.
     *
     * @param userId The user's ID.
     * @return `true` if the user is banned, `false` otherwise.
     */
    fun isBanned(userId: Long) = this.banishmentRepository.isBanned(userId) > 0

    /**
     * Finds all [Sanction]s by their type.
     *
     * @param T The sanction type.
     */
    final inline fun <reified T : Sanction> findAllByType(pageable: Pageable = Pageable.unpaged()) =
        this.findAllByType(T::class, pageable)

    /**
     * Finds all [Sanction]s by their type.
     *
     * @param type The sanction type.
     */
    @Suppress("UNCHECKED_CAST")
    fun <T : Sanction> findAllByType(type: KClass<T>, pageable: Pageable = Pageable.unpaged()): Collection<T> {
        if (type == Banishment::class) {
            return (this.banishmentRepository.findAll(pageable) as Page<T>).toList()
        }

        if (type == Exclusion::class) {
            return (this.exclusionRepository.findAll(pageable) as Page<T>).toList()
        }

        if (type == Mute::class) {
            return (this.muteRepository.findAll(pageable) as Page<T>).toList()
        }

        throw IllegalStateException("Wrong type ${type.simpleName} provided!")
    }

    /**
     * Saves the provided [Sanction].
     *
     * @param sanction The [Sanction] to save.
     */
    fun save(sanction: Sanction): Sanction {
        val now = ZonedDateTime.now()

        return when (sanction) {
            is Banishment -> {
                if (sanction.startDate <= now && (sanction.endDate == null || sanction.endDate >= now)) {
                    this.discordService.guild.controller
                        .ban(sanction.target.id.toString(), 1, sanction.reason)
                        .complete()
                }

                this.banishmentRepository.save(sanction)
            }
            is Exclusion -> {
                this.discordService.guild.controller.kick(sanction.target.id.toString(), sanction.reason).complete()
                this.exclusionRepository.save(sanction)
            }
            is Mute -> this.muteRepository.save(sanction)
            else -> throw IllegalStateException("Wrong type ${sanction::class.simpleName} provided!")
        }
    }

    /**
     * Deletes the provided [Sanction].
     *
     * @param sanction The [Sanction] to delete.
     */
    fun delete(sanction: Sanction) {
        when (sanction) {
            is Banishment -> {
                val target = sanction.target

                this.banishmentRepository.delete(sanction)

                if (!this.isBanned(target)) {

                    this.discordService.guild.controller
                        .unban(target.id.toUnsignedString())
                        .complete()
                }
            }
            is Exclusion -> this.exclusionRepository.delete(sanction)
            is Mute -> this.muteRepository.delete(sanction)
            else -> throw IllegalStateException("Wrong type ${sanction::class.simpleName} provided!")
        }
    }

    /**
     * Pardons the users that got banned in the Discord server but that are no longer banned in the database.
     */
    @Scheduled(cron = "0 0 0 * * *")
    fun pardonNonBannedUsers() {
        val controller = this.discordService.guild.controller
        val invalidBans = this.discordService.guild.banList.complete().filter { !this.isBanned(it.user) }

        for (invalidBan in invalidBans) {
            controller.unban(invalidBan.user).complete()
        }
    }

    /**
     * A filter to query [Sanction]s.
     */
    class SanctionFilter(
        @get:Min(0)
        val page: Int? = null,
        val moderator: Long? = null,
        val target: Long? = null,

        @get:DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        val start: ZonedDateTime? = null,

        @get:DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        val end: ZonedDateTime? = null,
        val type: SanctionType? = null
    )

    enum class SanctionType {
        BANISHMENT,
        MUTE,
        EXCLUSION
    }
}
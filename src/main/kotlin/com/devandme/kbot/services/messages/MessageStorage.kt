/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.services.messages

import org.yaml.snakeyaml.Yaml
import java.io.FileNotFoundException

/**
 * The [MessageStorage] represents a file that contains
 *
 * @property uri The location of the message storage.
 */
enum class MessageStorage(fileName: String) {

    SUPPORT_SERVICE("support-service"),
    SUPPORT_CONTROLLER("support-controller"),
    COMMAND_CONTROLLER("command-controller"),
    HELP_CONTROLLER("help-controller"),
    USER_CONTROLLER("user-controller"),
    GENERAL_COMMANDS_CONTROLLER("general-commands-controller"),
    TIP_CONTROLLER("tip-controller"),
    KEYWORD_CONTROLLER("keyword-controller"),
    SANCTION_CONTROLLER("sanction-controller"),
    SETTINGS_CONTROLLER("settings-controller"),
    DISCORD_WIZARD_SERVICE("discord-wizard-service"),
    MODERATION_CONTROLLER("moderation-controller");

    private val uri = "/fr/devandme/kbot/services/messages/$fileName.yml"

    /**
     * Reads this storage fully and returns them.
     */
    @Throws(MessageStorageParseException::class)
    fun readAndParse(): Messages {
        val stream = MessageStorage::class.java.getResourceAsStream(this.uri)
            ?: throw FileNotFoundException("Message storage ${this.uri} was not found!")

        @Suppress("UNCHECKED_CAST")
        val parsed = stream.use { Yaml().loadAs(it, HashMap::class.java) as? Map<String, Any?> ?: emptyMap() }
        val messages = hashMapOf<String, Message>()

        for ((key, message) in parsed) {
            if (message is Map<*, *>) {
                val levels = mutableMapOf<LanguageLevel, String>()

                for (languageLevel in LanguageLevel.values()) {
                    val value = (message[languageLevel.yamlName]
                        ?: throw MessageStorageParseException("Missing $languageLevel language level for key $key!")) as? String
                        ?: throw MessageStorageParseException("Value of $key.$languageLevel must be a String!")

                    levels[languageLevel] = value
                }

                messages[key] = Message(levels)
            } else {
                throw MessageStorageParseException("A message key should have an object child!")
            }
        }

        return Messages(messages)
    }
}
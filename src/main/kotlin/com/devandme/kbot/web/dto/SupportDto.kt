/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.dto

import com.devandme.kbot.services.MAX_ATTACHMENT_COUNT
import com.devandme.kbot.services.MAX_CONTENT_LENGTH
import com.devandme.kbot.services.MAX_KEYWORD_COUNT
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.multipart.MultipartFile
import java.time.ZonedDateTime
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@ApiModel("Support")
class SupportDto(
    @get:JsonSerialize(using = ToStringSerializer::class)
    val channelId: Long,

    @get:DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val creationDate: ZonedDateTime,

    @get:JsonSerialize(using = ToStringSerializer::class)
    val author: Long,

    val title: String,

    val shortTitle: String,

    @get:DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val solvedDate: ZonedDateTime?,

    val keywords: Array<String>
)

@ApiModel("SupportCreation")
class SupportCreationDto(
    @get:ApiModelProperty(required = true)
    @get:Pattern(regexp = "^[a-z0-9_-]{3,30}$")
    val shortTitle: String,

    @get:ApiModelProperty(required = true)
    @get:NotBlank
    @get:Size(min = 6, max = 255)
    val title: String,

    @get:ApiModelProperty(required = true)
    @get:NotBlank
    @get:Size(max = MAX_CONTENT_LENGTH)
    val content: String,

    @get:ApiModelProperty(required = true)
    @get:Size(min = 1, max = MAX_KEYWORD_COUNT)
    val keywords: List<String>,

    @get:Size(max = MAX_ATTACHMENT_COUNT)
    var attachments: List<MultipartFile>?
)

@ApiModel("SupportUpdate")
class SupportUpdateDto(
    @get:Size(min = 6, max = 255)
    val title: String?,

    @get:Size(max = 10)
    val keywords: List<String>?
)

@ApiModel("SupportClosure")
class SupportClosureDto(
    @get:Size(max = 10)
    val helpers: List<String>? = null
)
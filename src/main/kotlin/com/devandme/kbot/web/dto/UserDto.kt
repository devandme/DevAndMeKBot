/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.dto

import com.devandme.kbot.services.messages.LanguageLevel
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import io.swagger.annotations.ApiModel
import org.hibernate.validator.constraints.Range
import java.time.ZonedDateTime
import javax.validation.constraints.Min

@ApiModel("User")
class UserDto(
    @get:JsonSerialize(using = ToStringSerializer::class)
    val id: Long,
    val power: Int,
    @get:JsonSerialize(using = ToStringSerializer::class)
    val messageCount: Long,
    val supportHelperCount: Int,
    val experience: Int,
    val preferredLanguageLevel: LanguageLevel,
    val discordLeaveDate: ZonedDateTime?
)

@ApiModel("UserUpdate")
class UserUpdateDto(
    @get:Range(min = 0, max = 100)
    val power: Int?,

    @get:Min(0)
    val experience: Int?,

    val preferredLanguageLevel: LanguageLevel?
)
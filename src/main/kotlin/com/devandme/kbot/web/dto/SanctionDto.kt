/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.dto

import com.devandme.kbot.database.entities.Banishment
import com.devandme.kbot.database.entities.Exclusion
import com.devandme.kbot.database.entities.Mute
import com.devandme.kbot.database.entities.Sanction
import com.devandme.kbot.services.SanctionService
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StringDeserializer
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import org.springframework.format.annotation.DateTimeFormat
import java.time.ZonedDateTime
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@ApiModel("Sanction", subTypes = [MuteDto::class, BanishmentDto::class, ExclusionDto::class], discriminator = "type")
abstract class SanctionDto(
    val id: Int?,

    @get:NotNull
    @get:NotBlank
    val reason: String,

    @get:NotNull
    @get:JsonSerialize(using = ToStringSerializer::class)
    val moderator: Long?,

    @get:NotNull
    @get:JsonSerialize(using = ToStringSerializer::class)
    val target: Long,

    @get:NotNull
    val startDate: ZonedDateTime
) {
    abstract val type: SanctionService.SanctionType

    companion object {
        fun fromSanction(s: Sanction) = when (s) {
            is Mute -> MuteDto(s.id, s.reason, s.moderator?.id, s.target.id, s.startDate, s.endDate)
            is Banishment -> BanishmentDto(s.id, s.reason, s.moderator?.id, s.target.id, s.startDate, s.endDate)
            is Exclusion -> ExclusionDto(s.id, s.reason, s.moderator?.id, s.target.id, s.startDate)
            else -> throw RuntimeException("Unhandled case!")
        }
    }
}

@ApiModel("Mute", parent = SanctionDto::class)
class MuteDto(
    id: Int,
    reason: String,
    moderator: Long?,
    target: Long,
    startDate: ZonedDateTime,
    @get:NotNull
    val endDate: ZonedDateTime
) : SanctionDto(id, reason, moderator, target, startDate) {
    override val type = SanctionService.SanctionType.MUTE
}

@ApiModel("Banishment", parent = SanctionDto::class)
class BanishmentDto(
    id: Int,
    reason: String,
    moderator: Long?,
    target: Long,
    startDate: ZonedDateTime,
    val endDate: ZonedDateTime?
) : SanctionDto(id, reason, moderator, target, startDate) {
    override val type = SanctionService.SanctionType.BANISHMENT
}

@ApiModel("Exclusion", parent = SanctionDto::class)
class ExclusionDto(
    id: Int,
    reason: String,
    moderator: Long?,
    target: Long,
    startDate: ZonedDateTime
) : SanctionDto(id, reason, moderator, target, startDate) {
    override val type = SanctionService.SanctionType.EXCLUSION
}

@ApiModel("SanctionCreation")
class SanctionCreationDto(
    @get:ApiModelProperty(required = true, example = "Bad language")
    @get:NotNull
    @get:NotBlank
    val reason: String,

    @get:ApiModelProperty(required = true, example = "5485454851")
    @get:JsonDeserialize(using = StringDeserializer::class)
    @get:NotNull
    val target: Long,

    @get:ApiModelProperty(required = true, example = "EXCLUSION")
    @get:NotNull
    val type: SanctionService.SanctionType,

    @get:ApiModelProperty(required = false, example = "2018-08-23T13:38:17.614+02:00")
    @get:DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val endDate: ZonedDateTime?
)
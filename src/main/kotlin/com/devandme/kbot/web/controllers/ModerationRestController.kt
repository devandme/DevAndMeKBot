/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.controllers

import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.misc.lastMessage
import com.devandme.kbot.services.messages.MessageService
import com.devandme.kbot.services.messages.MessageStorage
import com.devandme.kbot.web.ResourceNotFoundException
import com.devandme.kbot.web.security.DatabaseAndDiscordAuthentication
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@Api(description = "Allows channel moderation!")
@RestController
@RequestMapping("moderation")
class ModerationRestController(private val discord: DiscordService, messageService: MessageService) {

    private val messages = messageService / MessageStorage.MODERATION_CONTROLLER

    @ApiOperation("Clears a channel", nickname = "clearChannel")
    @PreAuthorize("hasAuthority('POWER_60')")
    @PostMapping("channel/clear/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun clearChannel(@PathVariable("id") channelId: Long, @RequestBody count: Int, auth: DatabaseAndDiscordAuthentication) {
        val channel = this.discord.guild.getTextChannelById(channelId) ?: throw ResourceNotFoundException()

        if (channel in this.discord.channelsIgnoringClearCommand) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "This channel cannot be cleared.")
        }

        val lastMessageId = channel.lastMessage?.idLong ?: return
        val history = channel.getHistoryBefore(lastMessageId, count).complete()

        for (message in history.retrievedHistory + channel.lastMessage!!) {
            message.delete()
                    .reason("Cleanup requested by ${auth.discordMember.effectiveName} ($count)")
                    .complete()
        }

        channel.sendMessage((this.messages / "channel-clear").fmt(auth.databaseUser, auth.discordMember.asMention))
                .complete()
    }

    @ApiOperation("Defines the slowmode", nickname = "setSlowMode")
    @PreAuthorize("hasAuthority('POWER_60')")
    @PostMapping("channel/slow-mode/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun setSlowMode(@PathVariable("id") channelId: Long, @RequestBody time: Int) {
        val channel = this.discord.guild.getTextChannelById(channelId) ?: throw ResourceNotFoundException()

        if (time !in 0..120) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong time supplied!")
        }

        channel.manager.setSlowmode(time).complete()
    }
}
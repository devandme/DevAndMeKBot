/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.controllers

import com.devandme.kbot.database.entities.Keyword
import com.devandme.kbot.services.KeywordService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.transaction.Transactional
import javax.validation.constraints.NotEmpty

@Api(description = "Allows listing and managing keywords")
@Transactional
@RestController
@RequestMapping("keywords")
class KeywordRestController(private val keywords: KeywordService) {

    @ApiOperation("Retrieves all keywords", nickname = "findAllKeywords")
    @GetMapping
    fun search(@RequestParam @NotEmpty name: String): Collection<String> =
        this.keywords.search(name)
            .asSequence()
            .take(100)
            .map { it.label }
            .toList()

    @ApiOperation("Tests the existence of the provided keyword", nickname = "keywordExists")
    @GetMapping("exists/{label}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun exists(@PathVariable("label") keyword: Keyword) {
    }

    @ApiOperation("Deletes a keyword", nickname = "deleteKeyword")
    @PreAuthorize("hasAuthority('POWER_50')")
    @DeleteMapping("{label}")
    fun delete(@PathVariable("label") keyword: Keyword) {
        this.keywords.delete(keyword)
    }
}
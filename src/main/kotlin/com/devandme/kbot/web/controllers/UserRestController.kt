/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.controllers

import com.devandme.kbot.database.entities.User
import com.devandme.kbot.misc.POWER_UPDATE_OTHER_USER
import com.devandme.kbot.services.UserService
import com.devandme.kbot.web.ForbiddenException
import com.devandme.kbot.web.ResourceNotFoundException
import com.devandme.kbot.web.dto.UserDto
import com.devandme.kbot.web.dto.UserUpdateDto
import com.devandme.kbot.web.security.DatabaseAndDiscordAuthentication
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.transaction.Transactional
import javax.validation.Valid
import kotlin.math.max

@Api(description = "Allows listing and managing available users")
@Transactional
@RestController
@RequestMapping("users")
class UserRestController(private val users: UserService) {

    @ApiOperation("Retrieves all users", nickname = "findAllUsers")
    @GetMapping
    fun findAll(@RequestParam(defaultValue = "0") page: Int): Collection<UserDto> =
        this.users.findAll(PageRequest.of(page, 100)).map {
            UserDto(
                it.id,
                it.power,
                it.messageCount,
                it.supportHelperCount,
                it.experience,
                it.preferredLanguageLevel,
                it.discordLeaveDate
            )
        }

    @ApiOperation("Retrieves a single user thanks to its ID", nickname = "findUserById")
    @GetMapping("{id}")
    fun findById(@PathVariable("id") user: User?): UserDto {
        user ?: throw ResourceNotFoundException()

        return UserDto(
            user.id,
            user.power,
            user.messageCount,
            user.supportHelperCount,
            user.experience,
            user.preferredLanguageLevel,
            user.discordLeaveDate
        )
    }

    @ApiOperation("Updates a user", nickname = "updateUser")
    @PatchMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(@PathVariable("id") user: User?, @RequestBody @Valid data: UserUpdateDto, auth: DatabaseAndDiscordAuthentication) {
        user ?: throw ResourceNotFoundException()

        var requiredPower = 0

        if (data.power != null) {
            user.power = data.power
            requiredPower = max(requiredPower, data.power)
        }

        if (data.experience != null) {
            user.experience = data.experience
            requiredPower = max(requiredPower, POWER_UPDATE_OTHER_USER)
        }

        if (data.preferredLanguageLevel != null) {
            user.preferredLanguageLevel = data.preferredLanguageLevel

            if (user.id != auth.databaseUser.id) {
                requiredPower = max(requiredPower, POWER_UPDATE_OTHER_USER)
            }
        }

        if (!auth.hasPower(requiredPower)) {
            throw ForbiddenException()
        }

        this.users.save(user)
    }
}
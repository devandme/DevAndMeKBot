/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.controllers

import com.devandme.kbot.database.entities.Keyword
import com.devandme.kbot.database.entities.Support
import com.devandme.kbot.misc.POWER_CLOSE_OTHER_SUPPORT
import com.devandme.kbot.misc.POWER_OPEN_OTHER_SUPPORT
import com.devandme.kbot.misc.POWER_UPDATE_OTHER_SUPPORT
import com.devandme.kbot.services.*
import com.devandme.kbot.services.SupportService.NewAttachment
import com.devandme.kbot.web.ForbiddenException
import com.devandme.kbot.web.ResourceNotFoundException
import com.devandme.kbot.web.dto.*
import com.devandme.kbot.web.security.DatabaseAndDiscordAuthentication
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import javax.validation.Valid

@Api(description = "Allows support creation, modification and more!")
@RestController
@RequestMapping("supports")
@Transactional
class SupportRestController(
    private val supports: SupportService,
    private val sanctions: SanctionService,
    private val keywords: KeywordService,
    private val users: UserService
) {
    @ApiOperation("Finds all supports", nickname = "findAllSupports")
    @GetMapping
    fun findAll(filter: SupportService.SupportFilter): Collection<SupportDto> =
        this.supports.findAll(filter).map {
            SupportDto(
                it.channelId,
                it.creationDate,
                it.author.id,
                it.title,
                it.shortTitle,
                it.solvedDate,
                it.keywords.map(Keyword::label).toTypedArray()
            )
        }

    @ApiOperation("Finds a single support with its ID", nickname = "findSupportById")
    @GetMapping("{id}")
    fun findById(@PathVariable("id") support: Support?): SupportDto {
        support ?: throw ResourceNotFoundException()

        return SupportDto(
            support.channelId,
            support.creationDate,
            support.author.id,
            support.title,
            support.shortTitle,
            support.solvedDate,
            support.keywords.map(Keyword::label).toTypedArray()
        )
    }

    @ApiOperation("Finds all attachments for the given support", nickname = "findAttachmentsForSupportId")
    @GetMapping("{id}/attachments")
    fun findAttachments(@PathVariable("id") support: Support?): List<SupportAttachmentDto> =
        try {
            this.supports.getContents(support ?: throw ResourceNotFoundException()).attachments
                .asSequence()
                .filterIsInstance<SupportService.ExistingAttachment>() // All attachments are existing ones here.
                .map { SupportAttachmentDto(it.attachment.idLong, it.name, it.attachment.url) }
                .toList()
        } catch (e: SupportService.SupportChannelDeletedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel has been deleted!")
        } catch (e: SupportService.SupportChannelCorruptedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel messages have been edited or deleted!")
        }

    @ApiOperation("Adds an attachment to the given support", nickname = "createAttachmentForSupport")
    @PostMapping("{support-id}/attachments", consumes = ["multipart/form-data"])
    @ResponseStatus(HttpStatus.CREATED)
    fun addAttachment(
        @PathVariable("support-id") support: Support?,
        @RequestParam("file", required = true) attachment: MultipartFile,
        auth: DatabaseAndDiscordAuthentication
    ) {
        support ?: throw ResourceNotFoundException()

        if (support.author.id != auth.databaseUser.id && !auth.hasPower(POWER_UPDATE_OTHER_SUPPORT)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You must be the support's owner to do that!")
        }

        if (this.sanctions.isMuted(auth.databaseUser)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You are muted!")
        }

        if (support.solvedDate != null) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "A closed support can't be updated!")
        }

        try {
            val contents = this.supports.getContents(support)

            contents.attachments += NewAttachment(attachment)

            if (contents.attachments.size > MAX_ATTACHMENT_COUNT) {
                throw ResponseStatusException(HttpStatus.FORBIDDEN, "Too much attachments for this support!")
            }

            this.supports.setContents(support, contents)
        } catch (e: SupportService.SupportChannelDeletedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel has been deleted!")
        } catch (e: SupportService.SupportChannelCorruptedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel messages have been edited or deleted!")
        }
    }

    @ApiOperation("Removes an attachment from the given support", nickname = "deleteAttachmentForSupport")
    @DeleteMapping("{support-id}/attachments/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteAttachment(
        @PathVariable("support-id") support: Support?,
        @PathVariable("id") supportId: Long,
        auth: DatabaseAndDiscordAuthentication
    ) {
        support ?: throw ResourceNotFoundException()

        if (support.author.id != auth.databaseUser.id && !auth.hasPower(POWER_UPDATE_OTHER_SUPPORT)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You must be the support's owner to do that!")
        }

        if (this.sanctions.isMuted(auth.databaseUser)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You are muted!")
        }

        if (support.solvedDate != null) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "A closed support can't be updated!")
        }

        try {
            val contents = this.supports.getContents(support)

            if (!contents.attachments.removeIf { (it as SupportService.ExistingAttachment).attachment.idLong == supportId }) {
                throw ResourceNotFoundException()
            }

            this.supports.setContents(support, contents)
        } catch (e: SupportService.SupportChannelDeletedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel has been deleted!")
        } catch (e: SupportService.SupportChannelCorruptedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel messages have been edited or deleted!")
        }
    }

    @ApiOperation("Retrieves the content of the given support", nickname = "getContentForSupportId")
    @GetMapping("{id}/content", produces = ["text/markdown"])
    fun getContent(@PathVariable("id") support: Support?): String =
        try {
            // FIXME: Join split messages using a DiscordMessageJoiner.
            this.supports.getContents(support ?: throw ResourceNotFoundException()).content
        } catch (e: SupportService.SupportChannelDeletedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel has been deleted!")
        } catch (e: SupportService.SupportChannelCorruptedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel messages have been edited or deleted!")
        }

    @ApiOperation("Updates the content of the given support", nickname = "setContentForSupportId")
    @PutMapping("{id}/content", produces = ["text/markdown"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun setContent(@PathVariable("id") support: Support?, @RequestBody content: String, auth: DatabaseAndDiscordAuthentication) {
        support ?: throw ResourceNotFoundException()

        if (support.author.id != auth.databaseUser.id && !auth.hasPower(POWER_UPDATE_OTHER_SUPPORT)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You must be the support's owner to do that!")
        }

        if (this.sanctions.isMuted(auth.databaseUser)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You are muted!")
        }

        if (support.solvedDate != null) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "A closed support can't be updated!")
        }

        if (content.length > MAX_CONTENT_LENGTH) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "The provided content is too long!")
        }

        try {
            val contents = this.supports.getContents(support)

            contents.content = content

            this.supports.setContents(support, contents)
        } catch (e: SupportService.SupportChannelDeletedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel has been deleted!")
        } catch (e: SupportService.SupportChannelCorruptedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel messages have been edited or deleted!")
        }
    }

    @ApiOperation("Creates a new support", nickname = "createSupport")
    @PostMapping(consumes = ["multipart/form-data", "application/x-www-form-urlencoded"])
    fun create(@Valid form: SupportCreationDto, auth: DatabaseAndDiscordAuthentication): ResponseEntity<Void> {
        if (this.sanctions.isMuted(auth.databaseUser)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You're muted!")
        }

        val user = this.users.findOneById(auth.databaseUser.id) ?: throw ForbiddenException()
        val attachments = form.attachments ?: emptyList()

        for (attachment in attachments) {
            if (attachment.isEmpty) {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "File ${attachment.originalFilename} is empty!")
            }

            if (attachment.size > MAX_ATTACHMENT_SIZE) {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "File ${attachment.originalFilename} is too big!")
            }
        }

        val created = try {
            this.supports.create(
                user,
                form.title,
                form.shortTitle,
                form.content,
                attachments.map(::NewAttachment),
                this.keywords.findOrCreate(form.keywords)
            )
        } catch (e: SupportService.SupportChannelAlreadyExistsException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "A channel with the shortTitle already exists!")
        } catch (e: SupportService.TooManyOpenedSupportsException) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "The user owns too many supports!")
        }

        val builder = ServletUriComponentsBuilder.fromCurrentRequest()

        builder.replaceQuery(null)

        return ResponseEntity.created(builder.pathSegment(created.channelId.toString()).build().toUri()).build()
    }

    @ApiOperation("Updates a closed support", nickname = "updateSupport")
    @PatchMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(@PathVariable("id") support: Support?, @RequestBody @Valid form: SupportUpdateDto, auth: DatabaseAndDiscordAuthentication) {
        support ?: throw ResourceNotFoundException()

        if (support.author.id != auth.databaseUser.id && !auth.hasPower(POWER_UPDATE_OTHER_SUPPORT)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You must be the support's owner to do that!")
        }

        if (this.sanctions.isMuted(auth.databaseUser)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You're muted!")
        }

        if (support.solvedDate != null) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "A closed support can't be updated!")
        }

        if (form.title != null) {
            if (form.title.isBlank()) {
                throw ResponseStatusException(HttpStatus.FORBIDDEN, "The title must not be blank!")
            }

            support.title = form.title
        }

        if (form.keywords != null) {
            if (form.keywords.isEmpty()) {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "The keywords musn't be empty!")
            }

            support.keywords.clear()
            support.keywords.addAll(this.keywords.findOrCreate(form.keywords))
        }

        try {
            this.supports.update(support)
        } catch (e: SupportService.SupportChannelDeletedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel has been deleted!")
        } catch (e: SupportService.SupportChannelCorruptedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel messages have been edited or deleted!")
        }
    }

    @ApiOperation("Opens a closed support", nickname = "openSupport")
    @PostMapping("open/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun open(@PathVariable("id") support: Support?, auth: DatabaseAndDiscordAuthentication) {
        support ?: throw ResourceNotFoundException()

        if (support.author.id != auth.databaseUser.id && !auth.hasPower(POWER_OPEN_OTHER_SUPPORT)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You must be the support's owner to do that!")
        }

        if (support.solvedDate == null) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "The support is already open!")
        }

        if (this.sanctions.isMuted(support.author)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You're muted!")
        }

        try {
            this.supports.open(support)
        } catch (e: SupportService.SupportChannelDeletedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel has been deleted!")
        }
    }

    @ApiOperation("Closes an open support", nickname = "closeSupport")
    @PostMapping("close/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun close(@PathVariable("id") support: Support?, @RequestBody @Valid form: SupportClosureDto, auth: DatabaseAndDiscordAuthentication) {
        support ?: throw ResourceNotFoundException()

        if (support.author.id != auth.databaseUser.id && !auth.hasPower(POWER_CLOSE_OTHER_SUPPORT)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You must be the support's owner to do that!")
        }

        if (support.solvedDate != null) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "The support is already closed!")
        }

        if (this.sanctions.isMuted(support.author)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You're muted!")
        }

        // Get helpers.
        val helpers = (form.helpers ?: emptyList())
            .asSequence()
            .map {
                it.toLongOrNull()
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "helpers must be a int array!")
            }
            .distinct()
            .filter { it != auth.databaseUser.id }
            .map {
                this.users.findOneById(it)
                    ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "helper $it was not found!")
            }
            .toList()

        try {
            this.supports.close(support, helpers)
        } catch (e: SupportService.SupportChannelDeletedException) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Channel has been deleted!")
        }
    }

    @ApiOperation("Deletes a support", nickname = "deleteSupport")
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('POWER_50')")
    fun delete(@PathVariable("id") support: Support?) {
        this.supports.delete(support ?: throw ResourceNotFoundException())
    }
}
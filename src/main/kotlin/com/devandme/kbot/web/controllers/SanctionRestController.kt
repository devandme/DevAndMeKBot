/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.controllers

import com.devandme.kbot.database.entities.*
import com.devandme.kbot.services.SanctionService
import com.devandme.kbot.services.UserService
import com.devandme.kbot.web.ResourceNotFoundException
import com.devandme.kbot.web.dto.SanctionCreationDto
import com.devandme.kbot.web.dto.SanctionDto
import com.devandme.kbot.web.security.DatabaseAndDiscordAuthentication
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import net.dv8tion.jda.core.exceptions.HierarchyException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.time.ZonedDateTime
import javax.transaction.Transactional
import javax.validation.Valid

@Api(description = "Retrieves, creates and deletes sanctions for users")
@Transactional
@RestController
@RequestMapping("sanctions")
class SanctionRestController(private val sanctions: SanctionService, private val users: UserService) {

    @ApiOperation("Retrieves all sanctions", nickname = "findAllSanctions")
    @PreAuthorize("hasAuthority('POWER_50')")
    @GetMapping
    fun findAll(@Valid filter: SanctionService.SanctionFilter): Collection<SanctionDto> =
        this.sanctions.findAll(filter).map(SanctionDto.Companion::fromSanction)

    @ApiOperation("Retrieves a single sanction with its ID", nickname = "findSanctionById")
    @GetMapping("{id}")
    @PreAuthorize("hasAuthority('POWER_50')")
    fun findById(@PathVariable("id") sanction: Sanction?): SanctionDto =
        SanctionDto.fromSanction(sanction ?: throw ResourceNotFoundException())

    @ApiOperation("Creates a sanction", nickname = "createSanction")
    @PreAuthorize("hasAuthority('POWER_50')")
    @PostMapping
    fun create(@RequestBody @Valid form: SanctionCreationDto, auth: DatabaseAndDiscordAuthentication): ResponseEntity<Void> {
        val now = ZonedDateTime.now()

        val target = this.users.findOneById(form.target)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "target was not found!")

        if (auth.userPower <= target.power) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You don't have enough power to ban this user!")
        }

        var sanction = when (form.type) {
            SanctionService.SanctionType.EXCLUSION -> Exclusion(
                reason = form.reason,
                moderator = auth.databaseUser,
                target = target,
                startDate = now
            )

            SanctionService.SanctionType.BANISHMENT -> {
                if (form.endDate != null && form.endDate <= now) {
                    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "endDate must be greater than current date!")
                }

                Banishment(
                    reason = form.reason,
                    moderator = auth.databaseUser,
                    target = target,
                    startDate = now,
                    endDate = form.endDate
                )
            }

            SanctionService.SanctionType.MUTE -> Mute(
                reason = form.reason,
                moderator = auth.databaseUser,
                target = target,
                startDate = now,
                endDate = form.endDate
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "endDate is required for a MUTE sanction!")
            )
        }

        sanction = try {
            this.sanctions.save(sanction)
        } catch (ex: HierarchyException) {
            throw ResponseStatusException(
                HttpStatus.FORBIDDEN,
                "Couldn't apply the sanction because of the bot has insufficient permissions!"
            )
        }

        val builder = ServletUriComponentsBuilder.fromCurrentRequest()

        builder.replaceQuery(null)

        return ResponseEntity.created(builder.pathSegment(sanction.id.toString()).build().toUri()).build()
    }

    @ApiOperation("Pardons a banned user", nickname = "pardonUser")
    @PostMapping("pardon/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('POWER_50')")
    fun pardon(@PathVariable("id") user: User?) {
        user ?: throw ResourceNotFoundException()

        val activeBans = this.sanctions.getActiveBans(user)

        if (activeBans.isEmpty()) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "The user is not banned!")
        }

        for (ban in activeBans) {
            this.sanctions.delete(ban)
        }
    }

    @ApiOperation("Un-mutes a banned user", nickname = "unmuteUser")
    @PostMapping("unmute/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('POWER_50')")
    fun unmute(@PathVariable("id") user: User?) {
        user ?: throw ResourceNotFoundException()

        val activeMutes = this.sanctions.getActiveMutes(user)

        if (activeMutes.isEmpty()) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "The user is not muted!")
        }

        for (mute in activeMutes) {
            this.sanctions.delete(mute)
        }
    }
}
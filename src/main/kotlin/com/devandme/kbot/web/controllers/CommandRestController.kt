/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.controllers

import com.devandme.kbot.discord.services.commands.ApiCommandContext
import com.devandme.kbot.discord.services.commands.CommandRegistryService
import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.services.CommandExecutorService
import com.devandme.kbot.web.ResourceNotFoundException
import com.devandme.kbot.web.dto.CommandDto
import com.devandme.kbot.web.security.DatabaseAndDiscordAuthentication
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*
import kotlin.math.min

@Api(description = "Allows command execution, listing and more!")
@RestController
@RequestMapping("commands")
class CommandRestController(
    private val commandExecutor: CommandExecutorService,
    private val commands: CommandRegistryService,
    private val discord: DiscordService
) {

    @ApiOperation(
        "Executes the POSTed command and returns a plain text representation of the response",
        nickname = "executeCommand"
    )
    @PostMapping("execute", consumes = ["text/plain"], produces = ["text/markdown"])
    fun execute(@RequestBody command: String, authentication: DatabaseAndDiscordAuthentication): String {
        val power = min(this.discord.getRolesPower(authentication.discordMember) + authentication.userPower, 100)
        val commandContext = ApiCommandContext(authentication.databaseUser, authentication.discordMember, power)

        this.commandExecutor.execute(commandContext, command)

        return commandContext.messagesSent.joinToString("\n\n") { it.contentRaw }
    }

    @ApiOperation("Retrieves all available commands", nickname = "findAllCommands")
    @GetMapping
    fun findAll(@RequestParam(defaultValue = "0") page: Int, auth: DatabaseAndDiscordAuthentication): List<CommandDto> =
        this.commands
            .findAll(auth.userPower, PageRequest.of(page, 100))
            .map { CommandDto(it.names.toTypedArray(), it.syntax) }

    @ApiOperation("Retrieves commands by its name", nickname = "findAllCommandsByName")
    @GetMapping("find-by-name/{name}")
    fun findByName(@PathVariable("name") name: String, @RequestParam(defaultValue = "0") page: Int, auth: DatabaseAndDiscordAuthentication): List<CommandDto> =
        this.commands
            .findCommandsByName(name, auth.userPower, PageRequest.of(page, 100))
            .map { CommandDto(it.names.toTypedArray(), it.syntax) }

    @ApiOperation("Retrieves commands thanks to one search keyword", nickname = "searchCommands")
    @GetMapping("search")
    fun search(@RequestParam keyword: String, @RequestParam(defaultValue = "0") page: Int, auth: DatabaseAndDiscordAuthentication): List<CommandDto> =
        this.commands
            .findCommandsContaining(keyword, auth.userPower, PageRequest.of(page, 100))
            .map { CommandDto(it.names.toTypedArray(), it.syntax) }

    @ApiOperation("Retrieves a single command thanks to its parameters", nickname = "findCommandByParameters")
    @GetMapping("find-by-parameters")
    fun findByParameters(@RequestParam parameters: List<String>, auth: DatabaseAndDiscordAuthentication): CommandDto =
        (this.commands.findCommandByParameters(parameters) ?: throw ResourceNotFoundException())
            .let { CommandDto(it.names.toTypedArray(), it.syntax) }
}
/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.security

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import com.devandme.kbot.database.entities.User as DatabaseUser
import net.dv8tion.jda.core.entities.Member as DiscordUser

/**
 * List of the authorities the users can have.
 */
private val AUTHORITIES = (0..100).associate { it to SimpleGrantedAuthority("POWER_$it") }

/**
 * An authentication combining a Discord and database authentication.
 *
 * @property discordMember The Discord user.
 * @property databaseUser The database user.
 * @property credentials The credentials used to authenticate.
 */
class DatabaseAndDiscordAuthentication(
        val discordMember: DiscordUser,
        val databaseUser: DatabaseUser,
        val userPower: Int,
        private val credentials: Any
) : Authentication {

    private var authenticated: Boolean = true

    private val grantedAuthorities by lazy { AUTHORITIES.filter { (k, _) -> k <= this.userPower }.values }

    override fun getAuthorities(): Collection<GrantedAuthority> = this.grantedAuthorities

    /**
     * Determines if the user has the provided power.
     *
     * @param power The power.
     * @return `true` if the user got the power, `false` otherwise.
     */
    fun hasPower(power: Int) = this.userPower >= power

    override fun getName(): String = this.discordMember.effectiveName

    override fun getCredentials() = this.credentials

    override fun getPrincipal() = this.databaseUser to this.discordMember

    override fun setAuthenticated(isAuthenticated: Boolean) {
        this.authenticated = isAuthenticated
    }

    override fun isAuthenticated() = this.authenticated

    override fun getDetails() = null
}
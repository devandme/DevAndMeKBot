/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.security

import com.devandme.kbot.database.embeded.PrivateAuthenticationToken
import org.springframework.security.crypto.codec.Hex
import java.nio.charset.StandardCharsets
import java.security.GeneralSecurityException
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * Represents a public token derived from a private token.
 */
class PublicAuthenticationToken(private val raw: String) {

    /**
     * Validates a token against a [PrivateAuthenticationToken].
     *
     * @param privateToken The private token.
     * @return `true` if the token is valid, `false` otherwise.
     */
    fun validateAgainst(privateToken: PrivateAuthenticationToken): Boolean {
        val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")

        cipher.init(
            Cipher.DECRYPT_MODE,
            SecretKeySpec(privateToken.encryptionKey, "AES"),
            IvParameterSpec(privateToken.initializationVector)
        )

        val tokenString = try {
            cipher.doFinal(Hex.decode(this.raw)).toString(StandardCharsets.UTF_8)
        } catch (e: IllegalArgumentException) {
            // Hex representation is invalid.
            return false
        } catch (e: GeneralSecurityException) {
            // Bad data provided.
            return false
        }

        if ('|' !in tokenString) {
            return false
        }

        val (payload, expiryDateString) = tokenString.split('|')

        if (payload != privateToken.payload) {
            return false
        }

        val expiryDate = try {
            ZonedDateTime.parse(expiryDateString, DateTimeFormatter.ISO_ZONED_DATE_TIME)
        } catch (e: DateTimeParseException) {
            return false
        }

        return expiryDate >= ZonedDateTime.now()
    }

    override fun toString() = this.raw

    companion object {

        /**
         * Creates a new [PublicAuthenticationToken] based on the provided [PrivateAuthenticationToken] and expiry date.
         *
         * @param privateToken The private token.
         * @param expiryDate The public token expiry date.
         * @return The created [PublicAuthenticationToken].
         */
        fun createFromPrivate(privateToken: PrivateAuthenticationToken, expiryDate: ZonedDateTime):
                PublicAuthenticationToken {

            val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")

            cipher.init(
                Cipher.ENCRYPT_MODE,
                SecretKeySpec(privateToken.encryptionKey, "AES"),
                IvParameterSpec(privateToken.initializationVector)
            )

            val contentString = privateToken.payload + '|' + expiryDate.format(DateTimeFormatter.ISO_ZONED_DATE_TIME)
            val raw = String(Hex.encode(cipher.doFinal(contentString.toByteArray(StandardCharsets.UTF_8))))

            return PublicAuthenticationToken(raw)
        }
    }
}
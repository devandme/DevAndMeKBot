/*
 * Copyright 2019 Mathéo CIMBARO, Arthur MILLE
 *
 * Licensed under “Commons Clause” License Condition v1.0
 * and Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://commonsclause.com/
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devandme.kbot.web.security

import com.devandme.kbot.discord.services.discord.DiscordService
import com.devandme.kbot.services.UserService
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class AuthenticationService(
    private val users: UserService,
    private val discord: DiscordService
) : AuthenticationProvider {

    override fun authenticate(authentication: Authentication): Authentication {
        val userId = authentication.name.toLongOrNull()
            ?: throw BadCredentialsException("User name is not a valid Long!")

        val databaseUser = this.users.findOneById(userId)
            ?: throw UsernameNotFoundException("User was not found!")

        val discordUser = this.discord.guild.getMemberById(userId)
            ?: throw UsernameNotFoundException("User is not on Discord!")

        val token = PublicAuthenticationToken(authentication.credentials as String)

        if (!token.validateAgainst(databaseUser.privateToken)) {
            throw BadCredentialsException("The provided token invalid or expired!")
        }

        return DatabaseAndDiscordAuthentication(
            discordUser,
            databaseUser,
            this.users.getTotalPower(databaseUser, discordUser),
            token
        )
    }

    override fun supports(authentication: Class<*>) =
        authentication.isAssignableFrom(UsernamePasswordAuthenticationToken::class.java)
}